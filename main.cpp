#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>

using namespace std;

class Date { // клас Дата
private: // приватні методи
	string day;
	string month;
	string year;
public: // публічні методи
	void setDay(string value) {
		day = value;
	}
	void setMonth(string value) {
		month = value;
	}
	void setYear(string value) {
		year = value;
	}
	string printDate() {
		return day + " " + month + " " + year;
	}
	void setDate() {
		cout << "Enter day: ";
		do // обмеження можливих значень при введенні дати
			cin >> day;
			if (atoi(day.c_str()) > 31) {
				cout << "Please enter numbers in range 1-31 for day:";
			}
		} while (atoi(day.c_str()) > 31);
		cout << "Enter month: ";
		do {
			cin >> month;
			if (atoi(month.c_str()) > 12) {
				cout << "Please enter numbers in range 1-12 for month:";
			}
		} while (atoi(month.c_str()) > 12);
		cout << "Enter year: ";
		do {
			cin >> year;
			if (atoi(year.c_str()) < 1000 || atoi(year.c_str()) > 2017) {
				cout << "Year must have 4 digits and be not gretet than current year:";
			}
		} while ((atoi(year.c_str()) < 1000 || atoi(year.c_str()) > 2017));

	}
	void setDate(ifstream& fin) {
		fin >> day;
		fin >> month;
		fin >> year;
	}
};

class Person { // клас Особа
protected: // захищені методи - доступні з похідних класів
	string lastName;
	string name;
	Date birthday;
public: // публічні методи
	Person() {}
	Person(string tName, string tLastName) {
		name = tName;
		lastName = tLastName;
	}
	void setLastName(string value) {
		lastName = value;
	}
	void setName(string value) {
		name = value;
	}
	void setBirth() {
		birthday.setDate();
	}
	void setBirth(ifstream& fin) {
		birthday.setDate(fin);
	}
	string getLastName() {
		return lastName;
	}
	string getName() {
		return name;
	}
	Date getDate() {
		return birthday;
	}
};

class Artist : public Person { // клас Артист, успадковування від класу Person
	string type;
public:
	Artist() {
		type = "0";
	}
	Artist(string tName, string tLastName, string tType) : Person(tName, tLastName) {
		type = tType;
	}
	string getType() {
		return type;
	}
	void setType(string value) {
		type = value;
	}
};

class Performer { // клас Виконавець
	Person pers; // агрегація класу Person в клас Performance (композиція)
	Artist art; // агрегація класу Artist в клас Performance (композиція)
public:
	Person getPerson() {
		return pers;
	}
	Artist getArt() {
		return art;
	}
};

class QuartetOfPerformers { // клас Квартет Виконавців
	Artist art[4];
public:
	Artist getPerformer(int ind) {
		return art[ind];
	}
	void setPerform(int index, Artist tArt) {
		art[index] = tArt;
	}
	string printQuartet() {
		return art[0].getLastName() + ", " + art[1].getLastName() + ", " + art[2].getLastName() + ", " + art[3].getLastName();
	}
	string writeArtist(int ind) {
		return art[ind].getName() + " " + art[ind].getLastName() + " " + art[ind].getType();
	}
};

class Quartet { // клас Квартет (музичний твір)
public:
	Person composer;
	string Opus;
	Date dateCreate;
};

class Performance { // клас Виконання
public:
	//Quartet *composition;
	vector<Quartet> composition;
	QuartetOfPerformers performances;
	Date implemen;
	int time;
	int sizeCopmosit;

	void setSize(int size) {
		composition.reserve(size);
		sizeCopmosit = size;
	}

	int getTime() {
		return time;
	}
};

class LibraryOfQuartets { // клас Фонотека Квартетів
public:
	vector<Performance>	impl;
	Quartet quaert;
	QuartetOfPerformers QuartOfPerf;
	int sizeImpl;
	void setSize(int size) {
		impl.reserve(size);
		sizeImpl = size;
	}
};

class Files { // клас Файл
	string file = "text.txt";
	string binFile = "binary.txt";
public:
	void write(LibraryOfQuartets lib[]) { // запис даних у текстовий файл
		ofstream fout;
		try
		{
			fout.open("text.txt");
			if (!fout) {
				throw "File cannot be opened.";
			}
			// TODO: WTF? refactor this shit
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 4; j++) {
					fout << lib[0].QuartOfPerf.writeArtist(j) << " "; // TODO: why space here?
					fout << lib[0].QuartOfPerf.getPerformer(j).getDate().printDate() << " ";
				}
				fout << lib[0].sizeImpl << " ";
				for (int j = 0; j < lib[0].sizeImpl; j++) {
					fout << lib[0].impl[j].time << " ";
					fout << lib[0].impl[j].implemen.printDate() << " ";
					fout << lib[0].impl[j].sizeCopmosit << " ";
					for (int k = 0; k < lib[0].impl[j].sizeCopmosit; k++) {
						fout << lib[0].impl[j].composition[k].composer.getName() << " ";
						fout << lib[0].impl[j].composition[k].composer.getLastName() << " ";
						fout << lib[0].impl[j].composition[k].composer.getDate().printDate() << " ";
						fout << lib[0].impl[j].composition[k].dateCreate.printDate() << " ";
						fout << lib[0].impl[j].composition[k].Opus << " ";
					}
				}
			}
			fout.close();
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}

	void writeBin(LibraryOfQuartets lib[]) { // запис даних у бінарний файл
		ofstream fout;
		try
		{
			fout.open("textBin.txt", ios::binary | ios::out); // TODO: why binary? it's a text
			if (!fout) {
				throw "File cannot be opened.";
			}
			LibraryOfQuartets temp;
			for (int i = 0; i < 10; i++) {
				fout.write((char*)&lib[i], sizeof(temp));
			}

			fout.close();
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}
	vector<Performance> impls;
	vector <Quartet> compositions;

	void readBin(LibraryOfQuartets lib[]) { // зчитування даних з бінарного файлу
		ifstream fin;
		try
		{
			fin.open("textBin.txt", ios::binary | ios::in); // TODO: why textBin.txt????
			if (!fin) {
				throw "File cannot be opened.";
			}
			LibraryOfQuartets temp;
			for (int i = 0; i < 10; i++) {
				fin.read((char*)&lib[i], sizeof(temp));
			}


			fin.close();
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}

	void read(LibraryOfQuartets lib[]) { // зчитування даних з текстового файлу
		ifstream fin;
		try
		{
			fin.open("text.txt");
			if (!fin) {
				throw "File cannot be opened.";
			}
			string name, lastName, type;
			for (int i = 0; i < 10; i++) {
				QuartetOfPerformers quartet;
				for (int j = 0; j < 4; j++) {
					fin >> name;
					fin >> lastName;
					fin >> type;
					Artist temp(name, lastName, type);
					temp.setBirth(fin);
					quartet.setPerform(j, temp);
				}
				lib[i].QuartOfPerf = quartet;
				int size;
				int sizeComposition;
				fin >> size;
				lib[i].sizeImpl = size;
				lib[i].setSize(size);
				vector<Performance> impl(size);
				for (int j = 0; j < size; j++) {
					fin >> impl[j].time;
					impl[j].implemen.setDate(fin);
					impl[j].performances = lib[i].QuartOfPerf;
					fin >> sizeComposition;
					impl[j].sizeCopmosit = sizeComposition;
					impl[j].setSize(sizeComposition);
					vector <Quartet> composition(sizeComposition);
					for (int k = 0; k < sizeComposition; k++) {
						fin >> name;
						fin >> lastName;
						Person temp(name, lastName);
						composition[k].composer = temp;
						composition[k].composer.setBirth(fin);
						composition[k].dateCreate.setDate(fin);
						fin >> composition[k].Opus;
						impl[j].composition.emplace_back(composition[k]);
					}
					lib[i].impl.emplace_back(impl[j]);
				}
			}
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}
};

class ListQuart { // агрегація

public:
	Files file;
	LibraryOfQuartets lib[10];
	LibraryOfQuartets &ListQuart::operator [] (int i) // перезавантаження оператора []
	{
		if (i < 0 || i> 9) {
			cout << "Boundary Error\n";
			exit(1);
		}
		else {
			return lib[i];
		}
	}

	// template <typename Type> // приклад шаблону фукнції
	// Type avarage(int index, Type a[index]) {
	// 	 double result = 0;
	// 	 for (int i = 0; i < lib[index].sizeImpl; i++) {
	// 	 	result += a[index];
	// 	 }
	// 	 return result / lib[index].sizeImpl;
	// }

	double averageCountImpl(int index) { // порахувати кількість виконань
		double result = 0;
		for (int i = 0; i < lib[index].sizeImpl; i++) {
			result += lib[index].impl[i].sizeCopmosit;
		}
		return result / lib[index].sizeImpl;
	}
	double timeImpl(int index) {
		double result = 0;
		for (int i = 0; i < lib[index].sizeImpl; i++) {
			result += lib[index].impl[i].time;
		}
		return result / lib[index].sizeImpl;
	}

	void show() { // виведення даних у вигляді таблиці
		string temp = string(80, '-');
		printf("%s\n", temp.c_str());
		printf("|%-40s|%-20s|%-20s|\n", "Quartet", "Av. count perfom.", "Av. time of performance");
		printf("%s\n", temp.c_str());
		for (int i = 0; i < 10; i++) {
			double impl = averageCountImpl(i);
			double time = timeImpl(i);
			printf("|%-30s|%-20f|%-20f|\n", lib[i].QuartOfPerf.printQuartet().c_str(), impl, time);
			printf("%s\n", temp.c_str());
		}
	}
	void clear() {
		system("cls");
	}
	void inputPerfomers(ListQuart &list) {
		string name, lastName, type;
		for (int i = 0; i < 1; i++) {
			cout << "Enter quartet of musicians #" << i + 1 << ": " << endl;
			QuartetOfPerformers quartet;
			for (int j = 0; j < 4; j++) {
				cout << "Enter musician #" << j + 1 << ": " << endl;
				cout << "Enter first name: ";
				cin >> name;
				cout << "Enter last name: ";
				cin >> lastName;
				cout << "Enter musical instrument: ";
				cin >> type;
				Artist temp(name, lastName, type);
				temp.setBirth();
				quartet.setPerform(j, temp);
				clear();
			}
			list[i].QuartOfPerf = quartet;
			int size;
			int sizeComposition;
			cout << "Enter number of performances: ";
			cin >> size;
			// list[i].sizeImpl = size;
			list[i].setSize(size);
			vector<Performance> impl(size);
			for (int j = 0; j < size; j++) {
				cout << "Enter performance #" << j + 1 << ": " << endl;
				cout << "Enter the duration of performance: ";
				cin >> impl[j].time;
				impl[j].implemen.setDate();
				impl[j].performances = lib[i].QuartOfPerf;
				cout << "Enter duration of composition: ";
				cin >> sizeComposition;
				impl[j].sizeCopmosit = sizeComposition;
				impl[j].setSize(sizeComposition);
				vector <Quartet> composition(sizeComposition);
				for (int k = 0; k < sizeComposition; k++) {
					cout << "Enter composition #" << k + 1 << ": " << endl;
					cout << "Enter composer first name: ";
					cin >> name;
					cout << "Enter composer last name: ";
					cin >> lastName;
					Person temp(name, lastName);
					composition[k].composer = temp;
					cout << "Enter composer birthdate:" << endl;
					composition[k].composer.setBirth();
					cout << "Enter data of creation of the composition:" << endl;
					composition[k].dateCreate.setDate();
					cout << "Enter opus: ";
					cin >> composition[k].Opus;
					clear();
					impl[j].composition.emplace_back(composition[k]);
				}
				list[i].impl.emplace_back(impl[j]);
			}
		}
	}
	void search(int key) { // пошук інформації за числовим полем
		int count = 0;
		string temp = string(60, '-');
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < lib[i].sizeImpl; j++) {
				if (key == lib[i].impl[j].time) {
					if (count == 0) {
						printf("%s\n", temp.c_str());
						printf("|%-7s|%-10s|%-40s|\n", "Time", "Data", "Quartet of performers");
						printf("%s\n", temp.c_str());
					}
					int tTemp = lib[i].impl[j].getTime();
					string date = lib[i].impl[j].implemen.printDate();
					string quart = lib[i].impl[j].performances.printQuartet();
					printf("|%-7d|%-10s|%-40s|\n", tTemp, date.c_str(), quart.c_str());
					printf("%s\n", temp.c_str());
					count++;
				}
			}
		}
		cout << "Total number of performances: " << count << endl;
	}

	void search(string key) { // пошук інформації за текстовим полем
		int count = 0;
		string temp = string(60, '-');
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < lib[i].sizeImpl; j++) {
				for (int k = 0; k < lib[i].impl[j].sizeCopmosit; k++) {
					if (key == lib[i].impl[j].composition[k].Opus) {
						if (count == 0) {
							printf("%s\n", temp.c_str());
							printf("|%-10s|%-10s|%-40s|\n", "Opus", "Data", "Composer");
							printf("%s\n", temp.c_str());
						}
						string opus = lib[i].impl[j].composition[k].Opus;
						string date = lib[i].impl[j].composition[k].dateCreate.printDate();
						string composer = lib[i].impl[j].composition[k].composer.getLastName() + " " + lib[i].impl[j].composition[k].composer.getName();
						printf("|%-10s|%-10s|%-40s|\n", opus.c_str(), date.c_str(), composer.c_str());
						printf("%s\n", temp.c_str());
						count++;
					}
				}
			}
		}
	}

};

void delay() {
	system("pause");
}
void clear() {
	system("cls");
}
