class ListQuart { // агрегація

public:
	Files file;
	LibraryOfQuartets lib[10];
	LibraryOfQuartets &ListQuart::operator [] (int i) // перезавантаження оператора []
	{
		if (i < 0 || i> 9) {
			cout << "Boundary Error\n";
			exit(1);
		}
		else {
			return lib[i];
		}
	}

	// template <typename Type> // приклад шаблону фукнції
	// Type avarage(int index, Type a[index]) {
	// 	 double result = 0;
	// 	 for (int i = 0; i < lib[index].sizeImpl; i++) {
	// 	 	result += a[index];
	// 	 }
	// 	 return result / lib[index].sizeImpl;
	// }

	double averageCountImpl(int index) { // порахувати кількість виконань
		double result = 0;
		for (int i = 0; i < lib[index].sizeImpl; i++) {
			result += lib[index].impl[i].sizeCopmosit;
		}
		return result / lib[index].sizeImpl;
	}
	double timeImpl(int index) {
		double result = 0;
		for (int i = 0; i < lib[index].sizeImpl; i++) {
			result += lib[index].impl[i].time;
		}
		return result / lib[index].sizeImpl;
	}

	void show() { // виведення даних у вигляді таблиці
		string temp = string(80, '-');
		printf("%s\n", temp.c_str());
		printf("|%-40s|%-20s|%-20s|\n", "Quartet", "Av. count perfom.", "Av. time of performance");
		printf("%s\n", temp.c_str());
		for (int i = 0; i < 10; i++) {
			double impl = averageCountImpl(i);
			double time = timeImpl(i);
			printf("|%-30s|%-20f|%-20f|\n", lib[i].QuartOfPerf.printQuartet().c_str(), impl, time);
			printf("%s\n", temp.c_str());
		}
	}
	void clear() {
		system("cls");
	}
	void inputPerfomers(ListQuart &list) {
		string name, lastName, type;
		for (int i = 0; i < 1; i++) {
			cout << "Enter quartet of musicians #" << i + 1 << ": " << endl;
			QuartetOfPerformers quartet;
			for (int j = 0; j < 4; j++) {
				cout << "Enter musician #" << j + 1 << ": " << endl;
				cout << "Enter first name: ";
				cin >> name;
				cout << "Enter last name: ";
				cin >> lastName;
				cout << "Enter musical instrument: ";
				cin >> type;
				Artist temp(name, lastName, type);
				temp.setBirth();
				quartet.setPerform(j, temp);
				clear();
			}
			list[i].QuartOfPerf = quartet;
			int size;
			int sizeComposition;
			cout << "Enter number of performances: ";
			cin >> size;
			// list[i].sizeImpl = size;
			list[i].setSize(size);
			vector<Performance> impl(size);
			for (int j = 0; j < size; j++) {
				cout << "Enter performance #" << j + 1 << ": " << endl;
				cout << "Enter the duration of performance: ";
				cin >> impl[j].time;
				impl[j].implemen.setDate();
				impl[j].performances = lib[i].QuartOfPerf;
				cout << "Enter duration of composition: ";
				cin >> sizeComposition;
				impl[j].sizeCopmosit = sizeComposition;
				impl[j].setSize(sizeComposition);
				vector <Quartet> composition(sizeComposition);
				for (int k = 0; k < sizeComposition; k++) {
					cout << "Enter composition #" << k + 1 << ": " << endl;
					cout << "Enter composer first name: ";
					cin >> name;
					cout << "Enter composer last name: ";
					cin >> lastName;
					Person temp(name, lastName);
					composition[k].composer = temp;
					cout << "Enter composer birthdate:" << endl;
					composition[k].composer.setBirth();
					cout << "Enter data of creation of the composition:" << endl;
					composition[k].dateCreate.setDate();
					cout << "Enter opus: ";
					cin >> composition[k].Opus;
					clear();
					impl[j].composition.emplace_back(composition[k]);
				}
				list[i].impl.emplace_back(impl[j]);
			}
		}
	}
	void search(int key) { // пошук інформації за числовим полем
		int count = 0;
		string temp = string(60, '-');
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < lib[i].sizeImpl; j++) {
				if (key == lib[i].impl[j].time) {
					if (count == 0) {
						printf("%s\n", temp.c_str());
						printf("|%-7s|%-10s|%-40s|\n", "Time", "Data", "Quartet of performers");
						printf("%s\n", temp.c_str());
					}
					int tTemp = lib[i].impl[j].getTime();
					string date = lib[i].impl[j].implemen.printDate();
					string quart = lib[i].impl[j].performances.printQuartet();
					printf("|%-7d|%-10s|%-40s|\n", tTemp, date.c_str(), quart.c_str());
					printf("%s\n", temp.c_str());
					count++;
				}
			}
		}
		cout << "Total number of performances: " << count << endl;
	}

	void search(string key) { // пошук інформації за текстовим полем
		int count = 0;
		string temp = string(60, '-');
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < lib[i].sizeImpl; j++) {
				for (int k = 0; k < lib[i].impl[j].sizeCopmosit; k++) {
					if (key == lib[i].impl[j].composition[k].Opus) {
						if (count == 0) {
							printf("%s\n", temp.c_str());
							printf("|%-10s|%-10s|%-40s|\n", "Opus", "Data", "Composer");
							printf("%s\n", temp.c_str());
						}
						string opus = lib[i].impl[j].composition[k].Opus;
						string date = lib[i].impl[j].composition[k].dateCreate.printDate();
						string composer = lib[i].impl[j].composition[k].composer.getLastName() + " " + lib[i].impl[j].composition[k].composer.getName();
						printf("|%-10s|%-10s|%-40s|\n", opus.c_str(), date.c_str(), composer.c_str());
						printf("%s\n", temp.c_str());
						count++;
					}
				}
			}
		}
	}

};
