class QuartetOfPerformers { // клас Квартет Виконавців
	Artist art[4];
public:
	Artist getPerformer(int ind) {
		return art[ind];
	}
	void setPerform(int index, Artist tArt) {
		art[index] = tArt;
	}
	string printQuartet() {
		return art[0].getLastName() + ", " + art[1].getLastName() + ", " + art[2].getLastName() + ", " + art[3].getLastName();
	}
	string writeArtist(int ind) {
		return art[ind].getName() + " " + art[ind].getLastName() + " " + art[ind].getType();
	}
};
