class Person { // клас Особа
protected: // захищені методи - доступні з похідних класів
	string lastName;
	string name;
	Date birthday;
public: // публічні методи
	Person() {}
	Person(string tName, string tLastName) {
		name = tName;
		lastName = tLastName;
	}
	void setLastName(string value) {
		lastName = value;
	}
	void setName(string value) {
		name = value;
	}
	void setBirth() {
		birthday.setDate();
	}
	void setBirth(ifstream& fin) {
		birthday.setDate(fin);
	}
	string getLastName() {
		return lastName;
	}
	string getName() {
		return name;
	}
	Date getDate() {
		return birthday;
	}
};
