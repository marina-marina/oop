class Date { // клас Дата
private: // приватні методи
	string day;
	string month;
	string year;
public: // публічні методи
	void setDay(string value) {
		day = value;
	}
	void setMonth(string value) {
		month = value;
	}
	void setYear(string value) {
		year = value;
	}
	string printDate() {
		return day + " " + month + " " + year;
	}
	void setDate() {
		cout << "Enter day: ";
		do { // обмеження можливих значень при введенні дати
			cin >> day;
			if (atoi(day.c_str()) > 31) {
				cout << "Please enter numbers in range 1-31 for day:";
			}
		} while (atoi(day.c_str()) > 31);
		cout << "Enter month: ";
		do {
			cin >> month;
			if (atoi(month.c_str()) > 12) {
				cout << "Please enter numbers in range 1-12 for month:";
			}
		} while (atoi(month.c_str()) > 12);
		cout << "Enter year: ";
		do {
			cin >> year;
			if (atoi(year.c_str()) < 1000 || atoi(year.c_str()) > 2017) {
				cout << "Year must have 4 digits and be not gretet than current year:";
			}
		} while ((atoi(year.c_str()) < 1000 || atoi(year.c_str()) > 2017));

	}
	void setDate(ifstream& fin) {
		fin >> day;
		fin >> month;
		fin >> year;
	}
};
