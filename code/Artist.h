class Artist : public Person { // клас Артист, успадковування від класу Person
	string type;
public:
	Artist() {
		type = "0";
	}
	Artist(string tName, string tLastName, string tType) : Person(tName, tLastName) {
		type = tType;
	}
	string getType() {
		return type;
	}
	void setType(string value) {
		type = value;
	}
};
