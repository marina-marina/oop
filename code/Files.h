class Files { // клас Файл
	string file = "text.txt";
	string binFile = "binary.txt";
public:
	void write(LibraryOfQuartets lib[]) { // запис даних у текстовий файл
		ofstream fout;
		try
		{
			fout.open("text.txt");
			if (!fout) {
				throw "File cannot be opened.";
			}
			// TODO: WTF? refactor this shit
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 4; j++) {
					fout << lib[0].QuartOfPerf.writeArtist(j) << " "; // TODO: why space here?
					fout << lib[0].QuartOfPerf.getPerformer(j).getDate().printDate() << " ";
				}
				fout << lib[0].sizeImpl << " ";
				for (int j = 0; j < lib[0].sizeImpl; j++) {
					fout << lib[0].impl[j].time << " ";
					fout << lib[0].impl[j].implemen.printDate() << " ";
					fout << lib[0].impl[j].sizeCopmosit << " ";
					for (int k = 0; k < lib[0].impl[j].sizeCopmosit; k++) {
						fout << lib[0].impl[j].composition[k].composer.getName() << " ";
						fout << lib[0].impl[j].composition[k].composer.getLastName() << " ";
						fout << lib[0].impl[j].composition[k].composer.getDate().printDate() << " ";
						fout << lib[0].impl[j].composition[k].dateCreate.printDate() << " ";
						fout << lib[0].impl[j].composition[k].Opus << " ";
					}
				}
			}
			fout.close();
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}

	void writeBin(LibraryOfQuartets lib[]) { // запис даних у бінарний файл
		ofstream fout;
		try
		{
			fout.open("textBin.txt", ios::binary | ios::out); // TODO: why binary? it's a text
			if (!fout) {
				throw "File cannot be opened.";
			}
			LibraryOfQuartets temp;
			for (int i = 0; i < 10; i++) {
				fout.write((char*)&lib[i], sizeof(temp));
			}

			fout.close();
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}
	vector<Performance> impls;
	vector <Quartet> compositions;

	void readBin(LibraryOfQuartets lib[]) { // зчитування даних з бінарного файлу
		ifstream fin;
		try
		{
			fin.open("textBin.txt", ios::binary | ios::in); // TODO: why textBin.txt????
			if (!fin) {
				throw "File cannot be opened.";
			}
			LibraryOfQuartets temp;
			for (int i = 0; i < 10; i++) {
				fin.read((char*)&lib[i], sizeof(temp));
			}


			fin.close();
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}

	void read(LibraryOfQuartets lib[]) { // зчитування даних з текстового файлу
		ifstream fin;
		try
		{
			fin.open("text.txt");
			if (!fin) {
				throw "File cannot be opened.";
			}
			string name, lastName, type;
			for (int i = 0; i < 10; i++) {
				QuartetOfPerformers quartet;
				for (int j = 0; j < 4; j++) {
					fin >> name;
					fin >> lastName;
					fin >> type;
					Artist temp(name, lastName, type);
					temp.setBirth(fin);
					quartet.setPerform(j, temp);
				}
				lib[i].QuartOfPerf = quartet;
				int size;
				int sizeComposition;
				fin >> size;
				lib[i].sizeImpl = size;
				lib[i].setSize(size);
				vector<Performance> impl(size);
				for (int j = 0; j < size; j++) {
					fin >> impl[j].time;
					impl[j].implemen.setDate(fin);
					impl[j].performances = lib[i].QuartOfPerf;
					fin >> sizeComposition;
					impl[j].sizeCopmosit = sizeComposition;
					impl[j].setSize(sizeComposition);
					vector <Quartet> composition(sizeComposition);
					for (int k = 0; k < sizeComposition; k++) {
						fin >> name;
						fin >> lastName;
						Person temp(name, lastName);
						composition[k].composer = temp;
						composition[k].composer.setBirth(fin);
						composition[k].dateCreate.setDate(fin);
						fin >> composition[k].Opus;
						impl[j].composition.emplace_back(composition[k]);
					}
					lib[i].impl.emplace_back(impl[j]);
				}
			}
		}
		catch (char err) { // обробка виключних ситуацій
			cout << err << endl;
		}
	}
};
