class Performer { // клас Виконавець
	Person pers; // агрегація класу Person в клас Performance (композиція)
	Artist art; // агрегація класу Artist в клас Performance (композиція)
public:
	Person getPerson() {
		return pers;
	}
	Artist getArt() {
		return art;
	}
};
