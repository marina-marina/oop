#include <cmath>
#include <iomanip>
#include <time.h>                                 
#include <stdlib.h>
#include <conio.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>

using namespace std;

class Date {
private:
	string day;
	string month;
	string year;
public:
	void setDay(string value) {
		day = value;
	}
	void setMounth(string value){
		month = value;
	}
	void setYear(string value){
		year = value;
	}
	string printDate() {
		return day + " " + month + " " + year;
	}
	void setDate() {
		cout << "Enter day -> ";
		cin >> day;
		cout << "Enter month -> ";
		cin >> month;
		cout << "Enter year-> ";
		cin >> year;
	}
	void setDate(ifstream& fin) {
		fin >> day;
		fin >> month;
		fin >> year;
	}
};//����

class Person {
protected:
	string lastName;
	string name;
	Date bithday;
public:
	Person() {
	
	}
	Person(string tName, string tLastName) {
		name = tName;
		lastName = tLastName;
	}
	void setLastName(string value) {
		lastName = value;
	}
	void setName(string value) {
		name = value;
	}
	void setBith() {
		bithday.setDate();
	}
	void setBith(ifstream& fin) {
		bithday.setDate(fin);
	}
	string getLastName() {
		return lastName;
	}
	string getName() {
		return name;
	}
	Date getDate() {
		return bithday;
	}
};

class Artist : public Person {
	string type;
public:
	Artist() {
		type = "0";
	}
	Artist(string tName, string tLastName, string tType) : Person(tName, tLastName) {
		type = tType;
	}
	string getType() {
		return type;
	}
	void setType(string value) {
		type = value;
	}
};

class Performer {//������
	Person pers;//�����
	Artist art;//����������
public:
	Person getPerson() {
		return pers;
	}
	Artist getArt() {
		return art;
	}

}; 

class QuartetOfPerformers {//������� ������������
	Artist art[4];
public:
	
	Artist getPerformer(int ind) {
		return art[ind];
	}
	void setPerform(int index, Artist tArt) {
		art[index] = tArt;
	}
	string printQuartet() {
		return art[0].getLastName() + ", " + art[1].getLastName() + ", " + art[2].getLastName() + ", " + art[3].getLastName();
	}
	string writeArtist(int ind) {
		return art[ind].getName() + " " + art[ind].getLastName() + " " + art[ind].getType();
	}
};

class Quartet {//�����������
public:
	Person composer;
	string Opus;
	Date dateCreate;
};

class Implementation {
public:
	//Quartet *composition;
	vector<Quartet> composition;
	QuartetOfPerformers perfomanses;
	Date implemen;
	int time;
	int sizeCopmosit;

	void setSize(int size) {
		composition.reserve(size);
	}

	int getTime() {
		return time;
	}
};

class LibraryQuartets {
public:
	vector<Implementation>	impl;
	Quartet quaert;
	QuartetOfPerformers QuartOfPerf;
	int sizeImpl;
	void setSize(int size) {
		impl.reserve(size);
	}
};


class Files {

	string file = "text.txt";
	string binFile = "binary.txt";
public:
	void write(LibraryQuartets lib[]) {
		ofstream fout;
		try
		{
			fout.open("text.txt");
			if (!fout) {
				throw "File not opened";
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 4; j++) {
					fout << lib[0].QuartOfPerf.writeArtist(j) << " ";
					fout << lib[0].QuartOfPerf.getPerformer(j).getDate().printDate() << " ";
				}
				fout << lib[0].sizeImpl << " ";
				for (int j = 0; j < lib[0].sizeImpl; j++) {
					fout << lib[0].impl[j].time << " ";
					fout << lib[0].impl[j].implemen.printDate() << " ";
					fout << lib[0].impl[j].sizeCopmosit << " ";
					for (int k = 0; k < lib[0].impl[j].sizeCopmosit; k++) {
						fout << lib[0].impl[j].composition[k].composer.getName() << " ";
						fout << lib[0].impl[j].composition[k].composer.getLastName() << " ";
						fout << lib[0].impl[j].composition[k].composer.getDate().printDate() << " ";
						fout << lib[0].impl[j].composition[k].dateCreate.printDate() << " ";
						fout << lib[0].impl[j].composition[k].Opus << " ";
					}
				}
			}
			fout.close();
		}
		catch (char err) {
			cout << err << endl;
		}
	}

	void writeBin(LibraryQuartets lib[]) {
		ofstream fout;
		try
		{
			fout.open("textBin.txt", ios::binary | ios::out);
			if (!fout) {
				throw "File not opened";
			}
			LibraryQuartets temp;
			for (int i = 0; i < 10; i++) {
				fout.write((char*)&lib[i], sizeof(temp));
			}
			
			fout.close();
		}
		catch (char err) {
			cout << err << endl;
		}
	}
	vector<Implementation> impls;
	vector <Quartet> compositions;

	void readBin(LibraryQuartets lib[]) {
		ifstream fin;
		try
		{
			fin.open("textBin.txt", ios::binary | ios::in);
			if (!fin) {
				throw "File not opened";
			}
			LibraryQuartets temp;
			for (int i = 0; i < 10; i++) {
				fin.read((char*)&lib[i], sizeof(temp));
			}
			
			
			fin.close();
		}
		catch (char err) {
			cout << err << endl;
		}
	}

	void read(LibraryQuartets lib[]) {
		ifstream fin;
		try
		{
			fin.open("text.txt");
			if (!fin) {
				throw "File not opened";
			}
			string name, lastName, type;
			for (int i = 0; i < 10; i++) {
				QuartetOfPerformers quartet;
				for (int j = 0; j < 4; j++) {
					fin >> name;
					fin >> lastName;
					fin >> type;
					Artist temp(name, lastName, type);
					temp.setBith(fin);
					quartet.setPerform(j, temp);
				}
				lib[i].QuartOfPerf = quartet;
				int size;
				int sizeComposition;
				fin >> size;
				lib[i].sizeImpl = size;
				lib[i].setSize(size);
				vector<Implementation> impl(size);
				for (int j = 0; j < size; j++) {
					fin >> impl[j].time;
					impl[j].implemen.setDate(fin);
					impl[j].perfomanses = lib[i].QuartOfPerf;
					fin >> sizeComposition;
					//lib[i].impl[j].sizeCopmosit = sizeComposition;
					impl[j].sizeCopmosit = sizeComposition;
					impl[j].setSize(sizeComposition);
					vector <Quartet> composition(sizeComposition);
					for (int k = 0; k < sizeComposition; k++) {
						fin >> name;
						fin >> lastName;
						Person temp(name, lastName);
						composition[k].composer = temp;
						composition[k].composer.setBith(fin);
						composition[k].dateCreate.setDate(fin);
						fin >> composition[k].Opus;
						impl[j].composition.emplace_back(composition[k]);
					}
					lib[i].impl.emplace_back(impl[j]);
					//lib[i].impl[j].composition = composition;
				}
				//lib[i].impl = impl;
			}
		}
		catch (char err) {
			cout << err << endl;
		}
	}
};

class ListQuart {
	
public:
	Files file;
	LibraryQuartets lib[10];
	LibraryQuartets &ListQuart::operator [] (int i)
	{
		if (i < 0 || i>9) {
			cout << "Boundary Error\n";
			exit(1);
		}
		else {
			return lib[i];
		}
	}

	//template <typename Type>
	//Type avarage(int index, Type a[index]) {
	//	double result = 0;
	//	for (int i = 0; i < lib[index].sizeImpl; i++) {
	//		result += a[index];
	//	}
	//	return result / lib[index].sizeImpl;
	//}
	double averageCountImpl(int index) {//��������� ���������� ����������
		double result = 0;
		for (int i = 0; i < lib[index].sizeImpl; i++) {
			result += lib[index].impl[i].sizeCopmosit;
		}
		return result / lib[index].sizeImpl;
	}
	double timeImpl(int index) {
		double result = 0;
		for (int i = 0; i < lib[index].sizeImpl; i++) {
			result += lib[index].impl[i].time;
		}
		return result / lib[index].sizeImpl;

	}

	void show() {
		string temp = string(80, '-');
		printf("%s\n", temp.c_str());
		printf("|%-40s|%-20s|%-20s|\n", "Quartet", "Av. count perfom.", "Av. time perfom");
		printf("%s\n", temp.c_str());
		for (int i = 0; i < 10; i++) {
			double impl = averageCountImpl(i);
			double time = timeImpl(i);
			printf("|%-30s|%-20f|%-20f|\n", lib[i].QuartOfPerf.printQuartet().c_str(), impl, time);
			printf("%s\n", temp.c_str());
		}
	}
	void clear() {
		system("cls");
	}
	void inputPerfomers(ListQuart &list) {
		string name, lastName, type;
		for (int i = 0; i < 1; i++) {
			cout << "Enter " << i + 1 << "quartet executors ->" << endl;
			QuartetOfPerformers quartet;
			for (int j = 0; j < 4; j++) {
				cout << "Enter" << j + 1 << " musicanes -> " << endl;
				cout << "Enter name -> ";
				cin >> name;
				cout << "Enter lastname -> ";
				cin >> lastName;
				cout << "Enter musical instrument -> ";
				cin >> type;
				Artist temp(name,lastName,type);
				temp.setBith();
				quartet.setPerform(j, temp);
				clear();
			}
			list[i].QuartOfPerf = quartet;
			int size;
			int sizeComposition;
			cout << "Enter count perfomanses -> ";
			cin >> size;
			list[i].sizeImpl = size;
			list[i].setSize(size);
			vector<Implementation> impl(size);
			for (int j = 0; j < size; j++) {
				cout << "Enter " << j + 1 << " speech -> " << endl;
				cout << "Enter duration speech -> ";
				cin >> impl[j].time;
				impl[j].implemen.setDate();
				impl[j].perfomanses = lib[i].QuartOfPerf;
				cout << "Enter count composition -> ";
				cin >> sizeComposition;
				impl[j].sizeCopmosit = sizeComposition;
				impl[j].setSize(sizeComposition);
				vector <Quartet> composition(sizeComposition);
				for (int k = 0; k < sizeComposition; k++) {
					cout << "Enter " << k+1 << " composition -> " << endl;
					cout << "Enter name compositor -> ";
					cin >> name;
					cout << "Enter lastname compositor -> ";
					cin >> lastName;
					Person temp(name, lastName);
					composition[k].composer = temp;
					cout << "Enter bithday compositor - >" << endl;
					composition[k].composer.setBith();
					cout << "Enter data create composition ->" << endl;
					composition[k].dateCreate.setDate();
					cout << "Enter opus - >";
					cin >> composition[k].Opus;
					clear();
					impl[j].composition.emplace_back(composition[k]);
				}
				//copy(composition.begin(),   // �������� ������ �������
				//	composition.end(),     // �������� ����� �������
				//	impl[j].composition.begin()
				//);
				//impl[j].composition = composition;
				list[i].impl.emplace_back(impl[j]);
			}
			//copy(impl.begin(),   // �������� ������ �������
			//	impl.end(),     // �������� ����� �������
			//	lib[i].impl.begin()
			//);
			//lib[i].impl = impl;
		}
	}
	void search(int key) {
		int count = 0;
		string temp = string(60, '-');
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < lib[i].sizeImpl; j++) {
				if (key == lib[i].impl[j].time) {
					if (count == 0) {
						printf("%s\n",temp.c_str());
						printf("|%-7s|%-10s|%-40s|\n", "Time", "Data", "Quartet executores");
						printf("%s\n", temp.c_str());
					}
					int tTemp = lib[i].impl[j].getTime();
					string date = lib[i].impl[j].implemen.printDate();
					string quart = lib[i].impl[j].perfomanses.printQuartet();
					printf("|%-7d|%-10s|%-40s|\n", tTemp, date.c_str(), quart.c_str());
					printf("%s\n", temp.c_str());
					count++;
				}
			}
		}
		cout << "All number of speeches -> " << count << endl;

	}

	void search(string key) {
		int count = 0;
		string temp = string(60, '-');
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < lib[i].sizeImpl; j++) {
				for (int k = 0; k < lib[i].impl[j].sizeCopmosit; k++) {
					if (key == lib[i].impl[j].composition[k].Opus) {
						if (count == 0) {
							printf("%s\n", temp.c_str());
							printf("|%-10s|%-10s|%-40s|\n", "Opus", "Data", "Compositor");
							printf("%s\n", temp.c_str());
						}
						string opus = lib[i].impl[j].composition[k].Opus;
						string date = lib[i].impl[j].composition[k].dateCreate.printDate();
						string compositor = lib[i].impl[j].composition[k].composer.getLastName() + " " + lib[i].impl[j].composition[k].composer.getName();
						printf("|%-10s|%-10s|%-40s|\n", opus.c_str(), date.c_str(), compositor.c_str());
						printf("%s\n", temp.c_str());
						count++;
					}
				}
			}
		}
	}
	
};

void delay() {
	system("pause");
}
void clear() {
	system("cls");
}

int main()
{
	setlocale(LC_ALL, "Russian");
	int key;
	int temp;
	ListQuart list;
	do {
		cout << "Enter 1 for input data -> " << endl;
		cout << "Enter 2 for output data -> " << endl;
		cout << "Enter 3 for �� write data in file -> " << endl;
		cout << "Enter 4 for read data from file -> " << endl;
		cout << "Enter 5 for search data -> " << endl;
		cout << "Enter 0 for exit -> " << endl;
		cout << "Make your choice -> ";
		cin >> key;
		switch (key) {
		case 1:
			clear();
			list.inputPerfomers(list);
			delay();
			break;
		case 2:
			clear();
			list.show();
			delay();
			break;
		case 3:
			clear();
			cout << "Enter 1 for write data in txt -> " << endl;
			cout << "Enter 2 for write data in binary txt -> " << endl;
			cin >> temp;
			if (temp == 1) {
				list.file.write(list.lib);
			}
			else {
				list.file.writeBin(list.lib);
			}
			delay();
			break;
		case 4:
			clear();
			cout << "Enter 1 for read data from txt -> " << endl;
			cout << "Enter 2 for read data from binary txt -> " << endl;
			cin >> temp;
			if (temp == 1) {
				list.file.read(list.lib);
			}
			else {
				list.file.readBin(list.lib);
			}
			delay();
			break;
		case 5:
			clear();
			cout << "Enter 1 for search speech for time -> " << endl;
			cout << "Enter 2 for search composition for opus -> " << endl;
			cin >> temp;
			if (temp == 1) {
				int time;
				cout << "Enter time -> ";
				cin >> time;
				list.search(time);
			}
			else {
				string opus;
				cout << "Enter opus -> ";
				cin >> opus;
				list.search(opus);
			}
			delay();
			break; 
		case 0:
			break;
		default:
			cout << "Repeat your choice -> ";
			break;
		}
	} while (key != 0);

	return 0;
}
