#include "lab03.h"
#include <iostream>
#include <stdio.h>

Educator::Educator()
{
	char* buffer = "unknown";
	int n = sizeof(buffer);
	this->description = new (char) (n+1);
	strcpy(this->description, buffer);
	printf("Default constructor \n");
}
Educator::Educator(char* surname, char* name, int day, int month, int year, post position, int experience_months, int experience_years, int teaching_load, char* desc) {
	printf("Initialization constructor \n");
	if(strlen(surname) > 10) this->surname = 0;
	else this->surname = surname;
	if(strlen(name) > 10) this->name =0;
	else this->name = name;
	if(day < 1 || day > 31) this->day = 0;
	else this->day = day;
	if(month < 1 || month > 12) this->month = 0;
	else this->month = month;
	if(year < 1900 || year > 2015) this->year = 0;
	else this->year = year;
	if(position != 1 && position != 2 && position != 3 && position != 4 && position != 5) this->position = assistant;
	else this->position = position;
	if(experience_years < 1 || experience_years > 60) this->experience_years = 0;
	else this->experience_years = experience_years;
	if(experience_months < 1 ) this->experience_months = 0;
	else this->experience_months = experience_months;
	if(teaching_load <= 0 || teaching_load > 115200) this->teaching_load = 0;
	else this->teaching_load = teaching_load;
	int n = sizeof(desc);
	this->description = new (char)(n+1);
	strcpy(this->description, desc);
}

Educator::Educator(const Educator&  c) {
	printf("Copy constructor \n");
	this->surname = c.surname;
	this->name = c.name;
	this->day = c.day;
	this->month = c.month;
	this->year = c.year;
	this->position = c.position;
	this->experience_months = c.experience_months;
	this->experience_years = c.experience_years;
	this->teaching_load = c.teaching_load;
	int n = sizeof(c.description);
	this->description = new (char) (n+1);
	strcpy(this->description, c.description);
}

Educator::~Educator()
{
	printf("destructor \n");
	if(!description) free(description);
}

void Educator::setSurname() {
	char* input = new char[100];
	do {
		printf("Input surname (to 10 chars): ");
		scanf("%s", input);
	} while (strlen(input) > 10);
	surname = input;
}

void Educator::setName() {
	char* input = new char[100];
	do {
		printf("Input name (to 10 chars): ");
		scanf("%s", input);
	} while (strlen(input) > 10);
	name = input;
}

void Educator::setDate() {
	char input[20];
	printf("Input date: \n");
	do {
		printf("\tInput day from 1 to 31: ");
		scanf("%s", input);
		day = atoi(input);
	} while (day < 1 || day > 31);

	do {
		printf("\tInput month from 1 to 12: ");
		scanf("%s", input);
		month = atoi(input);
	} while (month < 1 || month > 12);

	do {
		printf("\tInput year from 1900 to 2015: ");
		scanf("%s", input);
		year = atoi(input);
	} while (year < 1900 || year > 2015);
}

void Educator ::setPost() {
	int choice = 0;
	do {
	printf("Please, enter your post 1-4: 1 - assistant, 2 - lecture, 3 - senior_lecture, 4 - docent, 5 - professor \n");
	scanf("%d",&choice);
	fflush(stdin);
	switch(choice) {
		case 1: position = assistant; break;
		case 2: position = lecture; break;
		case 3: position = senior_lecture; break;
		case 4: position = docent; break;
		case 5: position = professor; break;
		default : printf("Please, enter values from 1 to 5 \n"); break;
	};
} while (choice != 1 && choice != 2 && choice != 3 && choice != 4 && choice != 5);
}

void Educator::setExperience() {
	printf("Input teaching experience: \n");

	char input[20];
	do {
		printf("\tInput the number of years from 1 to 60: ");
		scanf("%s", input);
		experience_years = atoi(input);
	} while (experience_years < 1 || experience_years > 60);


	do {
		printf("\tInput the number of months from 1 to 12: ");
		scanf("%s", input);
		experience_months = atoi(input);
		if (experience_months >= 12) {
			experience_years += experience_months / 12;
			experience_months = experience_months % 12;
		}
	} while (experience_months < 1);

	
}

void Educator::setTeaching_load() {
	char input[20];
	do {
		printf("Input the total number of hours teaching load: ");
		scanf("%s", input);
		teaching_load = atoi(input);
	} while (teaching_load <= 0 || teaching_load > 115200);
}

int Educator::getExperience_years() {
	return experience_years;
}

int Educator::getExperience_months() {
	return experience_months;
}

char* Educator::getPost() {
	switch(position) {
		case assistant: return "assistant"; break;
		case lecture: return "lecture"; break;
		case senior_lecture: return "senior_lecture"; break;
		case docent: return "docent"; break;
		case professor: return "professor"; break;
		default : return 0;
	};
}

int Educator::getTeaching_load() {
	return teaching_load;
}

int Educator::getYear() {
	return year;
}

int Educator::getMonth() {
	return month;
}

int Educator::getDay() {
	return day;
}

char* Educator::getSurname() {
	return surname;
}

char* Educator::getName() {
	return name;
}

char* Educator::getDescription() {
	return description;
}

Educator Educator::operator + (const Educator& d)
{
	Educator addition(*this);
	teaching_load = teaching_load + d.teaching_load;
	return *this;
}

Educator::operator int()
{
	int experience = experience_years * 12 + experience_months;
	return experience;
	//return (int)teaching_load;
}
