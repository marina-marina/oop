#pragma once
#include <iostream>
#include <stdio.h>

enum post {assistant, lecture, senior_lecture, docent, professor};

class Educator {
	private:
		char* description;
		char* surname;
		char* name;
		int year, month, day;
		post position;
		int experience_years;
		int experience_months;
		int teaching_load;
	public:
		char* getDescription();
		void setSurname();
		void setName();
		void setDate();
		void setPost();
		void setExperience();
		void setTeaching_load();
		int getTeaching_load();
		int getExperience_years();
		int getExperience_months();
		char* getPost();
		int getYear();
		int getMonth();
		int getDay();
		char* getName();
		char* getSurname();

		 Educator();
		 Educator(char*, char*, int, int, int, post, int, int, int, char*);
		 Educator(const Educator&);
		 ~Educator();

		 Educator operator+(const Educator&);
		 operator int();

		 bool operator > (const Educator& d)
		 {
			 if(teaching_load > d.teaching_load)
			 {
				 return true;
			 }
			 else
			 {
				 return false;
			 }
		 }
};

