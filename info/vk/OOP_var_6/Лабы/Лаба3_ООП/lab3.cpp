#include "lab03.h"

void output(Educator** d,int k) {
	printf("_________________________________________________________________________________________________________\n");
	printf("|    Surname  |    Name      |     Date     |    Post     |  Experience  | Teaching load |     Destr    |\n");
	printf("|-------------|--------------|--------------|-------------|--------------|---------------|--------------|\n");
	for (int i = 0; i < k; i++) {
	printf("|%-13s|%-14s|%2d.%2d.%8d|%13s|%11d.%2d|%15d|%14s|\n",d[i]->getSurname(), d[i]->getName(), d[i]->getDay(), d[i]->getMonth(), d[i]->getYear(), d[i]->getPost(), d[i]->getExperience_years(), d[i]->getExperience_months(), d[i]->getTeaching_load(), d[i]->getDescription());
	printf("|-------------|--------------|--------------|-------------|--------------|---------------|--------------|\n");
	}
}
void output1(Educator* d) {
	printf("_________________________________________________________________________________________________________\n");
	printf("|    Surname  |     Name     |     Date     |    Post     |  Experience  | Teaching load |     Destr    |\n");
	printf("|-------------|--------------|--------------|-------------|--------------|---------------|--------------|\n");
	printf("|%-13s|%-14s|%2d.%2d.%8d|%13s|%11d.%2d|%15d|%14s|\n",d->getSurname(), d->getName(), d->getDay(), d->getMonth(), d->getYear(), d->getPost(), d->getExperience_years(), d->getExperience_months(), d->getTeaching_load(), d->getDescription());
	printf("|-------------|--------------|--------------|-------------|--------------|---------------|--------------|\n");
	}

void enter(Educator* d) {
	d->setSurname();
	d->setName();
	d->setDate();
    d->setPost();
	d->setExperience();
	d->setTeaching_load();
}

int main() {

	int quantity;
	int j = 0;
	char input[20];
	char b;
	char* d;

	Educator initialize("Doroshchuk", "Dasha", 9, 6, 1997, assistant, 5, 7, 3256, "teacher");
	Educator next(initialize);
	Educator copy(initialize);

	printf("Print initialized file \n");
	output1(&initialize);

	printf("Print copied file \n");
	output1(&copy);

	printf("Print added file \n");
	next = next + initialize;
	output1(&next);
	printf("Bool %d \n", next > initialize);
	printf("Conversation %d \n", (int)initialize);

	do {
	printf("Input a number of the object: ");
	scanf("%s", input);
	quantity = atoi(input);
	} while(quantity < 1);
	Educator* teacher = new Educator[quantity];

	do {
		printf("\nSelect one of the points:\n 1 - Input\n 2 - Output\n 3 - Exit\n");
		scanf("%s", input);
		j = atoi(input);
		if ((j <= 0) || (j > 3)) printf("Incorrect number. Please try again\n");
		switch (j) {
		case 1:
			for(int i = 0; i < quantity; i++)
			enter(&teacher[i]);
			break;
		case 2:
				output(&teacher,quantity);
			break;
		}
	} while (j != 3);
	return 0;
}