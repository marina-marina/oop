
#include <iostream>
#include <string>
#include <cstring>
#include <stdio.h>

class Educator {
	private:
		char surname[100];
		char name[10];
		int year, month, day;
		enum post {assistant, lecture, senior_lecture, docent, professor};
		post position;
		int experience_years;
		int experience_months;
		int teaching_load;
	public:
		void input();
		void setSurname();
		void setName();
		void setDate();
		void setPost();
		void setExperience();
		void setTeaching_load();
		int getTeaching_load();
		int getExperience_years();
		int getExperience_months();
		char* getPost();
		int getYear();
		int getMonth();
		int getDay();
		char* getName();
		char* getSurname();
};

void Educator::input() {
	setSurname();
	setName();	
	setDate();
	setPost();
	setExperience();
	setTeaching_load();
}

void Educator::setSurname() {
	do {
		printf("Input surname (to 10 chars): ");
		scanf("%s", surname);
	} while (strlen(surname) > 10);
}

void Educator::setName() {
	do {
		printf("Input name (to 10 chars): ");
		scanf("%s", name);
	} while (strlen(name) > 10);
}

void Educator::setDate() {
	char input[20];
	printf("Input date: \n");
	do {
		printf("\tInput day from 1 to 31: ");
		scanf("%s", input);
		day = atoi(input);
	} while (day < 1 || day > 31);

	do {
		printf("\tInput month from 1 to 12: ");
		scanf("%s", input);
		month = atoi(input);
	} while (month < 1 || month > 12);

	do {
		printf("\tInput year from 1900 to 2015: ");
		scanf("%s", input);
		year = atoi(input);
	} while (year < 1900 || year > 2015);
}

void Educator ::setPost() {
	int choice = 0;
	do {
	printf("Please, enter your post 1-4: 1 - assistant, 2 - lecture, 3 - senior_lecture, 4 - docent, 5 - professor \n");
	scanf("%d",&choice);
	fflush(stdin);
	switch(choice) {
		case 1: position = assistant; break;
		case 2: position = lecture; break;
		case 3: position = senior_lecture; break;
		case 4: position = docent; break;
		case 5: position = professor; break;
		default : printf("Please, enter values from 1 to 5 \n"); break;
	};
} while (choice != 1 && choice != 2 && choice != 3 && choice != 4 && choice != 5);
}

void Educator::setExperience() {
	char input[20];
	printf("Input teaching experience: \n");
	do {
		printf("\tInput the number of months from 1 to 12: ");
		scanf("%s", input);
		experience_months = atoi(input);
	} while (experience_months < 1 || experience_months > 12);

	do {
		printf("\tInput the number of years from 1 to 60: ");
		scanf("%s", input);
		experience_years = atoi(input);
	} while (experience_years < 1 || experience_years > 60);
}

void Educator::setTeaching_load() {
	char input[20];
	do {
		printf("Input the total number of hours teaching load: ");
		scanf("%s", input);
		teaching_load = atoi(input);
	} while (teaching_load <= 0 || teaching_load > 115200);
}

int Educator::getExperience_years() {
	return experience_years;
}

int Educator::getExperience_months() {
	return experience_months;
}

char* Educator::getPost() {
	switch(position) {
		case assistant: return "assistant"; break;
		case lecture: return "lecture"; break;
		case senior_lecture: return "senior_lecture"; break;
		case docent: return "docent"; break;
		case professor: return "professor"; break;
	};
}

int Educator::getTeaching_load() {
	return teaching_load;
}

int Educator::getYear() {
	return year;
}

int Educator::getMonth() {
	return month;
}

int Educator::getDay() {
	return day;
}

char* Educator::getSurname() {
	return surname;
}

char* Educator::getName() {
	return name;
}


int main()
{
	int j = 0;
	char input[20];
	int quantity;
	//Educator teacher;
	do{
	printf("Input a number of the object: ");
	scanf("%s", input);
	quantity = atoi(input);
	}while(quantity < 1);
	Educator* teacher = new Educator[quantity];
	do {
		printf("\nSelect one of the points:\n 1 - Input\n 2 - Output\n 3 - Exit\n");
		scanf("%s", input);
		j = atoi(input);
		if ((j <= 0) || (j > 3)) printf("Incorrect number. Please try again\n");
		switch (j) {
		case 1:
			for(int i = 0; i < quantity; i++)
			teacher[i].input();
			break;
		case 2:
			for(int i = 0; i < quantity; i++)
			{
			printf("The data  about of the object #");
			printf("%d \n", i + 1);
			printf("-------------------------------------------- \n");
			printf("|\t Surname         |  %14s | \n",teacher[i].getSurname());
			printf("|\t Name            |  %14s | \n",teacher[i].getName());
			printf("|\t Date            |  %8d.%2d.%2d | \n",teacher[i].getYear(),teacher[i].getMonth(), teacher[i].getDay());
			printf("|\t Post            |  %14s | \n",teacher[i].getPost());
			printf("|\t Experience      |  %11d.%2d | \n",teacher[i].getExperience_years(),teacher[i].getExperience_months());
			printf("|\t Teaching load   |  %14d | \n",teacher[i].getTeaching_load());
			printf("-------------------------------------------- \n");
			}
			break;
		}
	} while (j != 3);
	return 0;
}