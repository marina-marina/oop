#include <iostream>
#include <cmath>
#include<math.h>
#include <stdio.h>
#include<conio.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	double factorial = 1, f_result, sum = 0, h = 0.05, E = 0, a, b, eps = 0.001, fact;
	double Fx, Fx_t;
	int n = 1;
	double E0;
	double Bi[12] = {0, 1/6, 1/30, 1/42, 1/30, 5/66, 691/2730, 7/6, 3617/510, 43867/798, 174611/330, 854513/138};
	printf("____________________________________________________________\n\n");
	printf("|   x   |     Fx      |   Fx_t      |    f_result  |    N  |\n");
	printf("|__________________________________________________________|\n\n");
	int i=1;
	factorial = i * 2;
	a = 2 * i;
	b = a - 1;
	E0 = (pow(2,a) * Bi[0] * pow(0,b)) / factorial;
	Fx=0;
	Fx_t = E0;
	f_result = 100;
	printf("| %6.2f|%13.8f|%13.8f|%14.8f|",0,Fx,Fx_t,f_result);
	printf("%7d|\n",1);
	for(double x = 0.05; x <= 1.05; x += h)
	{
		n = 0;
		for(int i = 1;; i++)
		{
			factorial = i * 2;
			fact = factorial;
			for(int m = 1; m < fact; m++)
			{
				factorial = factorial * m;
			}
			a = 2 * i;
			b = a - 1;
			E = (pow(2,a) * Bi[i] * pow(x,b)) / factorial;
			n++;
			if ((fabs(E) < eps) && (fabs(E0)<eps)) break;
			sum += E;
			E0 = E;
		}
		Fx = (1 / x) - sum;
		Fx_t = cos(x) / sin(x);
		f_result = (Fx - Fx_t) * 100 / Fx;
		printf("| %6.2f|%13.8f|%13.8f|%14.8f|%7d|\n",x,Fx,Fx_t,f_result,n);
		sum=0;
	}
	printf("|__________________________________________________________|\n\n");
	_getch();
	return 0;
}