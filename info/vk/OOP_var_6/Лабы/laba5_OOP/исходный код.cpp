#include<iostream>
#include<stdio.h>
#include <fstream>
using namespace std;
enum post {assistant, lecture, senior_lecture, docent, professor,undefined};

class Date {
private:
	int year;
	int month;
	int day;

public:
	void setYear(int y1){year = y1;}
	int getYear(){return year;}
	void setMonth(int m1){month = m1;}
	int getMonth(){return month;}
	void setDay(int d1){day = d1;}
	int getDay(){return day;}
	Date() {
		year = 1950;
		month = 0;
		day = 0;
	}
	Date(int year_, int month_, int day_){
		year = year_;
		month = month_;
		day = day_;
	}
};

class Position {
private:
	post job_title;
public:
	char* getJob_Title() {
		switch (job_title) {
			case assistant: return "assistant"; break;
			case lecture: return "lecture"; break;
			case senior_lecture: return "senior_lecture"; break;
			case docent:return "docent"; break;
			case professor:return "professor"; break;
			case undefined:return "undefined"; break;
			default:return "undefined"; break;
		};
	}
	void setJob_Title(post teacher) {
		job_title = teacher;
	}
	Position(){
		job_title = undefined;
	}
	Position(post teacher_){
		job_title = teacher_;
	}
};

class Subject {
private:
	char title[50];
	int hours;
	int lectureHours;
	int practicalHours;
	int remainderHours;
public:
	void setTitle(char title_1[]){ strcpy(title,title_1);}
	char* getTitle(){ return title;}
	void setHours(int h1){hours = h1;}
	int getHours(){return hours;}
	void setLectureHours(int l_h1){lectureHours = l_h1;}
	int getLectureHours(){return lectureHours;}
	void setPracticalHours(int p_h1){practicalHours = p_h1;}
	int getPracticalHours(){return practicalHours;}
	void setRemainderHours(int r_h1){remainderHours = r_h1;}
	int getRemainderHours(){return remainderHours;}

	Subject(char* title_,int hours_,int lectureHours_,int practicalHours_,int remainderHours_) {
		strcpy(title,title_);
		hours = hours_;
		lectureHours = lectureHours_;
		practicalHours = practicalHours_;
		remainderHours = hours - (lectureHours + practicalHours);
	}

	Subject() {
		strcpy(title,"Unknown");
		hours = 0;
		lectureHours = 0;
		practicalHours = 0;
		remainderHours = hours - (lectureHours + practicalHours);
	}

	
friend istream& operator >> (istream& is, Subject &operation) {
int temp;
char temp1[20];

cout << "Please, enter your title: " << endl;
is >> temp1;
operation.setTitle(temp1);

cout << "Please, enter the total number of hours: " << endl;
is >> temp;
operation.setHours(temp);

cout << "Please, enter the number of lecture hours: " << endl;
is >> temp;
operation.setLectureHours(temp);

cout << "Please, enter the number of practical hours: " << endl;
is >> temp;
operation.setPracticalHours(temp);

cout << "Please, enter the remainder of hours(the number of independent work): " << endl;
is >> temp;
operation.setRemainderHours(temp);

return is;
}

friend ostream& operator << (ostream& os, Subject &operation){
	os << "Title: " << operation.getTitle() << endl;
	os << "Hours: " << operation.getHours() << endl;
	os << "Lecture hours: " << operation.getLectureHours() << endl;
	os << "Practical hours: " << operation.getPracticalHours() << endl;
	os << "Remainder hours: " << operation.getRemainderHours() << endl;
	
	return os;
	}
};

class Person: public Date{
private:
	char surname[20];
	char name[20];
public:
	void setSurname(char surname_1[]){ strcpy(surname,surname_1);};
	void setName(char name_1[]){ strcpy(name, name_1);};
	char* getSurname(){ return surname;};
	char* getName(){ return name;};

	Person(char* surname_, char* name_,int day_, int month_, int year_): Date( day_,  month_,  year_) {
		strcpy(surname,surname_);
		strcpy(name,name_);
	}
	Person () : Date(){
		strcpy(surname, "Unknown1");
		strcpy(name, "Unknown1");
	}
};

class Teacher: public Person, public Position {
private:
	Subject* list;
	Subject* temp;
	int Counting;
	double summ;
public:
	friend ostream& operator << (ostream& os, Teacher &operation){
	os << "Name: " << operation.getName() << endl;
	os << "Surname: " << operation.getSurname() << endl;
	os << "Day: " << operation.getDay() << endl;
	os << "Month: " << operation.getMonth() << endl;
	os << "Year: " << operation.getYear() << endl;
	os << "Position: " << operation.getJob_Title() << endl;
	return os;
	}

friend istream& operator >> (istream& is, Teacher &operation) {
int temp;
char temp1[20];

cout << "Please, enter your name: " << endl;
is >> temp1;
operation.setName(temp1);

cout << "Please, enter your surname: " << endl;
is >> temp1;
operation.setSurname(temp1);

cout << "Please, enter your birthday's day: " << endl;
is >> temp;
operation.setDay(temp);

cout << "Please, enter your birthday's month: " << endl;
is >> temp;
operation.setMonth(temp);

cout << "Please, enter your birthday's year: " << endl;
is >> temp;
operation.setYear(temp);

//SWITCH
do{
cout << "Please, enter your position(1 - assistant, 2 - lecture, 3 - senior_lecture, 4 - docent, 5 - professor): " << endl;
is >> temp1;
temp = atoi(temp1);
}while(temp < 1 || temp > 5);

switch(temp)
{
	case 1: operation.setJob_Title(assistant); break;
	case 2: operation.setJob_Title(lecture); break;
	case 3: operation.setJob_Title(senior_lecture); break;
	case 4: operation.setJob_Title(docent); break;
	case 5: operation.setJob_Title(professor); break;
}
//SWITCH (end)

return is;
}

	Teacher(char* surname_, char* name_,int day_, int month_, int year_, post job_title_,const int C): Person( surname_, name_, day_,  month_,  year_), Position( job_title_){
		Counting = C;
		list = new Subject[Counting];
	}

	Teacher(): Person(), Position(){
		Counting = 1;
		list = new Subject[Counting];
	}

	Teacher(int C): Person(), Position(){
		Counting = C;
		list = new Subject[Counting];
	}

	void inputList()
	{
		for (int i = 0; i < Counting; i++)
		{
			cin >> list[i];
		}
	}

	void inputObj(char* title_,int hours_,int lectureHours_,int practicalHours_,int remainderHours_,int i)
	{
		Subject temp(title_,hours_,lectureHours_,practicalHours_,remainderHours_);
		list[i] = temp;
	}

	void addToList(int position)
	{
		Subject* t;
		t = new Subject[Counting + 1];
		Subject p;
		cin >> p;
		for (int i = 0; i < position; i++)
			{
				t[i] = list[i];
			}
		t[position] = p;
		for (int i = position+1; i < Counting; i++)
			{
				t[i] = list[i];
			} 
		Counting++;
		delete[] list;
		list = t; 
	}
	void removeFromList()
	{
		temp = new Subject[Counting - 1];
		Teacher temp1;
		cin >> temp1;
		for (int i = 0; i < Counting-1; i++)
		{
			temp[i] = list[i];
		}
		Counting--;
		list = temp;
	}

	int* getAverageNumberOfLectureH() {
		int* arr = new int[Counting];
		for (int i = 0; i < Counting; i++) {
			arr[i] = list[i].getLectureHours();
		}
		return arr;
	}

	double* getAverageNumberOfPracticalH() {
		double* arr = new double[Counting];
		for (int i = 0; i < Counting; i++) {
			arr[i] = list[i].getLectureHours() / list[i].getPracticalHours();
		}
		return arr;
	}

	int getCounting() {
		return Counting;
	}

	Subject* getMyList()
	{
		return list;
	}
};

template <typename T>
T calculating(T* mass, int n) {
	T sum = 0;
	
	for (int i = 0; i < n; i++)
		sum += mass[i];
	sum = sum / n;
	return sum;
};


int main()
{
	Teacher teach(10);
	teach.inputObj("Math",50,25,15,10,0);
	teach.inputObj("History",60,25,25,10,1);
	teach.inputObj("Biology",50,20,25,5,2);
	teach.inputObj("English",50,15,15,20,3);
	teach.inputObj("Ukrainian",52,27,18,17,4);
	teach.inputObj("Chemistry",48,13,25,10,5);
	teach.inputObj("Phisica",53,21,21,12,6);
	teach.inputObj("FP",55,25,27,3,7);
	teach.inputObj("Teor Ver",50,25,23,2,8);
	teach.inputObj("OOP",55,29,21,5,9);
	for (int i = 1; i < 10; i++) {
		cout << "Subject # " << i << endl;
		cout << teach.getMyList()[i] << endl;
	}
	cout << endl;
	
	int* ab1 = teach.getAverageNumberOfLectureH();
	int abc1 = calculating(ab1,teach.getCounting());
	cout << "The Average number of lecture hours: " << endl;
	cout << abc1 << endl;
	double* ab = teach.getAverageNumberOfPracticalH();
	double abc = calculating(ab,teach.getCounting());
	cout << "The average ratio of LH to PH: " << endl;
	cout << abc << endl;
	cout << "\nEnter file name" << endl;
	char fileTitle[30];
	cin >> fileTitle;

	Subject* mass = teach.getMyList();

	ofstream out(fileTitle, ios::out | ios::binary);
	out.write((char*)&mass, 10 * sizeof(Subject));
	out.close();
	system("pause");
	return 0;
}

