#define _CRT_SECURE_NO_WARNINGS

#include "lab03.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <cctype>
#include <conio.h>

using namespace std;


PriceLine::PriceLine()
{
	printf("Constructor by defaulte: \n");
	// ����������� ���� name �� ������� ������
	//name = new char[strlen("") + 1];
	name = new char[15];
	strcpy(name, "");
	// price ���������� 0
	price = 0;

	// ����������� ���� phone �� ������� ������
	//phone = new char[strlen("") + 1];
	phone = new char[14];
	strcpy(phone, "");
	//����������� ���� companyName �� ������� ������
	//companyName = new char[strlen("") + 1];
	companyName = new char[30];
	strcpy(companyName, "");
}

//����������� ������������
PriceLine::PriceLine(char* name0, float price0, char* phone0, char* companyName0)
{
	printf("Constructor by initialization: \n");
	// ����������� ���� name �� name0
	name = new char[strlen(name0) + 1];
	strcpy(name, name0);
	// price ���������� price0
	price = price0;

	// ����������� ���� phone �� phone0
	phone = new char[strlen(phone0) + 1];
	strcpy(phone, phone0);
	//����������� ���� companyName �� companyName0
	companyName = new char[strlen(companyName0) + 1];
	strcpy(companyName, companyName0);
}

//����������� ���������
PriceLine::PriceLine(const PriceLine &price_construct)
{
	printf("Constructor by copy: \n");
	// ������� ����
	name = new char[strlen(price_construct.name) + 1];
	strcpy(name, price_construct.name);

	price = price_construct.price;


	phone = new char[strlen(price_construct.phone) + 1];
	strcpy(phone, price_construct.phone);

	companyName = new char[strlen(price_construct.companyName) + 1];
	strcpy(companyName, price_construct.companyName);
}

//����������
PriceLine::~PriceLine(){
	printf("\nDestruktor\n"); 
	if (name != NULL)
		delete[] name;
	if (phone != NULL)
		delete[] phone;
	if (companyName != NULL)
		delete[] companyName;
}

// ������ ����� ������
void PriceLine::setName()
{
	
	fflush(stdin);
	// k - ����� ���������� �����
	// correct - �-�� ��������� ��������� ��������
	/*int k, correct;
	char* Name = new char;
	do{
		correct = 0;
		printf("\n\nEnter Name no more than 15 symbols: ");
		scanf("%180s",Name);
		k = strlen(Name);
		// �������� ������ �� �� �� �� �����
		for (int i = 0; i < k; i++)
		{
			if (isalpha(Name[i]) || isdigit(Name[i]) || Name[i] == '-' || Name[i] == ' ')
				correct++;
		}
	} while ((correct != k) || (k>15) || (k<1));
	name = Name;*/
	// ���� �� ��� �� ���� ������ ���� � �������� �� ���������� � �������
	

	printf("Enter Product Name'\n");
	char* PName = new char[15];
	int i = 0;
	char c = 0;
	bool ind = true;
	while ((ind == true) && (i<13))
	{
		c = _getch();
		if (c == 13)
		{
			printf("\n");
			if (i != 0)
			{
				PName[i] = '\0';
				ind = false;
				break;
			}
		}
		if (((c >= 48) && (c <= 57)) || ((c >= 65) && (c <= 90)) || ((c >= 97) && (c <= 122)) || (c == 32))
		{
			printf("%c", c);
			PName[i] = c;
			i++;
		}
	}
	PName[i] = '\0';
	
	
	printf("\n");
	//���������
	strcpy(name, PName);
	delete[] PName;
}

// ������ ����� ��������
void PriceLine::setCompanyName()
{
	fflush(stdin);
	// k - ����� ���������� �����
	// correct - �-�� ��������� ��������� ��������
	/*int k, correct;
	char* Name = new char;
	do{
		correct = 0;
		printf("\n\nEnter Company Name no more than 30 symbols: ");
		scanf("%180s",Name);
		k = strlen(Name);
		// �������� ������ �� �� �� �� �����
		for (int i = 0; i < k; i++)
		{
			if (isalpha(Name[i]) || isdigit(Name[i]) || Name[i] == '-' || Name[i] == ' ')
				correct++;
		}
	} while ((correct != k) || (k>30) || (k<1));*/
	// ���� �� ��� �� ���� ������ ���� � �������� �� ���������� � �������
	//���������
	printf("Enter Company Name'\n");
	char* CName = new char[30];
	int i = 0;
	int c = 0;
	bool ind = true;
	while ((ind == true) && (i<28))
	{
		c = _getch();
		if (c == 13)
		{
			printf("\n");
			if (i != 0)
			{
				CName[i] = '\0';
				ind = false;
				break;
			}
		}
		if (((c >= 48) && (c <= 57)) ||  ((c >= 65) && (c <= 90)) || ((c >= 97) && (c <= 122)) || (c == 32))
		{
			printf("%c", c);
			CName[i] = c;
			i++;
		}
	}
	CName[i] = '\0';
	//scanf("%s", StartPoint);
	strcpy(companyName,CName);
	delete[] CName;
	printf("\n");
}
// ���� ��������
void PriceLine::setPhone()
{
	char* StartPoint = new char[13];
	printf("Enter Phone (13 digits) \n");

	int i = 0;
	int c = 0;
	bool ind = true;
	while ((ind == true) && (i<12))
	{
		if ((c == 13))
		{
			printf("\n");
			if (i != 0)
			{
				ind = false;
				break;
			}
		}

		c = _getch();
		//a-z A-Z
		if ( ((c >= 48) && (c <= 57)) )
		{
			printf("%c", c);
			StartPoint[i] = c;
			i++;
		}
		else
		{
			
		}
	}
	StartPoint[i] = '\0';
	//scanf("%s", StartPoint);
	strcpy(phone,StartPoint);
	delete[] StartPoint;
	printf("\n");
	//phone = Numberphone;
}

// ���� ����
void PriceLine::setPrice(){
	char c = 0;
	char* rob = new char[10];
	printf("Enter price. (8 digits) \n");
	
	bool ind = true;
	int i = 0;
	bool indPoint = false;
	while ((ind == true) && (i<8))
	{
		c = _getch();
		// 13 = �����
		if ((c == 13))
		{
			printf("\n");
			if (i != 0)
			{
				ind = false;
				break;
			}
		}
		// 46 - �����, 48 �� 57 �����
		if (((c >= 48) && (c <= 57)) || ( (c == 46) && (!indPoint) ) )
		{
			printf("%c", c);
			rob[i] = c;
			i++;

			if (c == 46){
				indPoint = true;
			}
		}
		else
		{
			//printf("Error. Enter price again \n");
		}
	}
	price = atof(rob);

	delete[] rob;
	//scanf("%lf", &distance);	
	printf("\n");
	//price = k;
}

float PriceLine::getPrice(){
	return price;
}
char* PriceLine::getName(){
	return name;
}

char* PriceLine::getPhone(){
	return phone;
}
char* PriceLine::getCompanyName(){
	return companyName;
}
