#pragma once;
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <cctype>


class PriceLine{
	//����
	char *name;
	float price;
	char *companyName;
	char *phone;

public:

	//�������
	void setName();
	void setPrice();
	void setCompanyName();
	void setPhone();

	// �������
	char* getName();
	float getPrice();
	char* getCompanyName();
	char* getPhone();

	// ����������� �� �������������
	PriceLine();

	//����������� ������������
	PriceLine(char*, float, char*, char*);

	//����������� ���������
	PriceLine(const PriceLine &p);

	~PriceLine();
};