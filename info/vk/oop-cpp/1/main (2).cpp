#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <conio.h>

struct var7{
	double a, b, x1, x2, dx;
	double* arr;
	int arrSize;
};

void input(var7 myStr){
	scanf("%lf", myStr.a);
}

int main(){
	var7 myStruct;
	input(myStruct);
	printf("%lf",myStruct.a);
	_getch();	
	return 0;
}