#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <iostream>

#define Eps 0.00001

struct Lab{
	double a, b;
	double x1, x2;
	double dx;
	double *arr;
};

double function(double x, Lab str)
{
	if (abs(str.b) < Eps || abs(x) < Eps)
		return 0.0;
	if ((abs(x - 0.7) < Eps) || (x < -0.7))
		return -(str.a) * pow(x, 2);
	else
		return (str.a - x) / (str.b * x);
}

Lab vvod()
{
	Lab str;
	printf("Set a: ");
	scanf("%lf", &str.a);
	fflush(stdin);
	printf("Set b: ");
	scanf("%lf", &(str).b);
	fflush(stdin);
	printf("Set x1: ");
	scanf("%lf", &(str.x1));
	fflush(stdin);
	printf("Set x2: ");
	scanf("%lf", &(str).x2);
	fflush(stdin);
	printf("Set dx: ");
	scanf("%lf", &(str).dx);
	fflush(stdin);
	/*str.a = 1;
	str.b = 5;
	str.x1 = -2;
	str.x2 = -0.1;
	str.dx = 0.1;*/
	str.arr = NULL;
	return str;
}

void vivod(Lab str)
{
	printf("|    x     |    N     |   f(x)   |");
	double x = str.x1;
	int i = 1;
	while (abs(x - str.x2) > Eps && x < str.x2)
	{
		printf("\n|%10.1f|%10d|%10.4f|",x,i,str.arr[i-1]);
		x += str.dx;
		i++;
	}
	if (abs(x - str.x2) < Eps)
	printf("\n|%10.1f|%10d|%10.4f|", x, i, str.arr[i - 1]);
}

void ArrayFill(Lab str)
{
	double x = str.x1;
	int i = 0;
	while (abs(x - str.x2) > Eps && x < str.x2)
	{
		str.arr[i] = function(x, str);
		i++;
		x += str.dx;
	}
	if (abs(x - str.x2) < Eps)
	str.arr[i] = function(x, str);
}


void Conversion(Lab str, int arr[], int n)
{
	double max = abs(str.arr[0]);
	double min = str.arr[0];
	double Max = str.arr[0];
	for (int i = 1; i < n; i++)
	{
		if (abs(str.arr[i]) > max)
			max = abs(str.arr[i]);
		if (str.arr[i] > Max)
			Max = str.arr[i];
		if (str.arr[i] < min)
			max = str.arr[i];
	}
	if (abs(max) < Eps) max = 80;
	//double koef = (max > 80) ? (80 / max) : 1;
	double koef = 80 / (Max - min);
	min = (min < 0) ? abs(min) : 0;
	for (int i = 0; i < n; i++)
	{
		arr[i] =  koef*(str.arr[i] + min) ;
	}
	/*for (int i = 0; i < n; i++)
	{
		printf("\n%f", arr[i]);
	}*/
}

void Graph(int arr[], int n)
{
	for (int i = 0; i < n; i++)
	{
		if (arr[i] < 80) printf("\n");
		for (int j = 0; j < arr[i]; j++)
			printf("%c", 0xDF);
		//printf("  %d) %d", i+1, arr[i]);
	}
}

double s1Count(double arr[],int n)
{
	int count = 0;
	double sum = 0;
	for (int i = 0; i < n; i++)
	{
		if (arr[i] < 0)
		{
			sum += arr[i];
			count++;
		}
	}
	return sum / count;
}
double s2Count(Lab str, int n)
{
	double sum = 0;
	double x = str.x1;
	for (int i = 0; i < n; i++)
	{
		if (abs(fmod(x,2)) < Eps){
			sum += str.arr[i];
		}
		x += str.dx;
	}
	return sum;
}

void CreateArr(Lab str, int n, double arr[])
{
	double s1 = s1Count(str.arr, n);
	double s2 = s2Count(str, n);
	double sMax = (s1 > s2) ? s1 : s2;
	double sMin = (s1 < s2) ? s1 : s2;
	double step = 1000*(0.01 * abs(sMax)) / 1000;
	sMin -= fmod(sMin, step);
	int possibleSteps = (sMax - sMin) / step;
	for (int i = 0; i < n; i++)
	{
		arr[i] = sMin + step * (rand() % possibleSteps);
	}
}

void SmoothArr(double arr[], double mainArr[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		mainArr[i] = (arr[i] + arr[i + 1]) / 2;
		//printf("\n %f", mainArr[i]);
	}
}

void FileWrite(double arr[], int n)
{
	FILE* out = fopen("Var7.txt", "wt");
	double max = arr[0], min = arr[0];
	for (int i = 1; i < n - 1; i++)
	{
		if (arr[i] > max) max = arr[i];
		if (arr[i] < min) min = arr[i];
	}
	for (int i = 0; i < n - 1; i++)
	{
		if ( abs(arr[i] - max) < Eps)
			fprintf(out, "|%10d|%10.4f|\n", i+1,arr[i]);
		if (abs(arr[i] - min) < Eps)
			fprintf(out, "|%10d|%10.4f|\n", i + 1, arr[i]);
	}
	fclose(out);
	
}

void main()
{
	Lab MainStruct = vvod();
	int n =  abs(MainStruct.x1-MainStruct.x2)/MainStruct.dx + 1;
	MainStruct.arr = new double[n];
	ArrayFill(MainStruct);
	vivod(MainStruct);
	int *convArr = new int[n];
	Conversion(MainStruct, convArr,n);
	Graph(convArr, n);
	double *randArr = new double[n];
	CreateArr(MainStruct, n, randArr);
	double *smoothArr = new double[n - 1];
	SmoothArr(randArr, smoothArr, n);
	FileWrite(smoothArr, n);
	printf("\n");
	system("PAUSE");
}
