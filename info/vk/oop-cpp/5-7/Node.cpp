#include "Node.h"


template <class T>
Node<T>::Node() :next(NULL){}

template <class T>
Node<T>::~Node()
{
	if (!next) delete next;
}

template <class T>
T Node<T>::getData() const
{
	return Data;
}