#pragma once
class Group
{
private:
	char* code;
	char* faculty;
	char* specialty;
	int year;
public:
	Group();
	Group(char*, char*, char*, int);
	~Group();
	//LAB 6
	friend std::ostream &operator<<(std::ostream & stream, const Group & Obj);

	void setCode(char*);
	void setFaculty(char*);
	void setSpecialty(char*);
	void setEnteringYear(int);

	char* getCode() const;
	char* getFaculty() const;
	char* getSpecialty() const;
	int getEnteringYear() const;
};

