#pragma once
#include "Node.h"
template <class T>
class List
{
public:
	List();
	~List();
	T Find(int number) const;

	Node<T> * getHead() const { return pHead; }
	int getCount() const { return count; }
	void display() const;
	void addItem(T data);
	void deleteItem(int number);
	void insertItem(int number, T data);

private:
	int count;
	Node<T> * pHead;
}