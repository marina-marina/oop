#include "stdafx.h"
#include <iostream>
#include <time.h>

#include  "Student.h"

Student::Student() :Group(), Person()
{
	resetAge();
	//std::cout << "Student" << std::endl;
}

Student::Student(char * _surname = "Vuchkan", char* _name = "Vladimir", int _sex = 1, int _d = 11, int _m = 2, int _y = 1998, char* _code = "TI-41", char* _faculty = "TEF", char* _specialty = "software engineering", int _ye = 2014) : Person(
	_surname, _name, _sex, _d, _m, _y
	), Group(
	_code, _faculty, _specialty, _ye
	)

{
	resetAge();
}

Student::~Student()
{
	// Nothing to free
}

int Student::getAge() const {
	return age;
}
//Вычисление возраста
void Student::resetAge(){
	time_t t;	time(&t);
	tm *tk;		tk = localtime(&t);

	setAge((1900 + tk->tm_year) - get_Year());
}

void Student::setAge(int value){
	age = value;
}