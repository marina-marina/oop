#pragma once

#include "Person.h"
#include "Date.h"
#include "Group.h"


class Student :public Group, public Person{
private:
	int age;
public:
	Student();
	Student(char*, char*, int, int, int, int, char*, char*, char*, int);
	~Student();

	friend std::ostream &operator<<(std::ostream & stream, const Student & Obj);
	int getAge() const;
	void setAge(int);
	void resetAge();
};

