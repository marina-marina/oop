#include "stdafx.h"
#include <iostream>
#include <cstring>	
#include "Student.h"
#include  "GroupOfStudent.h"

using namespace std;

GroupOfStudent::GroupOfStudent():Group(){
	CountOfStudent = 0;
	Student *ListOfStudent = new Student[0];
}

GroupOfStudent::GroupOfStudent(char* _code, char* _faculty, char* _specialty, int _y) : Group(_code, _faculty,  _specialty, _y){
	CountOfStudent = 0;
	Student *ListOfStudent = new Student[0];
}
GroupOfStudent::~GroupOfStudent(){
}
int GroupOfStudent::getCountOfStudent() const{
	return CountOfStudent;
}

Student* GroupOfStudent::getListOfStudent(){
	return ListOfStudent;
}



Student &GroupOfStudent::operator[](int i){
	if (i > CountOfStudent || i < 0) {
		printf("\nOut of bounse! [ array size is: %d, your index is: %d ]\n\n", CountOfStudent, i);
		system("pause");
		exit(1);
	}
	return ListOfStudent[i];
}

Student GroupOfStudent::getStudentByIndex(int index) const{
	return ListOfStudent[index];
}

void GroupOfStudent::addStudentToList(Student _st){
	
		Student * Temp = new Student[CountOfStudent];

		for (int i = 0; i < CountOfStudent; i++){
			Temp[i] =  ListOfStudent[i];
		}
		
		//delete[] ListOfStudent;

		ListOfStudent = new Student[CountOfStudent+1];

		for (int i = 0; i < CountOfStudent; i++){
			ListOfStudent[i] = Temp[i];
		}

		delete[] Temp;
	
	ListOfStudent[CountOfStudent] = _st;
	CountOfStudent++;
}

int GroupOfStudent::deleteStudentFromList(int index){
	if (index < 1 || index > CountOfStudent){
		cout << endl <<  "Studenta s zadanim indexom net!" << endl;
		return 0;
	}
	index--;

	Student * Temp = new Student[CountOfStudent-2];

	for (int i = 0; i < index; i++){
		Temp[i] = ListOfStudent[i];
	}

	for (int i = index + 1; i < CountOfStudent; i++){
		Temp[i-1] = ListOfStudent[i];
	}

	delete[] ListOfStudent;
	ListOfStudent = new Student[CountOfStudent - 1];

	for (int i = 0; i < CountOfStudent - 1; i++){
		ListOfStudent[i] = Temp[i];
	}
	if (!Temp)
	delete[] Temp;

	CountOfStudent--;
	return 0;
}