#include "stdafx.h"
#include "List.h"
#include "Node.h"


template<class T>
List<T>::List() :pHead(NULL), count(0){}

template <class T>
List<T>::~List()
{
	Node<T> * current;
	Node<T> * previous;
	if (pHead != NULL)
	{
		current = pHead;
		while (current != NULL)
		{
			previous = current;
			current = current->getNext();
			delete previous;
		}
	}
}


template <class T>
void List<T>::addItem(T data)
{
	Node <T> * newNode = new Node <T>;
	newNode->setData(data);
	newNode->setNext(NULL);

	if (pHead == NULL)
	{
		pHead = newNode;
		count++;
		return;
	}

	Node <T> * current = pHead;
	while (current->getNext() != NULL)
		current = current->getNext();
	current->setNext(newNode);
	count++;
}

template <class T>
void List<T>::display() const
{
	Node<T> * current = getHead();
	while (current != NULL)
	{
		cout << current->getData();
		current = current->getNext();
	}
}

template <class Type>
Type  Count(Type *arr, int size)
{
	Type sum = 0;
	for (int j = 0; j < size; j++)
		sum += arr[j];
	return sum;
}

template <class T>
void List<T>::deleteItem(int number)
{
	if (number < 0)
	{
		cout << "Error < 0";
		return;
	}
	if (getHead() == NULL)
	{
		cout << "List - NULL";
		return;
	}
	if (number > count)
	{
		cout << "Doesn't exists!";
		return;
	}

	if (number == 0)
	{
		Node<T> * temp = getHead();
		pHead = pHead->getNext();
		delete temp;
		count--;
		return;
	}

	Node<T> * current = getHead();
	Node<T> * previous = getHead();
	for (int i = 0; i < number; i++)
	{
		previous = current;
		current = current->getNext();
	}


	count--;
	previous->setNext(current->getNext());
	delete current;
}

template <class T>
void List<T>::insertItem(int number, T data)
{
	// ������ �� ����������
	if (number > count)
	{
		addItem(data);
		return;
	}
	if (number < 0)
	{
		cout << "Error < 0";
		return;
	}
	// ����� �����
	Node <T> * newNode = new Node <T>;
	newNode->setData(data);
	newNode->setNext(NULL);
	if (number == 0)
	{
		newNode->setNext(pHead);
		pHead = newNode;
		count++;
		return;
	}

	Node<T> * current = getHead();
	Node<T> * previous = getHead();
	for (int i = 0; i < number; i++)
	{
		previous = current;
		current = current->getNext();
	}
	previous->setNext(newNode);
	newNode->setNext(current);
	count++;
}

template <class T>
T List<T>::Find(int number) const
{
	if (number == 0)
	{
		return (pHead->getData());
	}
	if (number > (count - 1))
	{
		return (Find(count - 1));
	}
	Node<T> * current = pHead;
	for (int i = 0; i <= number; i++)
	{
		current = current->getNext();
	}
	return (current->getData());
}