#pragma once
#include "Date.h"
class Person :
	public Date
{
private:
	char* surname;
	char* name;
	int sex;
public:
	Person();
	Person(char*, char*, int, int, int, int);
	~Person();

	void setName(char*);
	void setSurname(char*);
	void setSex(int);

	char* getName() const;
	char* getSurname() const;
	int getSex() const;
};

