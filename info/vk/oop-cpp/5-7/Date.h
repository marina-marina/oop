#pragma once
class Date
{
private:
	int date[3];
public:
	Date();
	Date(int , int , int );
	~Date();
	void setDate(int, int, int);
	int get_Day() const;
	int get_Month() const;
	int get_Year() const;
};

