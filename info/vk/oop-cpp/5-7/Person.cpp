#include "stdafx.h"
#include "Person.h"


Person::Person() :Date() {
	setSurname("Vuchkan");
	setName("Vladimir");
	setSex(1);
	//std::cout << "Person" << std::endl;
}

Person::Person(char* _surname, char* _name, int _sex, int _d, int _m, int _y) :Date(_d, _m, _y)
{
	sex = _sex;
	setSurname(_surname);
	setName(_name);
}

Person::~Person(){
	if (!surname)
		delete[] surname;
	if (!name)
		delete[] name;
}

void Person::setSurname(char* _surname){
	surname = new char[strlen(_surname) + 1];
	strcpy(surname, _surname);
}

void Person::setName(char* _name){
	name = new char[strlen(_name) + 1];
	strcpy(name, _name);
}

void Person::setSex(int _sex)
{
	sex = _sex;
}


char* Person::getSurname() const{
	return surname;
}

char* Person::getName() const{
	return name;
}

int Person::getSex() const{
	return sex;
}