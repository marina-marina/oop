#pragma once
#include "Group.h"
#include "Student.h"
class GroupOfStudent :
	public Group
{
private: 
	int CountOfStudent;
	Student * ListOfStudent;
public:
	GroupOfStudent();
	GroupOfStudent(char*, char*, char*, int);
	~GroupOfStudent();


	
	Student &operator[](int);
	int getCountOfStudent() const;
	Student* getListOfStudent();
    Student  getStudentByIndex(int) const;

	void addStudentToList(Student);
	int deleteStudentFromList(int);
};

