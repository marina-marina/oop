#pragma once
template < class T>
class Node
{
public:
	Node();
	~Node();
	void setNext(Node * node){ next = node; }
	void setData(T data) { Data = data; }
	Node * getNext() const { return next; }
	T getData() const;
private:
	T Data;
	Node * next;
}