#include "Date.h"


Date::Date(){
	setDate(1, 1, 1998);
}

Date::Date(int d = 1, int m = 1, int y = 1950){
	setDate(d, m, y);
}
Date::~Date(){
}

void Date::setDate(int d, int m, int y){
	date[0] = d;
	date[1] = m;
	date[2] = y;
}


int Date::get_Day() const{
	return date[0];
}

int Date::get_Month() const{
	return date[1];
}

int Date::get_Year() const{
	return date[2];
}