#include "stdafx.h"

#include "GroupOfStudent.h"
#include "Student.h"
#include <math.h>
#include <algorithm>
using namespace std;

template < class T>
class Node
{
public:
	Node();
	~Node();
	void setNext(Node * node){ next = node; }
	void setData(T data) { Data = data; }
	Node * getNext() const { return next; }
	T getData() const;
private:
	T Data;
	Node * next;
};


template <class T>
class List
{
public:
	List();
	~List();
	T Find(int number) const;

	Node<T> * getHead() const { return pHead; }
	int getCount() const { return count; }
	void display() const;
	void addItem(T data);
	void deleteItem(int number);
	void insertItem(int number, T data);

private:
	int count;
	Node<T> * pHead;
};

// ������ �-�� �� ������ ����������� �������� � ������ ��������
template <class Type>
Type getValueInCaseOfClass(Type *array, int size)
{
	Type value = 0;
	for (int i = 0; i < size; i++)
		value = value > array[i] ? value : array[i];

	return value;
}

//���������� �������� ������ Student
ostream &operator<<(ostream & stream, const Student & Obj)
{
	stream << Obj.getName() << " " << Obj.getSurname() << " " << Obj.getAge() << endl;
	return stream;
}

ostream &operator<<(ostream & stream, const Group & Obj)
{
	stream << Obj.getCode() << " " << Obj.getFaculty() << " " << Obj.getSpecialty() << " " << Obj.getEnteringYear() << endl;
	return stream;
}
// ���������� �������� ������ GroupOfStudent
ostream &operator<<(ostream & stream, const GroupOfStudent & Obj)
{
	stream << "Code: " << Obj.getCode() << endl << "Faculty: " << Obj.getFaculty() << endl << "Speciality: " << Obj.getSpecialty() << endl << "Entering Year: " << Obj.getEnteringYear()<<endl << "Count of Student:" << Obj.getCountOfStudent() << endl << "List: " << endl;
	
	for (int i = 0; i < Obj.getCountOfStudent(); i++){
		stream << i+1 << "   "  <<  Obj.getStudentByIndex(i);
	}
	return stream;
}

int load_students(Student *, int);
void output_file(GroupOfStudent * Obj)
{
	ofstream file;
	char *filename=new char [1024];
	do{
		cout << "Enter file name" << endl;
		cout << "Example: E:\\lab.txt" << endl;
		cin >> filename;
		file.open(filename, ios::out);
		if (!file.is_open())
		{
			cout << "You entered incorrect filename or directory" << endl;
		}
		else break;
	}while(true);
	for (int i = 0; i < 10; i++ )
		file << Obj[i];
	file.close();
}


int main(){	
	// ������ ���������
	
	Student * BaseOfStudent = new Student[91]();
	cout << "Size:" << sizeof(BaseOfStudent) << endl;
	// ���������� ��������� � ���� ���������
	int CountOfStudent = load_students(BaseOfStudent, 91);
	int CountOfGroups = 10;
	GroupOfStudent * Groups = new  GroupOfStudent[CountOfGroups];
	Groups[0].setCode("TI-41");
	Groups[1].setCode("TV-41");
	Groups[2].setCode("TV-42");
	Groups[3].setCode("TS-41");
	Groups[4].setCode("TP-41");
	Groups[5].setCode("TK-41");
	Groups[6].setCode("TR-41");
	Groups[7].setCode("TF-42");
	Groups[8].setCode("TA-42");
	Groups[9].setCode("TM-41");
	
	for (int j = 0; j < CountOfStudent; j++)
	{
		for (int i = 0; i < CountOfGroups; i++) {
			if ( !strcmp(BaseOfStudent[j].getCode(), Groups[i].getCode()) ){
				Groups[i].addStudentToList(BaseOfStudent[j]);
			}
		}
	}
	
	for (int i = 0; i < 10; i++)
		cout << Groups[i] << endl;
	
	
	Groups[0].deleteStudentFromList(4);

		cout << Groups[0] << endl;

		// ������������ ������
		List <Student> st;
		st.addItem(Student("Vuchkan", "Vladimir", 1, 11, 2, 1998, "TI-41", "TEF", "SE", 2014));
		st.addItem(Student("Ivanov", "Ivan", 1, 11, 2, 1997, "TI-41", "TEF", "SE", 2014));
		st.addItem(Student("Sergeev", "Sergey", 1, 11, 2, 1996, "TI-41", "TEF", "SE", 2014));
		cout << "List of students: " << endl;
		st.display();

		st.deleteItem(1);
		cout << endl << "After deleting: " << endl;
		st.display();

		List <Group> gr;
		gr.addItem(Group("TI-41", "TEF", "SE",2014 ));
		gr.addItem(Group("TV-41", "TEF", "SE", 2014));
		gr.addItem(Group("TV-42", "TEF", "SE", 2014));
		cout << endl << "List of group: " << endl;
		gr.display();

		gr.deleteItem(1);
		gr.deleteItem(3);

		cout << endl << "List of group after changing: " << endl;
		gr.display();

		// �-�� ���������� ����������� �-�� ��������� � ����
		int iNumberOfGroups = 10;
		int iNumberOfStudents;
		int * iNumberOfGirlsInEachGroup;
		iNumberOfGirlsInEachGroup = new int[iNumberOfGroups];

		for (int i = 0; i<iNumberOfGroups; i++)
		{
			iNumberOfStudents = Groups[i].getCountOfStudent();
			iNumberOfGirlsInEachGroup[i] = 0;

			for (int j = 0; j < iNumberOfStudents; j++)
			if (Groups[i][j].getSex() == 2)
				iNumberOfGirlsInEachGroup[i]++;
		}

		cout << "Max. number of the codes (girl-srudent) in the groups: " << getValueInCaseOfClass(iNumberOfGirlsInEachGroup, iNumberOfGroups) << endl;

		delete[]iNumberOfGirlsInEachGroup;

		//�-�� ���������� ����������� ��������
		iNumberOfGroups = 10;

		int * iAgeOfTheEldestStudent;
		iAgeOfTheEldestStudent = new int[iNumberOfGroups];

		for (int i = 0; i<iNumberOfGroups; i++)
		{
			iNumberOfStudents = Groups[i].getCountOfStudent();
			iAgeOfTheEldestStudent[i] = 0;
			for (int j = 0; j < iNumberOfStudents; j++)
			{
				if (Groups[i][j].getSex() == 1) {
					iAgeOfTheEldestStudent[i] = max(iAgeOfTheEldestStudent[i], Groups[i][j].getAge());
				}
			}

		}

		cout << "The eldest student in the groups: " << getValueInCaseOfClass(iAgeOfTheEldestStudent, iNumberOfGroups) << endl;

		delete[]iAgeOfTheEldestStudent;

	system("pause");
}

int load_students(Student * Obj, int num){
	ifstream f;
	f.open("mybase.txt");

	int i = 0;
	char name[32];
	char surname[32];
	char group[20];
	unsigned short int sex;
	unsigned short int d;
	unsigned short int m;
	unsigned short int y;

	if (f) {
		while (!f.eof() && (i<num)) {
			f >> name >> surname >> d >> m >> y >> sex >> group;

			Obj[i].setName(name);
			Obj[i].setSurname(surname);
			Obj[i].setSex(sex);
			Obj[i].setDate(d, m, y);
			Obj[i].setCode(group);
			Obj[i].resetAge();


			if (!f.eof())
				i++;
		}
	}
	f.close();


	return i;
}




//����������� �������� �� ��������� ������ NULL
template <class T>
Node<T>::Node() :next(NULL){}

// �������� �������� �� ��������� �������
template <class T>
Node<T>::~Node()
{
	if (!next) delete next;
}

//������ �����
template <class T>
T Node<T>::getData() const
{
	return Data;
}


template<class T>
List<T>::List() :pHead(NULL), count(0){}

template <class T>
List<T>::~List()
{
	Node<T> * current;
	Node<T> * previous;
	if (pHead != NULL)
	{
		current = pHead;
		while (current != NULL)
		{
			previous = current;
			current = current->getNext();
			delete previous;
		}
	}
}


template <class T>
void List<T>::addItem(T data)
{
	Node <T> * newNode = new Node <T>;
	newNode->setData(data);
	newNode->setNext(NULL);

	if (pHead == NULL)
	{
		pHead = newNode;
		count++;
		return;
	}

	Node <T> * current = pHead;
	while (current->getNext() != NULL)
		current = current->getNext();
	current->setNext(newNode);
	count++;
}

template <class T>
void List<T>::display() const
{
	Node<T> * current = getHead();
	while (current != NULL)
	{
		cout << current->getData();
		current = current->getNext();
	}
}

template <class Type>
Type  Count(Type *arr, int size)
{
	Type sum = 0;
	for (int j = 0; j < size; j++)
		sum += arr[j];
	return sum;	
}

template <class T>
void List<T>::deleteItem(int number)
{


	if (number < 0)
	{
		cout << "Error < 0";
		return;
	}
	if (getHead() == NULL)
	{
		cout << "List - NULL";
		return;
	}
	if (number > count)
	{
		cout << "Doesn't exists!";
		return;
	}

	if (number == 0)
	{
		Node<T> * temp = getHead();
		pHead = pHead->getNext();
		delete temp;
		count--;
		return;
	}

	Node<T> * current = getHead();
	Node<T> * previous = getHead();
	for (int i = 0; i < number; i++)
	{
		previous = current;
		current = current->getNext();
	}


	count--;
	previous->setNext(current->getNext());
	delete current;
}

template <class T>
void List<T>::insertItem(int number, T data)
{
	// ������ �� ����������
	if (number > count)
	{
		addItem(data);
		return;
	}
	if (number < 0)
	{
		cout << "Error < 0";
		return;
	}
	// ����� �����
	Node <T> * newNode = new Node <T>;
	newNode->setData(data);
	newNode->setNext(NULL);
	if (number == 0)
	{
		newNode->setNext(pHead);
		pHead = newNode;
		count++;
		return;
	}

	Node<T> * current = getHead();
	Node<T> * previous = getHead();
	for (int i = 0; i < number; i++)
	{
		previous = current;
		current = current->getNext();
	}
	previous->setNext(newNode);
	newNode->setNext(current);
	count++;
}

template <class T>
T List<T>::Find(int number) const
{
	if (number == 0)
	{
		return (pHead->getData());
	}
	if (number > (count - 1))
	{
		return (Find(count - 1));
	}
	Node<T> * current = pHead;
	for (int i = 0; i <= number; i++)
	{
		current = current->getNext();
	}
	return (current->getData());
}
