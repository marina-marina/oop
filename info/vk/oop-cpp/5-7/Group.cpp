#include "stdafx.h"
#include <iostream>
#include "string.h"
#include  "Group.h"

Group::Group(){
	setEnteringYear(2014);
	setCode("TI-41");
	setFaculty("TEF");
	setSpecialty("software engineering");
	//std::cout << "Group" << std::endl;
}

Group::Group(char* _code, char* _faculty, char* _specialty, int _y)
{
	setEnteringYear(_y);
	setCode(_code);
	setFaculty(_faculty);
	setSpecialty(_specialty);
}

Group::~Group(){
	/*if (code)
		delete[] code;
	if (faculty)
		delete[] faculty;
	if (specialty)
		delete[] specialty;*/
}

void Group::setEnteringYear(int v){
	year = (v<1990 || v>2014) ? 2014 : v;
}

void Group::setCode(char* _str){
	
	code = new char[strlen(_str) + 1];
	strcpy(code, _str);
}

void Group::setFaculty(char* _str){
	
	faculty = new char[strlen(_str) + 1];
	strcpy(faculty, _str);
}

void Group::setSpecialty(char* _str){
	
	specialty = new char[strlen(_str) + 1];
	strcpy(specialty, _str);
}

int Group::getEnteringYear() const{
	return year;
}

char* Group::getCode() const{
	return code;
}

char* Group::getSpecialty() const{
	return specialty;
}

char* Group::getFaculty() const{
	return faculty;
}