#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <math.h>


using namespace std;

class PriceLine{
	//����
	char *name;
	float price;
	char *companyName;
	char *phone;

public:

	//�������
	void setName();
	void setPrice();
	void setCompanyName();
	void setPhone();

	// �������
	char* getName();
	float getPrice();
	char* getCompanyName();
	char* getPhone();

	// ����������� �� �������������
	PriceLine();

	//����������� ������������
	PriceLine(char*, float, char*,char* );

	//����������� ���������
	PriceLine(const PriceLine &p);

	~PriceLine();
};

// ����������� �� �������������
PriceLine::PriceLine()
{
	printf("Constructor by default: \n");
	// ����������� ���� name �� ������� ������
	name = new char[strlen("") + 1];
	strcpy(name, "");
	// price ���������� 0
	price = 0;

	// ����������� ���� phone �� ������� ������
	phone = new char[strlen("") + 1];
	strcpy(phone, "");
	//����������� ���� companyName �� ������� ������
	companyName = new char[strlen("") + 1];
	strcpy(companyName, "");
}

//����������� ������������
PriceLine::PriceLine(char* name0, float price0, char* phone0, char* companyName0)
{
	printf("Constructor by initialization: \n");
	// ����������� ���� name �� name0
	name = new char[strlen(name0) + 1];
	strcpy(name, name0);
	// price ���������� price0
	price = price0;

	// ����������� ���� phone �� phone0
	phone = new char[strlen(phone0) + 1];
	strcpy(phone, phone0);
	//����������� ���� companyName �� companyName0
	companyName = new char[strlen(companyName0) + 1];
	strcpy(companyName, companyName0);
}

//����������� ���������
PriceLine::PriceLine(const PriceLine &price_construct)
{
	printf("Constructor by copy: \n");
	// ������� ����
	name = new char[strlen(price_construct.name) + 1];
	strcpy(name, price_construct.name);

	price = price_construct.price;


	phone = new char[strlen(price_construct.phone) + 1];
	strcpy(phone, price_construct.phone);

	companyName = new char[strlen(price_construct.companyName) + 1];
	strcpy(companyName, price_construct.companyName);
}

//����������
PriceLine::~PriceLine(){
	printf("\nDestruktor\n");
}

// ������ ����� ������
void PriceLine::setName()
{
	fflush(stdin);
	// k - ����� ���������� �����
	// correct - �-�� ��������� ��������� ��������
	int k, correct;
	char* Name = new char;
	do{
		correct = 0;
		printf("\n\nEnter Name no more than 15 symbols: ");
		gets(Name);
		k = strlen(Name);
		// �������� ������ �� �� �� �� �����
		for (int i = 0; i < k; i++)
		{
			if ( isalpha(Name[i]) || isdigit(Name[i]) || Name[i] == '-'|| Name[i] == ' ')
				correct++;
		}
	} while ((correct != k) || (k>15) || (k<1));
	// ���� �� ��� �� ���� ������ ���� � �������� �� ���������� � �������
	//���������
	name = Name;
}

// ������ ����� ��������
void PriceLine::setCompanyName()
{
	fflush(stdin);
	// k - ����� ���������� �����
	// correct - �-�� ��������� ��������� ��������
	int k, correct;
	char* Name = new char;
	do{
		correct = 0;
		printf("\n\nEnter Company Name no more than 30 symbols: ");
		gets(Name);
		k = strlen(Name);
		// �������� ������ �� �� �� �� �����
		for (int i = 0; i < k; i++)
		{
			if (isalpha(Name[i]) || isdigit(Name[i]) || Name[i] == '-' || Name[i] == ' ')
				correct++;
		}
	} while ((correct != k) || (k>30) || (k<1));
	// ���� �� ��� �� ���� ������ ���� � �������� �� ���������� � �������
	//���������
	companyName = Name;
}

void PriceLine::setPhone()
{
	fflush(stdin);
	int k, correct;
	char* Numberphone = new char;
	do{
		correct = 0;
		printf("\nEnter Phone Number no more than 15 numerics: ");
		gets(Numberphone);
		k = strlen(Numberphone);
		for (int i = 0; i < k; i++)
		{
			if (isdigit(Numberphone[i]) || Numberphone[i] == '-' || Numberphone[i] == ' ')
				correct++;
		}
	} while ((correct != k) || (k>15) || (k<1));
	phone = Numberphone;
}

void PriceLine::setPrice(){
		int correct, k=0, incorrect,point,sign;
		char num[20];
		fflush(stdin);
		do
		{
			//��������� �������
			correct = 0;
			// ���������� �������
			incorrect = 0;
			//�����
			point = 0;
			printf("\nEnter Price more than 0: ");
			gets(num);
			k = atof(num);
			for (int i = 0; i < strlen(num); i++)
			{
				if (isdigit(num[i])) correct++;
				if (!isdigit(num[i])) incorrect++;
				if (num[i] == '.') point++;
			}
		} while ( (correct < 1) || (k < 0) || ((incorrect >= 1) && (point == 0)) || ((incorrect >= 2) && (point == 1)) );
		
		
		price = k;
}

float PriceLine::getPrice(){
	return price;
}
char* PriceLine::getName(){
	return name;
}

char* PriceLine::getPhone(){
	return phone;
}
char* PriceLine::getCompanyName(){
	return companyName;
}

int main(){
	// ����������� �� �������������
	PriceLine simple;
	//����������� ������������
	PriceLine init("Foo",12, "12345678", "Intellectus Studio");
	//����������� ���������
	PriceLine copyPrice(init);

	//��� �����
	simple.setName();
	simple.setPrice();
	simple.setCompanyName();
	simple.setPhone();


	//���� �������
	printf("\nData:");
	printf("\n%c", 0xDA); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); } printf("%c", 0xC2); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); }printf("%c", 0xC2); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); }printf("%c", 0xC2); for (int i = 0; i < 18; i++){ printf("%c", 0xC4); }printf("%c", 0xBF);
	printf("%c", 0xB3); printf("%15cName%c", NULL, 0xB3); printf("%14cPrice%c", NULL, 0xB3); printf("%7cCompany Name%c", NULL, 0xB3); printf("%13cPhone%c", NULL, 0xB3);
	printf("%c", 0xC3); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); } printf("%c", 0xC5); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); }printf("%c", 0xC5); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); }printf("%c", 0xC5); for (int i = 0; i < 18; i++){ printf("%c", 0xC4); }printf("%c", 0xB4);
	printf("%c", 0xB3);
	printf("%19s", simple.getName());
	printf("%c", 0xB3);
	printf("%19.2f", simple.getPrice());
	printf("%c", 0xB3);
	printf("%19s", simple.getCompanyName());
	printf("%c", 0xB3);
	printf("%18s", simple.getPhone());
	printf("%c", 0xB3);
	printf("%c", 0xC0); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); } printf("%c", 0xC1); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); }printf("%c", 0xC1); for (int i = 0; i < 19; i++){ printf("%c", 0xC4); }printf("%c", 0xC1); for (int i = 0; i < 18; i++){ printf("%c", 0xC4); }printf("%c", 0xD9);
	system("pause");
	_getch();
	return 0;
}