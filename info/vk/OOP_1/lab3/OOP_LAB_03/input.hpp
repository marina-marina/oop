#ifndef INPUT_HPP_INCLUDED
#define INPUT_HPP_INCLUDED

#include<iostream>

template<typename T>
T safeInput(T min, T max, std::istream& is = std::cin)
{
    T value;
    for ( char ch ; !(is>>value) || value>max || value<min || is.peek()!='\n' ; )
    {
        is.clear();
        bool trash = false;
        while((ch = is.get()) != '\n')
        {
                if (isgraph(ch) || ch<0)
                    trash = true;
        }

        if (trash || value>max || value<min)
        {
            if (is==std::cin) std::cout<<"Error! Try again\n";
        }
        else
            break;
    }

    return value;
}

#endif // INPUT_HPP_INCLUDED
