#include<climits>
#include<vector>
#include<sstream>
#include "book.hpp"
#include "input.hpp"

book::book(std::string a, std::string t, unsigned y, unsigned tot, double r) :
             author(a),
              title(t),
               year(y),
      totalReadings(tot),
    readingsPerYear(r)
{
    std::cout<<"[CTOR] Default/initial constructor for book \""<<title<<"\" by "<<author<<"\n";
}

book::book(const book& b) :
             author(b.author),
              title(b.title),
               year(b.year),
      totalReadings(b.totalReadings),
    readingsPerYear(b.readingsPerYear)
{
    std::cout<<"[CTOR] Copy constructor for book \""<<title<<"\" by "<<author<<"\n";
}

book::~book()
{
    std::cout<<"[DTOR] Destructor for book \""<<title<<"\" by "<<author<<"\n";
}

void book::init()
{
    std::cout<<"\ninput data:\n";
    std::cout<<"       author:\t\t";            getline(std::cin, author);
    std::cout<<"       title:\t\t";             getline(std::cin, title);
    std::cout<<"       year(0..2014):\t";       year = safeInput(0U, 2014U);
    std::cout<<"       totalReadings:\t";       totalReadings = safeInput(0U, UINT_MAX);
    std::cout<<"       readingsPerYear:\t";     readingsPerYear = safeInput(0.0, 1e100);
}

void book::show() const
{
    const unsigned lw = 22, rw = 53;
    const std::vector<std::string> lText = { "author", "title", "year", "total readings", "readings per year" };
    std::ostringstream buf;
    buf<<readingsPerYear;
    const std::vector<std::string> rText = { author, title, std::to_string(year), std::to_string(totalReadings), buf.str() };

    std::cout.put(0xDA); for (unsigned j=0; j<lw; ++j) std::cout.put(0xC4); std::cout.put(0xC2); for (unsigned j=0; j<rw; ++j) std::cout.put(0xC4); std::cout.put(0xBf); std::cout<<"\n";
    for (unsigned i = 0 ; i < lText.size() ; ++i)
    {
        std::cout<<char(0xB3);
            for (unsigned j=0; j<(lw-lText[i].length())/2; ++j) std::cout.put(' '); std::cout<<lText[i]; for (unsigned j=0; j<lw-lText[i].length()-(lw-lText[i].length())/2; ++j) std::cout.put(' '); std::cout.put(0xB3);
            for (unsigned j=0; j<(rw-rText[i].length())/2; ++j) std::cout.put(' '); std::cout<<rText[i]; for (unsigned j=0; j<rw-rText[i].length()-(rw-rText[i].length())/2; ++j) std::cout.put(' '); std::cout.put(0xB3);
        std::cout<<"\n";

        if (i < lText.size()-1)
        {
            std::cout.put(0xC3); for (unsigned j=0; j<lw; ++j) std::cout.put(0xC4); std::cout.put(0xC5); for (unsigned j=0; j<rw; ++j) std::cout.put(0xC4); std::cout.put(0xB4); std::cout<<"\n";
        }
    }

    std::cout.put(0xC0); for (unsigned j=0; j<lw; ++j) std::cout.put(0xC4); std::cout.put(0xC1); for (unsigned j=0; j<rw; ++j) std::cout.put(0xC4); std::cout.put(0xD9); std::cout<<"\n";
}
