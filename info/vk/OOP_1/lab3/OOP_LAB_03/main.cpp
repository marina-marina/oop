#include<iostream>
#include "book.hpp"

int main()
{
    {
        std::cout<<"Entering new block\n";
        std::cout<<"Creating book object from scratch...\n";
        book testBook1;
        testBook1.show();
        testBook1.init();
        std::cout<<"Creating book object copying other one...\n";
        book testBook2(testBook1);
        testBook2.show();
        std::cout<<"Quitting block\n";
    }

    system("pause");
    {
        std::cout<<"\n\nCreating new book...\n";
        book testBook("Yotam Ottolenghi", "Plenty More", 2014, 0, 0.0);
        testBook.show();
        std::cout<<"Add five readings\n";
        for (int i = 0; i < 5; testBook.addReading(), ++i);
        testBook.show();
        std::cout<<"Subtract one reading\n";
        testBook.subReading();
        testBook.show();
    }

    std::cout<<"\n\nGoodbye!\n";
}
