#include "head.hpp"

int main()
{
    Lector n1, n2( Person("Ivan", "Ivanov", Date(1970, 5, 5)),
                   Position::PROFESSOR,
                   std::vector<Discipline>({Discipline("Math", 100, 50, 30), Discipline("OOP", 90, 50, 20)}) );
    std::cout<<"Default ctor used:\n"<<n1<<"Init ctor used:\n"<<n2;
}
