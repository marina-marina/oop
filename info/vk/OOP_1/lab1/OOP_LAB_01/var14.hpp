#ifndef VAR14_HPP_INCLUDED
#define VAR14_HPP_INCLUDED

#include<iostream>

struct var14
{
            double      a, b, x1, x2, dx;
            int         sz;
            double*     values;

                        var14();
                        ~var14();

                        var14(const var14& a) = delete;
            var14&      operator=(const var14& a) = delete;

            void        input(std::istream& is = std::cin);
            void        output(std::ostream& os = std::cout);
            void        outputValues(std::ostream& os = std::cout);

            void        calcValues();

            double      getS1();
            double      getS2();
};

#endif // VAR14_HPP_INCLUDED
