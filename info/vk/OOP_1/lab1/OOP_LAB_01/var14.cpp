#include<iomanip>
#include<algorithm>
#include "var14.hpp"

inline double f(double x, double a, double b) { return x>1 ? a/(x+b) : a*x*x-b; }

var14::var14() :
         a(10),
         b(9),
        x1(0),
        x2(3),
        dx(0.1),
        sz(0),
    values(nullptr)
{}

var14::~var14()
{
    delete [] values;
}

void var14::input(std::istream& is)
{
    if (is != std::cin)
        is>>a>>b>>x1>>x2>>dx;
    else
    {
        std::cout<<"Please, input data:\n";
        std::cout<<"     a: "; is>>a;
        std::cout<<"     b: "; is>>b;
        std::cout<<"    x1: "; is>>x1;
        std::cout<<"    x2: "; is>>x2;
        std::cout<<"    dx: "; is>>dx;
    }
}

void var14::output(std::ostream& os)
{
    const int width = 12;
    os.put(0xDA); for (int i=0; i<4; ++i){for (int j=0; j<width; ++j) os.put(0xC4); os.put(0xC2);} for (int j=0; j<width; ++j) os.put(0xC4); os.put(0xBF); os<<"\n";
    os.put(0xB3);
                  for (int j=0; j<width/2; ++j) os.put(' '); os<<"a"; for (int j=0; j<width-width/2-1; ++j) os.put(' '); os.put(0xB3);
                  for (int j=0; j<width/2; ++j) os.put(' '); os<<"b"; for (int j=0; j<width-width/2-1; ++j) os.put(' '); os.put(0xB3);
                  for (int j=0; j<width/2-1; ++j) os.put(' '); os<<"x1"; for (int j=0; j<width-width/2-1; ++j) os.put(' '); os.put(0xB3);
                  for (int j=0; j<width/2-1; ++j) os.put(' '); os<<"x2"; for (int j=0; j<width-width/2-1; ++j) os.put(' '); os.put(0xB3);
                  for (int j=0; j<width/2-1; ++j) os.put(' '); os<<"dx"; for (int j=0; j<width-width/2-1; ++j) os.put(' '); os.put(0xB3);
    os<<"\n";
    os.put(0xC3); for (int i=0; i<4; ++i){for (int j=0; j<width; ++j) os.put(0xC4); os.put(0xC5);} for (int j=0; j<width; ++j) os.put(0xC4); os.put(0xB4); os<<"\n";
    os.put(0xB3);
                  os.width(width); os<<a; os.put(0xB3);
                  os.width(width); os<<b; os.put(0xB3);
                  os.width(width); os<<x1; os.put(0xB3);
                  os.width(width); os<<x2; os.put(0xB3);
                  os.width(width); os<<dx; os.put(0xB3);
    os<<"\n";
    os.put(0xC0); for (int i=0; i<4; ++i){for (int j=0; j<width; ++j) os.put(0xC4); os.put(0xC1);} for (int j=0; j<width; ++j) os.put(0xC4); os.put(0xD9); os<<"\n";
}

void var14::outputValues(std::ostream& os)
{
    const int lw = 5, rw = 8;

    os.put(0xC9); for (int j=0; j<lw; ++j) os.put(0xCD); os.put(0xCB); for (int j=0; j<rw; ++j) os.put(0xCD); os.put(0xBB); os<<"\n";
    os<<std::fixed<<char(0xBA);
        for (int j=0; j<lw/2; ++j) os.put(' '); os<<"x"; for (int j=0; j<lw-lw/2-1; ++j) os.put(' '); os.put(0xBA);
        for (int j=0; j<(rw-4)/2; ++j) os.put(' '); os<<"f(x)"; for (int j=0; j<rw-4-(rw-4)/2; ++j) os.put(' '); os.put(0xBA);
    os<<"\n";

    double x = x1;
    for (int i = 0 ; i < sz ; ++i, x+=dx )
    {
        os.put(0xCC); for (int j=0; j<lw; ++j) os.put(0xCD); os.put(0xCE); for (int j=0; j<rw; ++j) os.put(0xCD); os.put(0xB9); os<<"\n";
        os<<std::setprecision(1)<<char(0xBA)<<" "<<x<<" "<<char(0xBA)<<std::setprecision(3)<<" ";
        os.width(6);
        os<<values[i]<<" "<<char(0xBA)<<"\n";
    }

    os.put(0xC8); for (int j=0; j<lw; ++j) os.put(0xCD); os.put(0xCA); for (int j=0; j<rw; ++j) os.put(0xCD); os.put(0xBC); os<<"\n";
}

void var14::calcValues()
{
    // allocate values[] in heap and fill it using f(x)

    sz = (x2 - x1) / dx + 1.1;

    values = new double[sz];
    double x = x1;
    for ( int i = 0 ; i < sz ; ++i, x+=dx )
        values[i] = f(x, a, b);
}

double var14::getS1()
{
    // returns average value of negative elements in values[]

    double sum = 0;
    int num = 0;
    for ( int i = 0 ; i < sz ; ++i )
        if (values[i] < 0)
        {
            sum += values[i];
            ++num;
        }

    if (num)
        return sum/num;

    std::cerr<<"ERROR in var14::getS1():  num == 0\n\t(there is no negative elements in array)\nreturning 0.0";
    return 0;
}

double var14::getS2()
{
    // returns sum of negative elements with even indexes(odd dots) in values[]

    double sum = 0;
    for ( int i = 0 ; i < sz ; i += 2 )
        if (values[i] < 0)
            sum += values[i];

    return sum;
}
