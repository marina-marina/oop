#include <fstream>
#include <ctime>
#include <algorithm>
#include "var14.hpp"

void initArr(double* a, int n, double left, double right);
int* normalize(double* a, int n, int left, int right);

const int A = 75;
const double delta = 0.1;

int main()
{
    var14 data;
    data.output();

    std::cout<<"Do you want to change parameters?(y/n): ";
    char ch;
    std::cin.get(ch);
    if (ch == 'y')
    {
        data.input();        data.output();
    }
    else if (ch != 'n')
        std::cout<<"I'll take it as 'NO'\n";

    data.calcValues();
    data.outputValues();

    int n = data.sz;
    double* arr = new double[n];
    double s1 = data.getS1(), s2 = data.getS2();
    initArr(arr, n, std::min(s1,s2), std::max(s1,s2));
    std::sort(arr, arr+n);
    int* normArr = normalize(arr, n, 0, A);

    std::ofstream outFile("Var14.txt");
    for ( int i = 0 ; i < n ; outFile<<"\n", ++i )
        for ( int j = 0 ; j < normArr[i] ; outFile.put(0xDE), ++j );

    delete [] normArr;
    delete [] arr;
}

void initArr(double* a, int n, double left, double right)
{
    const double koef = std::abs(delta*right);
    const int max_i = std::abs(right-left) / koef;

    srand(time(nullptr));
    for ( int i = 0 ; i < n ; ++i )
        a[i] = left + (rand()%(max_i+1))*koef;
}

int* normalize(double* a, int n, int left, int right)
{
    auto extremums = std::minmax_element(a, a+n);
    double koef = (right-left) / (*(extremums.second) - *(extremums.first));

    int* result = new int[n];
    for ( int i = 0 ; i < n ; ++i )
        result[i] = (a[i]-*(extremums.first))*koef+left;

    return result;
}
