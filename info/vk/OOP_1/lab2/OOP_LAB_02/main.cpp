#include<iostream>
#include<vector>
#include "book.hpp"
#include "input.hpp"

void showMenu();

int main()
{
    book testBook;

    for (;;)
    {
        system("cls");
        showMenu();
        std::cout<<"Input choice: ";
        unsigned short choice = safeInput(0, 2);
        if (choice == 1)
            testBook.init();
        else if (choice == 2)
            testBook.show();
        else
            break;
        system("pause");
    }
}

void showMenu()
{
    const unsigned lw = 15, rw = 60;
    const std::vector<std::string> lText = { "1", "2", "0" };
    const std::vector<std::string> rText = { "input data", "output data", "quit" };

    std::cout.put(0xC9); for (unsigned j=0; j<lw; ++j) std::cout.put(0xCD); std::cout.put(0xCB); for (unsigned j=0; j<rw; ++j) std::cout.put(0xCD); std::cout.put(0xBB); std::cout<<"\n";
    std::cout<<char(0xBA);
        for (unsigned j=0; j<(lw-lText[0].length())/2; ++j) std::cout.put(' '); std::cout<<lText[0]; for (unsigned j=0; j<lw-lText[0].length()-(lw-lText[0].length())/2; ++j) std::cout.put(' '); std::cout.put(0xBA);
        for (unsigned j=0; j<(rw-rText[0].length())/2; ++j) std::cout.put(' '); std::cout<<rText[0]; for (unsigned j=0; j<rw-rText[0].length()-(rw-rText[0].length())/2; ++j) std::cout.put(' '); std::cout.put(0xBA);
    std::cout<<"\n";

    std::cout.put(0xCC); for (unsigned j=0; j<lw; ++j) std::cout.put(0xCD); std::cout.put(0xCE); for (unsigned j=0; j<rw; ++j) std::cout.put(0xCD); std::cout.put(0xB9); std::cout<<"\n";

    std::cout<<char(0xBA);
        for (unsigned j=0; j<(lw-lText[1].length())/2; ++j) std::cout.put(' '); std::cout<<lText[1]; for (unsigned j=0; j<lw-lText[1].length()-(lw-lText[1].length())/2; ++j) std::cout.put(' '); std::cout.put(0xBA);
        for (unsigned j=0; j<(rw-rText[1].length())/2; ++j) std::cout.put(' '); std::cout<<rText[1]; for (unsigned j=0; j<rw-rText[1].length()-(rw-rText[1].length())/2; ++j) std::cout.put(' '); std::cout.put(0xBA);
    std::cout<<"\n";

    std::cout.put(0xCC); for (unsigned j=0; j<lw; ++j) std::cout.put(0xCD); std::cout.put(0xCE); for (unsigned j=0; j<rw; ++j) std::cout.put(0xCD); std::cout.put(0xB9); std::cout<<"\n";

    std::cout<<char(0xBA);
        for (unsigned j=0; j<(lw-lText[2].length())/2; ++j) std::cout.put(' '); std::cout<<lText[2]; for (unsigned j=0; j<lw-lText[2].length()-(lw-lText[2].length())/2; ++j) std::cout.put(' '); std::cout.put(0xBA);
        for (unsigned j=0; j<(rw-rText[2].length())/2; ++j) std::cout.put(' '); std::cout<<rText[2]; for (unsigned j=0; j<rw-rText[2].length()-(rw-rText[2].length())/2; ++j) std::cout.put(' '); std::cout.put(0xBA);
    std::cout<<"\n";

    std::cout.put(0xC8); for (unsigned j=0; j<lw; ++j) std::cout.put(0xCD); std::cout.put(0xCA); for (unsigned j=0; j<rw; ++j) std::cout.put(0xCD); std::cout.put(0xBC); std::cout<<"\n";
}
