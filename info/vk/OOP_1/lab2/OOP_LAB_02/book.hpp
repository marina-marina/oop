#ifndef BOOK_HPP_INCLUDED
#define BOOK_HPP_INCLUDED

#include<string>

class book
{
private:
                std::string                 author;
                std::string                 title;
                unsigned                    year;
                unsigned                    totalReadings;
                double                      readingsPerYear;

public:
    explicit                                book(std::string a = "N/A", std::string t = "N/A", unsigned y = 0, unsigned tot = 0, double r = 0.0);
                void                        init();
                void                        show() const;
};

#endif // BOOK_HPP_INCLUDED
