#ifndef BOOK_HPP_INCLUDED
#define BOOK_HPP_INCLUDED

#include<string>

class book
{
private:
                std::string                 author;
                std::string                 title;
                unsigned                    year;
                unsigned                    totalReadings;
                double                      readingsPerYear;

                unsigned                    revSize;
                int*                        reviewMarks;

public:
    explicit                                book(std::string a = "N/A", std::string t = "N/A", unsigned y = 0, unsigned tot = 0, double r = 0.0);
                                            book(const book& b);
                                            ~book();

                void                        init();
                void                        show() const;
                void                        addReading(){ ++totalReadings; };
                void                        subReading(){ if (totalReadings) --totalReadings; };


};

#endif // BOOK_HPP_INCLUDED
