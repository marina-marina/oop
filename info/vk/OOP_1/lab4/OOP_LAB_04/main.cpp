#include<iostream>
#include "book.hpp"

int main()
{
    {
        std::cout<<"Entering new block\n";
        std::cout<<"Creating book object from scratch...\n";
        book testBook1;
        testBook1.show();
        testBook1.init();
        std::cout<<"Creating book object copying other one...\n";
        book testBook2(testBook1);
        testBook2.show();
        std::cout<<"Quitting block\n";
    }

    std::cout<<"\n\nGoodbye!\n";
}
