#ifndef MYLIST_HPP_INCLUDED
#define MYLIST_HPP_INCLUDED

template <typename T> class list
{
private:
	class node
	{
		friend class list<T>;
	private:
		node *next;
		T val;

		node(): next(nullptr){}
		node(T data): next(nullptr), val(data){}
		~node(){}
	};

	node *head;
	int count;
public:
	list(): head(nullptr), count(0){}
	~list() {clear();}

	int size() const {return count;}

	int addAfter(T data, int x = -1)
	{
	    //return new size
	    //return -1 on error
	    if (x == -1) x = this->count;
		if (x>count||x<0) return -1;
		node *to_add = new node(data);
		if(x == 0)
        {
            to_add->next = head;
			head = to_add;
        }
		else
		{
			node *current = head;
			for(int i = 1 ; i < x ; ++i)
				current = current->next;
            to_add->next = current->next;
			current->next = to_add;
		}
		return ++count;
	}

	int del(int x)
	{
	    //return new size
	    //return -1 on error
		if (x>count||x<1) return -1;
		node *to_del = head;
		if (x == 1)
		{
			head = head->next;
			delete to_del;
		}
		else
		{
			node *current = head;
			for(int i = 1 ; i < x-1 ; ++i)
				current = current->next;
			to_del = current->next;
			current->next = current->next->next;
			delete to_del;
		}
		return --count;
	}

	void clear()
	{
		node *current = head;
		node *to_del = head;
		while(to_del != nullptr)
		{
			current = current->next;
			delete to_del;
			to_del = current;
		}
		head = nullptr;
		count = 0;
	}

	T getData(int x) const
	{
		node *current;
		for(current = head ; x > 1 ; --x)
			current = current->next;
		return current->val;
	}
};

#endif // MYLIST_HPP_INCLUDED
