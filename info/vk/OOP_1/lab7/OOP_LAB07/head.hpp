#ifndef HEAD_HPP_INCLUDED
#define HEAD_HPP_INCLUDED
#include<string>
#include<vector>
#include<iostream>
#include<fstream>
#include<iomanip>
#include<sstream>
using namespace std;

class Date
{
    unsigned year, month, day;
public:
    Date(unsigned y = 2014, unsigned m = 1, unsigned d = 1) : year(y), month(m), day(d) {}
    friend ostream& operator<<(ostream& os, const Date& d) { return os<<setfill('0')<<setw(2)<<d.day<<"."<<setw(2)<<d.month<<"."<<setw(4)<<d.year<<setfill(' '); }
};

enum class Position { ASSISTANT, LECTOR, SENIOR_LECTOR, DOCENT, PROFESSOR };

class Discipline
{
    string name;
    unsigned totHours, lecHours, pracHours, otherHours;
public:
    Discipline(string n = "N/A", unsigned tH = 0, unsigned lH = 0, unsigned pH = 0) :
        name(n), totHours(tH), lecHours(lH), pracHours(pH), otherHours(tH<lH+pH?0:tH-lH-pH) {}

    unsigned getLecHours()const {return lecHours;}
    unsigned getPracHours()const {return pracHours;}
    friend ostream& operator<<(ostream& os, const Discipline& d) { return os<<d.name<<" ["<<d.totHours<<" = "<<d.lecHours<<"+"<<d.pracHours<<"+"<<d.otherHours<<"]"; }
};

class Person
{
    string name, surname;
    Date birth;
public:
    Person(string n = "N/A", string s = "N/A", const Date& d = Date()) : name(n), surname(s), birth(d) {}
    friend ostream& operator<<(ostream& os, const Person& d) { return os<<d.surname<<", "<<d.name<<" ("<<d.birth<<")"; }
};

class Lector : public Person
{
    Position pos;
    vector<Discipline> discList;
public:
    Lector(const Person& per = Person(), Position p = Position::LECTOR, const vector<Discipline>& dL = vector<Discipline>()) :
        Person(per), pos(p), discList(dL) {}
    friend ostream& operator<<(ostream& os, const Lector& l)
    {
        ostringstream buf1, buf2, buf3;
        buf1<<((const Person&)l)<<" ";
        switch (l.pos)
        {
            case Position::ASSISTANT: buf2<<"Assistant"; break;
            case Position::LECTOR: buf2<<"Lector"; break;
            case Position::SENIOR_LECTOR: buf2<<"Senior lector"; break;
            case Position::DOCENT: buf2<<"docent"; break;
            case Position::PROFESSOR: buf2<<"Professor"; break;
        }
        for (auto& x: l.discList) buf3<<" "<<x;

        const int fieldsNo = 3;
        int wid[] = {30, 15, 100};
        string wor[] = {"Person", "Position", "Disciplines"};

        os<<char(201); for (int j = 0; j < fieldsNo; ++j){ for (int i = 0; i < wid[j]; i++) os<<char(205); os<<char(j<fieldsNo-1?209:187); } os<<"\n";

        os<<char(186);
        for (int i = 0; i < fieldsNo; ++i)
        {
            if (wor[i].length() > wid[i])
                wor[i] = string(wor[i], 0, wid[i]-2)+"..";
            int rightSpace = (wid[i]-wor[i].length())/2;
            int leftSpace = wid[i] - rightSpace - wor[i].length();
            if (rightSpace<0) rightSpace = 0;
            if (leftSpace<0) leftSpace = 0;
            os<<setw(leftSpace)<<""<<wor[i]<<setw(rightSpace)<<""<<char(i<fieldsNo-1?179:186);
        }
        os<<"\n";

        os<<char(199); for (int j = 0; j < fieldsNo; ++j){ for (int i = 0; i < wid[j]; i++) os<<char(196); os<<char(j<fieldsNo-1?197:182); } os<<"\n";

        os<<char(186);
        string s1 = buf1.str();
        if (s1.length() > wid[0])
            s1 = string(s1, 0, wid[0]-2)+"..";
        int rightSpace1 = (wid[0]-s1.length())/2;
        int leftSpace1 = wid[0] - rightSpace1 - s1.length();
        os<<setw(leftSpace1)<<""<<s1<<setw(rightSpace1)<<"";
        os<<char(179);
        string s2 = buf2.str();
        if (s2.length() > wid[1])
            s2 = string(s2, 0, wid[1]-2)+"..";
        int rightSpace2 = (wid[1]-s2.length())/2;
        int leftSpace2 = wid[1] - rightSpace2 - s2.length();
        os<<setw(leftSpace2)<<""<<s2<<setw(rightSpace2)<<"";
        os<<char(179);
        string s3 = buf3.str();
        if (s3.length() > wid[2])
            s3 = string(s3, 0, wid[2]-2)+"..";
        int rightSpace3 = (wid[2]-s3.length())/2;
        int leftSpace3 = wid[2] - rightSpace3 - s3.length();
        os<<setw(leftSpace3)<<""<<s3<<setw(rightSpace3)<<"";
        os<<char(186)<<"\n";

        os<<char(200); for (int j = 0; j < fieldsNo; ++j){ for (int i = 0; i < wid[j]; i++) os<<char(205); os<<char(j<fieldsNo-1?207:188); } os<<"\n";

        return os;
    }
};

template<typename T>
pair<double,double> average(const vector<T>& a)
{
    double avLecHours = 0.0;
    double avRatio = 0.0;
    for (auto& x: a)
    {
        avLecHours += x.getLecHours();
        avRatio += 1.0*x.getLecHours()/x.getPracHours();
    }
    return make_pair(avLecHours/a.size(), avRatio/a.size());
}

#endif // HEAD_HPP_INCLUDED