#include "head.hpp"
#include "myList.hpp"

int main()
{
    vector<Discipline> a = {Discipline("Math", 100, 50, 30), Discipline("OOP", 90, 50, 20)};
    pair<double, double> aver = average(a);
    cout<<aver.first<<" "<<aver.second<<endl;

    Lector n1, n2( Person("Ivan", "Ivanov", Date(1970, 5, 5)),
                   Position::PROFESSOR,
                   std::vector<Discipline>({Discipline("Math", 100, 50, 30), Discipline("OOP", 90, 50, 20)}) );
    list<Lector> b;
    b.addAfter(n1);
    b.addAfter(n2);
    b.addAfter(n1,1);
    b.del(2);
    for (int i = 0; i < b.size(); ++i)
        cout<<b.getData(i+1);
}