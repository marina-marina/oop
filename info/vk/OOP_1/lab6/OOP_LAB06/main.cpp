#include "head.hpp"

int n = 10;
Lector* a;
void addElem(const Lector& elem, int pos = 0);
void delElem(int pos = 0);

int main()
{
    Lector n1, n2( Person("Ivan", "Ivanov", Date(1970, 5, 5)),
                   Position::PROFESSOR,
                   std::vector<Discipline>({Discipline("Math", 100, 50, 30), Discipline("OOP", 90, 50, 20)}) );

    std::string fN;
    std::cout<<"\nInput file name: ";
    std::cin>>fN;
    std::ofstream out(fN);
    a = new Lector[n]{n2, n1, n2, n2, n2, n2, n2, n2, n2, n2};
    for (Lector* p = a; p-a<n; out<<*p++);

    out<<"\n\n\nDeleting 2nd\n";
    delElem(1);
    for (Lector* p = a; p-a<n; out<<*p++);

    out<<"\n\n\nAdding before 3rd\n";
    addElem(n1, 2);
    for (Lector* p = a; p-a<n; out<<*p++);

    delete [] a;
}


void addElem(const Lector& elem, int pos)
{
    Lector* buf = new Lector[n+1];
    for (int i = 0; i < n; ++i)
        buf[ i+(i>=pos) ] = a[i];
    buf[pos] = elem;
    delete [] a;
    a = buf;
    ++n;
}

void delElem(int pos)
{
    Lector* buf = new Lector[n-1];
    for (int i = 0; i < n-1; ++i)
        buf[i] = a[ i+(i>=pos) ];
    delete [] a;
    a = buf;
    --n;
}