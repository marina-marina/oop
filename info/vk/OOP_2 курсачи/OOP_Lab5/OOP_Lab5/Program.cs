﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab5
{
    interface IDisplayable
    {
        void Display();
    }

    class Date : IDisplayable
    {
        const int
            DEFAULT_YEAR = 0,
            DEFAULT_MONTH = 1,
            DEFAULT_DAY = 1,
            MIN_MONTH = 1,
            MAX_MONTH = 12,
            MIN_DAY = 1;
        int month, day;
        public int Year { get; set; }
        public int Month
        {
            get{ return month; }
            set{ month = (value<MIN_MONTH || value>MAX_MONTH ? DEFAULT_MONTH : value); }
        }
        public int Day
        {
            get{ return day; }
            set{ day = (value<MIN_DAY || value>DaysInMonth(Year, Month) ? DEFAULT_DAY : value); }
        }

        public Date(int y = DEFAULT_YEAR, int m = DEFAULT_MONTH, int d = DEFAULT_DAY)
        {
            Year = y;
            Month = m;
            Day = d;
        }

        int DaysInMonth(int year, int month)
        {
            int[] days = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            if (year%400==0 || (year%4==0 && year%100!=0)) //leap year
                days[1] = 29;
            return days[month-1];
        }

        public void Display()
        {
            Console.Write(Month+"/"+Day+"/"+Year);
        }
    }
    class Person : IDisplayable
    {
        string name, surname;
        Date birthDate;
        long telephone;
        public string Name {get{return name;}}
        public string Surname {get{return surname;}}
        public Date BirthDate {get{return birthDate;}}
        public long Telephone {get{return telephone;}}

        public Person() : this("N/A", "N/A", new Date(), 1000000) {}
        public Person(Person p)
        {
            this.name = p.Name;
            this.surname = p.Surname;
            this.birthDate = p.BirthDate;
            this.telephone = p.Telephone;
        }
        public Person(string n, string sur, Date birth, long phone)
        {
            this.name = n;
            this.surname = sur;
            this.birthDate = birth;
            this.telephone = phone;
        }

        public void Display()
        {
            Console.Write("{0},{1} (",surname,name);
            birthDate.Display();
            Console.Write(") - {0}",telephone);
        }
    }
    class Firm : IDisplayable
    {
        string name, fond;
        Person contactPerson;

        public Firm() : this("N/A","N/A", new Person()) {}
        public Firm(string n, string f, Person cp)
        {
            this.name = n;
            this.fond = f;
            this.contactPerson = cp;
        }

        public void Display()
        {
            Console.Write("{0}, fond of {1}, contact person - ",name,fond);
            contactPerson.Display();
        }
    }
    enum Position { manager, marketer, headOfSector, branchManager };
    class Employee : Person, IDisplayable
    {
        Position position;
        
        public Employee() : this(new Person(), Position.marketer) {}
        public Employee(Person p, Position pos) : base(p)
        {
            this.position = pos;
        }

        public void Display()
        {
            base.Display();
            Console.Write(", ");
            switch (position)
            {
                case Position.manager: Console.Write("manager"); break;
                case Position.marketer: Console.Write("marketer"); break;
                case Position.headOfSector: Console.Write("head of sector"); break;
                case Position.branchManager: Console.Write("branch manager"); break;
                default: Console.Write("UNKNOWN POSITION"); break;
            }
        }
    }
    class Contract : IDisplayable
    {
        Firm firm;
        Date date;
        double price;
        Employee representative;

        public Contract() : this(new Firm(), new Date(), 0.0, new Employee()) {}
        public Contract(Firm f, Date d, double p, Employee e)
        {
            this.firm = f;
            this.date = d;
            this.price = p;
            this.representative = e;
        }

        public void Display()
        {
            firm.Display();
            Console.Write(" made contract at ");
            date.Display();
            Console.Write(", total price = {0}, representative - ",price);
            representative.Display();
        }
    }
    class EmployeeActivity : IDisplayable
    {
        Employee employee;
        List<Contract> contracts;

        public EmployeeActivity() : this(new Employee(), new List<Contract>()) {}
        public EmployeeActivity(Employee emp, List<Contract> li)
        {
            this.employee = emp;
            this.contracts = li;
        }

        public void Display()
        {
            employee.Display();
            Console.WriteLine(":");
            for (int i = 0; i < contracts.Count; ++i)
            {
                Console.Write("\t-");
                contracts[i].Display();
                Console.WriteLine();
            }
        }
    }
    class ResearchUnit : IDisplayable
    {
        List<Employee> staff;
        List<Contract> contracts;
        double totalPrice;

        public ResearchUnit() : this(new List<Employee>(), new List<Contract>(), 0.0) {}
        public ResearchUnit(List<Employee> st, List<Contract> con, double tP)
        {
            this.staff = st;
            this.contracts = con;
            this.totalPrice = tP;
        }

        public void Display()
        {
            Console.WriteLine("Staff:");
            for (int i = 0; i < staff.Count; ++i)
            {
                Console.Write("\t-");
                staff[i].Display();
                Console.WriteLine();
            }
            Console.WriteLine("Contracts:");
            for (int i = 0; i < contracts.Count; ++i)
            {
                Console.Write("\t-");
                contracts[i].Display();
                Console.WriteLine();
            }
            Console.WriteLine("Total Price: ${0}", totalPrice);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> staff = new List<Employee>();
            staff.Add(new Employee( new Person("Jack","Smith",new Date(1980,1,10),5551425), Position.manager ));
            staff.Add(new Employee( new Person("Jane","Williams",new Date(1976,5,8),5551428), Position.headOfSector ));
            staff.Add(new Employee( new Person("Bill","Turner",new Date(1960,7,7),5551449), Position.branchManager ));

            double price = 0.0;
            List<Contract> contracts = new List<Contract>();
            contracts.Add(new Contract( new Firm("Oak Inc.", "West", new Person("Will","Tako",new Date(1982,12,12),7841545)), new Date(2015,1,10), 789.98, staff[0]));
            contracts.Add(new Contract( new Firm("Oak Inc.", "West", new Person("Will","Tako",new Date(1982,12,12),7841545)), new Date(2015,2,10), 800.07, staff[2]));

            ResearchUnit test = new ResearchUnit(staff, contracts, 1590.05);
            test.Display();

            Console.ReadKey();
        }
    }
}
