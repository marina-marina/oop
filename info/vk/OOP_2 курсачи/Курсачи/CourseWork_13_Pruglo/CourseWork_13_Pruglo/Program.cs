﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CourseWork_13_Pruglo
{
    /**
     * 7.	інтерфейси
    */
    interface IDisplayable
    {
        void show();
    }

    [Serializable()]class Date : IDisplayable
    {
        const int
                                    DEFAULT_YEAR = 0,
                                    DEFAULT_MONTH = 1,
                                    DEFAULT_DAY = 1,
                                    MIN_MONTH = 1,
                                    MAX_MONTH = 12,
                                    MIN_DAY = 1;
        int                         month, day;
        /**
         * 4.	властивості (автоматичні)
        */
        public int                  Year { get; set; }
        /**
         * 3.	властивості (обмеження можливих значень полів)
        */
        public int                  Month
        {
            get{ return month; }
            set{ month = (value<MIN_MONTH || value>MAX_MONTH ? DEFAULT_MONTH : value); }
        }
        public int                  Day
        {
            get{ return day; }
            set{ day = (value<MIN_DAY || value>DaysInMonth(Year, Month) ? DEFAULT_DAY : value); }
        }

        public                      Date(string s)
        {
            //Console.WriteLine("Date => "+s);
            string[] elements = s.Split(Program.SPLIT_SYMBOLS[4]);
            if (elements.Length != 3)
                Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^REALLY BAD #4");
            Year = Convert.ToInt32(elements[0]);
            month = Convert.ToInt32(elements[1]);
            day = Convert.ToInt32(elements[2]);
        }
        public                      Date(int y = DEFAULT_YEAR, int m = DEFAULT_MONTH, int d = DEFAULT_DAY)
        {
            Year = y;
            Month = m;
            Day = d;
        }

        public static int           DaysInMonth(int year, int month)
        {
            int[] days = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            if (year%400==0 || (year%4==0 && year%100!=0)) //leap year
                days[1] = 29;
            return days[month-1];
        }

        void                        IDisplayable.show()
        {
            Console.Write(Month+"/"+Day+"/"+Year);
        }
        public string               toString()
        {
            string result = "";
            
            result += Convert.ToString(Year)+Program.SPLIT_SYMBOLS[4];
            result += Convert.ToString(month)+Program.SPLIT_SYMBOLS[4];
            result += Convert.ToString(day);

            return result;
        }

        public bool                 contains(double key)
        {
            return (Year == key || Month == key || Day == key);
        }
        public bool                 contains(string key)
        {
            return false;
        }
    }
    [Serializable()]class Buyer : IDisplayable
    {
        protected int               id;

        public                      Buyer(string s)
        {
            //Console.WriteLine("Buyer => "+s);
            id = Convert.ToInt32(s);
        }
        public                      Buyer() : this(0) {}
        public                      Buyer(int n)
        {
            this.id = n;
        }

        /**
         * 8.	інтерфейси (їх перевизначення)
        */
        void                        show()
        {
            Console.WriteLine("overloaded interface function");
        }
        void                        IDisplayable.show()
        {
            Console.Write("#{0}",id);
        }
        public string               toString()
        {
            string result = "";

            result += Convert.ToString(id);

            return result;
        }

        public bool                 contains(double key)
        {
            return key == id;
        }
        public bool                 contains(string key)
        {
            return false;
        }
    }
    /**
     * 1.	успадковування
    */
    class PermanentBuyer : Buyer, IDisplayable
    {
        double moneySpent;
        double discountPercent;

        public PermanentBuyer() : this(0.0, 0.0) {}
        public PermanentBuyer(double monSpent, double discount)
        {
            this.moneySpent = monSpent;
            this.discountPercent = discount;
        }

        void IDisplayable.show()
        {
            Console.Write("#{0}",base.id);
            Console.Write(", ");
            Console.Write("${0} (-{1}%)",moneySpent,discountPercent);
        }
    }
    [Serializable()]class Product : IDisplayable
    {
        string                      name;
        double                      price;
        double                      discountPercent;
        public double               Price { set{price=value;} get{return price;}}
        public double               DiscountPercent { set{discountPercent=value;} get{return discountPercent;}}

        public                      Product(string s)
        {
            //Console.WriteLine("Product => "+s);
            string[] elements = s.Split(Program.SPLIT_SYMBOLS[3]);
            if (elements.Length != 3)
                Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^REALLY BAD #2 {0},{1}",elements.Length,3);
            
            name = elements[0];
            price = Convert.ToDouble(elements[1]);
            discountPercent = Convert.ToDouble(elements[2]);
        }
        public                      Product() : this("N/A", 0.0, 0.0) {}
        public                      Product(string n, double p, double dP)
        {
            this.name = n;
            this.price = p;
            this.discountPercent = dP;
        }
        
        void                        IDisplayable.show()
        {
            Console.Write("{0}: ${1}(-{2}%)",name,price,discountPercent);
        }
        public string               toString()
        {
            string result = "";

            result += name+Program.SPLIT_SYMBOLS[3];
            result += Convert.ToString(price)+Program.SPLIT_SYMBOLS[3];
            result += Convert.ToString(discountPercent);

            return result;
        }

        public bool                 contains(double key)
        {
            return (price == key || discountPercent == key);
        }
        public bool                 contains(string key)
        {
            return name == key;
        }
    }
    [Serializable()]class BuyingOperation : IDisplayable
    {
        /**
         * 2.	агрегація
        */
        Date                        date;
        Buyer                       client;
        List<Product>               productsList;
        double                      totalPrice;
        double                      toPay;
        public double               ToPay{get{return toPay;} set{toPay = value;}}

        public                      BuyingOperation(string s)
        {
            //Console.WriteLine("Buying Operation => "+s);
            string[] elements = s.Split(Program.SPLIT_SYMBOLS[1]);
            if (elements.Length != 5)
                Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^REALLY BAD #7 {0},{1}",elements.Length,5);

            date = new Date(elements[0]);
            client = new Buyer(elements[1]);
            
            string[] products = elements[2].Split(Program.SPLIT_SYMBOLS[2]);
            productsList = new List<Product>();
            if (products.Length > 1)
            {
                for (int i = 0; i < products.Length; ++i)
                    productsList.Add(new Product(products[i]));
            }
            
            totalPrice = Convert.ToDouble(elements[3]);
            toPay = Convert.ToDouble(elements[4]);
        }
        public                      BuyingOperation() : this(new Date(), new Buyer(), new List<Product>()) {}
        public                      BuyingOperation(Date d, Buyer b, List<Product> li)
        {
            date = d;
            client = b;
            productsList = new List<Product>(li);
            totalPrice = 0.0;
            toPay = 0.0;
            for (int i = 0; i < productsList.Count; ++i)
            {
                totalPrice += li[i].Price;
                toPay += Math.Round(li[i].Price*(100.0-li[i].DiscountPercent)/100.0, 2);
            }
        }

        /**
         * 5.	перевантаження операторів (визначення)
        */
        public static DailyReport   operator+(BuyingOperation op1, BuyingOperation op2)
        {
            return new DailyReport(op1, op2);
        }
        public bool                 contains(double key)
        {
            bool listContains = false;
            for (int i = 0; i < productsList.Count(); ++i)
                if (productsList[i].contains(key))
                    listContains = true;

            return (   key == totalPrice
                    || key == toPay
                    || date.contains(key)
                    || client.contains(key)
                    || listContains
                   );
        }
        public bool                 contains(string key)
        {
            bool listContains = false;
            for (int i = 0; i < productsList.Count(); ++i)
                if (productsList[i].contains(key))
                    listContains = true;

            return (   date.contains(key)
                    || client.contains(key)
                    || listContains
                   );
        }

        void                        IDisplayable.show()
        {
            ((IDisplayable)date).show();
            Console.Write("; client ");
            ((IDisplayable)client).show();
            Console.Write("; bought ");
            for (int i = 0; i < productsList.Count; ++i)
            {
                ((IDisplayable)productsList[i]).show();
                if (i < productsList.Count-1)
                    Console.Write(", ");
            }
            Console.Write("; Total price: {0}, to pay: {1}",totalPrice,toPay);
        }
        public string               toString()
        {
            string result = "";

            result += date.toString()+Program.SPLIT_SYMBOLS[1];
            result += client.toString()+Program.SPLIT_SYMBOLS[1];
            for (int i = 0; i < productsList.Count; ++i)
            {
                result += productsList[i].toString();
                if (i<productsList.Count-1)
                    result += Program.SPLIT_SYMBOLS[2];
            }
            result += Program.SPLIT_SYMBOLS[1];
            result += Convert.ToString(totalPrice)+Program.SPLIT_SYMBOLS[1];
            result += Convert.ToString(toPay);

            return result;
        }
    }
    [Serializable()]class DailyReport : IDisplayable
    {
        List<BuyingOperation>       operations;

        public                      DailyReport(string s)
        {
            //Console.WriteLine("Daily Report => "+s);
            operations = new List<BuyingOperation>();
            string[] elements = s.Split(Program.SPLIT_SYMBOLS[0]);
            if (elements.Length>1)
            {
                for (int i = 0; i < elements.Length; ++i)
                {
                    try
                    {
                        this.operations.Add( new BuyingOperation(elements[i]) );
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception '"+e.Message+"' while creating new BuyingOperation object from string. Creating with default constructor.");
                        this.operations.Add( new BuyingOperation() );
                    }
                }
            }
        }
        public                      DailyReport(DailyReport dr)
        {
            operations = new List<BuyingOperation>(dr.operations);
        }
        public                      DailyReport(params BuyingOperation[] ops)
        {
            operations = new List<BuyingOperation>();
            for (int i = 0; i < ops.Length; ++i)
                operations.Add(ops[i]);
        }

        public int                  operationsAmount() { return operations.Count; }
        public void                 Clear() { operations.Clear(); }
        public void                 AddOperation(BuyingOperation op)
        {
            operations.Add(op);
        }
        /**
         * 9.	індексатори (визначення)
        */
        public BuyingOperation      this[int i]
        {
            get { if(i<0||i>operations.Count) throw new ArgumentOutOfRangeException(); return operations[i]; }
            set { operations[i] = value; }
        }
        public static DailyReport   operator+(DailyReport dr, BuyingOperation op)
        {
            DailyReport result = new DailyReport(dr);
            result.AddOperation(op);
            return result;
        }
    
        void                        IDisplayable.show()
        {
            Console.WriteLine("Shopping list:");
            for (int i = 0; i < operations.Count; ++i)
            {
                Console.Write("\t-");
                ((IDisplayable)operations[i]).show();
                Console.WriteLine();
            }
        }
        public string               toString()
        {
            string result = "";
            
            for (int i = 0; i < operations.Count; ++i)
            {
                result += operations[i].toString();
                if (i<operations.Count-1)
                    result += Program.SPLIT_SYMBOLS[0];
            }
            
            return result;
        }
    }

    /**
     * 15.	меню
    */
    class Menu : IDisplayable
    {
        /**
         * 14.	делегати (патерн джерело-спостерігач)
        */
        class Observable : IObservable<bool>
        {
            private class Unsubscriber : IDisposable
            {
                OnNextDelegate oRefresh;
                OnCompletedDelegate oRelease;
                IObserver<bool> observer;

                public Unsubscriber(OnNextDelegate ond, OnCompletedDelegate or, IObserver<bool> ob)
                {
                    oRefresh = ond;
                    oRelease = or;
                    observer = ob;
                }

                public void Dispose()
                {
                    oRefresh -= observer.OnNext;
                    oRelease -= observer.OnCompleted;
                }
            }

            delegate void OnNextDelegate(bool hasChanged);
            delegate void OnCompletedDelegate();
            OnNextDelegate          observersRefresh;
            OnCompletedDelegate     observersRelease;   

            public IDisposable      Subscribe(IObserver<bool> observer)
            {
                observersRefresh += observer.OnNext;
                observersRelease += observer.OnCompleted;

                return new Unsubscriber(observersRefresh, observersRelease, observer);
            }
            public void             Call()
            {
                observersRefresh(true);
            }
            public void             Finish()
            {
                observersRelease();
            }
        }
        class Observer : IObserver<bool>
        {
            public double value;

            /**
             * 12.	делегати (розрахунок статистичних функцій)
            */
            public delegate double StatisticsDelegate();
            StatisticsDelegate statisticsFunction;

            private IDisposable     unsubscriber;

            public                  Observer(IObservable<bool> provider, StatisticsDelegate sFunc)
            {
                statisticsFunction = sFunc;
                this.Subscribe(provider);
            }

            public void             Subscribe(IObservable<bool> provider)
            {
                unsubscriber = provider.Subscribe(this);
            }
            public void             Unsubscribe()
            {
                unsubscriber.Dispose();
            }

            public void             OnNext(bool hasChanged)
            {
                if (hasChanged)
                    value = statisticsFunction();
            }
            public void             OnError(Exception e){}
            public void             OnCompleted()
            {
                // Nothing to do
            }
        }
        Observable      changesHandler = new Observable();
        List<Observer>  statisticians = new List<Observer>();
        
        DailyReport     data;
        double          totalPrice()
        {
            double result = 0.0;
            for (int i = 0; i < data.operationsAmount(); ++i)
                result += data[i].ToPay;
            return result;
        }
        double          averagePrice()
        {
            double result = 0.0;
            for (int i = 0; i < data.operationsAmount(); ++i)
                result += data[i].ToPay/data.operationsAmount();
            return result;
        }

        /**
         * 13.	делегати (виведення значень функцій на екран)
        */
        delegate void Action();
        Dictionary<int, Action> options;

        public          Menu()
        {
            data = new DailyReport();
            statisticians.Add(new Observer(changesHandler, totalPrice));
            statisticians.Add(new Observer(changesHandler, averagePrice));

            options = new Dictionary<int,Action>
            {
                {1, input},
                {2, output},
                {3, writeToDisk},
                {4, readFromDisk},
                {5, search},
                {6, demonstrate},
                {7, exit}
            };
        }

        public void     show()
        {
            Console.WriteLine("       MENU");
            Console.WriteLine("1. Input data");
            Console.WriteLine("2. Output data");
            Console.WriteLine("3. Write to disk");
            Console.WriteLine("4. Read from disk");
            Console.WriteLine("5. Search data");
            Console.WriteLine("6. Demonstrate");
            Console.WriteLine("7. Exit");
        }
        public void     engage()
        {
            Console.Clear();
            this.show();
            Console.WriteLine("\nYour choice[int 1..7]:");
            int choice = Program.safeInputInt32(1,7);
            //if (!options.ContainsKey(choice))
            //    throw new ArgumentException(string.Format("Operation {0} is invalid", op), "op");
            Console.Clear();
            options[choice]();
            if (choice != 7)
            {
                Console.WriteLine("\nPress any key to return to menu");
                Console.ReadKey();
                this.engage();
            }
        }

        void            input()
        {
            Console.WriteLine("Let's add some buying operations!");
            
            Console.Write("Input amount of operations[int >=0]: ");
            int n = Program.safeInputInt32(0);

            for (int i = 0; i < n; ++i)
            {
                Console.WriteLine("{0} operation:",i+1);

                int t_year,t_month,t_day;
                Console.WriteLine("\tDate:");
                Console.Write("\t\tYear[int]:\t\t");
                t_year = Program.safeInputInt32();
                Console.Write("\t\tMonth[int 1..12]:\t");
                t_month = Program.safeInputInt32(1,12);
                Console.Write("\t\tDay[int 1.."+Date.DaysInMonth(t_year,t_month)+"]:\t");
                t_day = Program.safeInputInt32(1,Date.DaysInMonth(t_year,t_month));
                Date t_date = new Date(t_year, t_month, t_day);

                int t_id;                
                Console.WriteLine("\tBuyer:");
                Console.Write("\t\tID[int]:\t\t");
                t_id = Program.safeInputInt32();
                Buyer t_buyer = new Buyer(t_id);

                int t_productsAmount;                
                Console.WriteLine("\tList of products:");
                Console.Write("\t\tAmount[int >=0]:\t");
                t_productsAmount = Program.safeInputInt32(0);
                List<Product> t_productsList= new List<Product>(t_productsAmount);
                for (int j = 0; j < t_productsAmount; ++j)
                {
                    Console.WriteLine("\t\t{0} product:",j+1);

                    string t_name;
                    double t_price, t_discount;
                    Console.Write("\t\t\tName:\t\t");
                    t_name = Console.ReadLine();
                    Console.Write("\t\t\tPrice[double >=0]:\t\t");
                    t_price = Program.safeInputDouble(0.0);
                    Console.Write("\t\t\tDiscount[double >=0]:\t");
                    t_discount = Program.safeInputDouble(0.0);

                    t_productsList.Add(new Product(t_name, t_price, t_discount));
                }

                data.AddOperation(new BuyingOperation(t_date, t_buyer, t_productsList));
            }
            changesHandler.Call();
        }
        void            output()
        {
            if (data.operationsAmount()==0)
            {
                Console.WriteLine("There are no buying operations in base.");
                Console.WriteLine("Add some operations:");
                Console.WriteLine("   - manually via menu item \"Input data\"");
                Console.WriteLine("   - read form file via menu item read from disk \"Input data\"");
            }
            else
            {
                //╔═╗║ ╬ ╦╩ ╠ ╣ ╝ ╚
                Console.Write("╔══════");
                for (int i = 0; i < Program.WIN_WIDTH-9; ++i)
                    Console.Write("═");
                Console.WriteLine("╗");
            
                Console.Write("║");
                for (int j = 0; j < (Program.WIN_WIDTH-3-18)/2; ++j)
                    Console.Write(" ");
                Console.Write("List of Operations");
                for (int j = 0; j < (Program.WIN_WIDTH-3-18)-(Program.WIN_WIDTH-3-18)/2; ++j)
                    Console.Write(" ");
                Console.WriteLine("║");
            
                Console.Write("╠═════╦");
                for (int i = 0; i < Program.WIN_WIDTH-9; ++i)
                    Console.Write("═");
                Console.WriteLine("╣");

                Console.Write("║  #  ║");
                for (int j = 0; j < (Program.WIN_WIDTH-9-16)/2; ++j)
                    Console.Write(" ");
                Console.Write("Buying Operation");
                for (int j = 0; j < (Program.WIN_WIDTH-9-16)-(Program.WIN_WIDTH-9-16)/2; ++j)
                    Console.Write(" ");
                Console.WriteLine("║");
            
                if (data.operationsAmount() > 0)
                {
                    Console.Write("╠═════╬");
                    for (int i = 0; i < Program.WIN_WIDTH-9; ++i)
                        Console.Write("═");
                    Console.WriteLine("╣");
                }
                else
                {
                    Console.Write("╚═════╩");
                    for (int j = 0; j < Program.WIN_WIDTH-9; ++j)
                        Console.Write("═");
                    Console.WriteLine("╝");
                }

                for (int i = 0; i < data.operationsAmount(); ++i)
                {
                    Console.Write("║{0,3}  ║ ",i+1);
                    ((IDisplayable)data[i]).show();
                    Console.SetCursorPosition(Program.WIN_WIDTH-2,Console.CursorTop);
                    Console.WriteLine("║");
                    if (i != data.operationsAmount()-1)
                    {
                        Console.Write("╠═════╬");
                        for (int j = 0; j < Program.WIN_WIDTH-9; ++j)
                            Console.Write("═");
                        Console.WriteLine("╣");
                    }
                    else
                    {
                        Console.Write("╚═════╩");
                        for (int j = 0; j < Program.WIN_WIDTH-9; ++j)
                            Console.Write("═");
                        Console.WriteLine("╝");
                    }
                }

                Console.WriteLine("Total price =   {0,7:0.000}",statisticians[0].value);
                Console.WriteLine("Average price = {0,7:0.000}",statisticians[1].value);
            }
        }
        void            writeToDisk()
        {
            Console.WriteLine("NOTE: Default save files(with 10 real objects) are named \"default.txt\"/\"default.bin\"\n");

            Console.WriteLine("Write to binary or text file?");
            Console.WriteLine("1. Binary(.bin)");
            Console.WriteLine("2. Text(.txt)");
            Console.WriteLine("\nYour choice[int 1..2]:");
            int choice = Program.safeInputInt32(1,2);

            Console.Write("Input file name (without extension): ");
            string fileName = Console.ReadLine();
            string sure = "y";
            if (fileName == "default")
            {
                Console.WriteLine("WARNING! You are trying to rewrite default files. Are you sure? y/n");
                sure = Console.ReadLine();
            }

            if (sure != "y")
                Console.WriteLine("Canceled by user");
            else
            {
                if (choice == 1)
                    writeToBinaryFile(fileName+".bin");
                else
                    writeToTextFile(fileName+".txt");

                Console.WriteLine("Data had been written succesfully!");
            }
        }
        /**
         * 18.	запис даних у бінарний файл
        */
        void            writeToBinaryFile(string fileName)
        {
            Stream stream = File.Open(fileName, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, data);
            stream.Close();
        }
        /**
         * 16.	запис даних у текстовий файл
        */
        void            writeToTextFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            StreamWriter writer = file.CreateText();
            writer.Write(data.toString());
            writer.Close();
        }
        void            readFromDisk()
        {
            Console.WriteLine("NOTE: default save files(with 10 real objects) are named \"default.txt\"/\"default.bin\"\n");
            
            Console.WriteLine("Read from binary or text file?");
            Console.WriteLine("1. Binary(.bin)");
            Console.WriteLine("2. Text(.txt)");
            Console.WriteLine("\nYour choice[int 1..2]:");
            int choice = Program.safeInputInt32(1,2);

            Console.Write("Input file name (without extension): ");
            string fileName = Console.ReadLine();

            if (choice == 1)
                readFromBinaryFile(fileName+".bin");
            else
                readFromTextFile(fileName+".txt");

            changesHandler.Call();
        }
        /**
         * 19.	зчитування даних з бінарного файлу
        */
        void            readFromBinaryFile(string fileName)
        {
            try
            {
                Stream stream = File.Open(fileName, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                data = (DailyReport)bFormatter.Deserialize(stream);
                stream.Close();
                Console.WriteLine("Data had been read succesfully!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Data had not been read!");
            }
        }
        /**
         * 17.	зчитування даних з текстового файлу
        */
        void            readFromTextFile(string fileName)
        {            
            try
            {
                StreamReader reader = File.OpenText(fileName);
                data = new DailyReport(reader.ReadLine());
                reader.Close();
                Console.WriteLine("Data had been read succesfully!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Data had not been read!");
            }            
        }
        /**
         * 20.	пошук інформації за числовим полем
         * 21.	пошук інформації за текстовим полем
        */
        void            search()
        {
            Console.WriteLine("What do you want to search?");
            Console.WriteLine("1. Number");
            Console.WriteLine("2. String");
            Console.WriteLine("\nYour choice[int 1..2]:");
            int choice = Program.safeInputInt32(1,2);

            double doubleKey = 0.0;
            string stringKey = "";
            if (choice == 1)
            {
                Console.WriteLine("Input number[double]:");
                doubleKey = Program.safeInputDouble();
            }
            else
            {
                Console.WriteLine("Input string:");
                stringKey = Console.ReadLine();
            }

            bool somethingHadBeenFound = false;
            DailyReport searchResults = new DailyReport();
            for (int i = 0; i < data.operationsAmount(); ++i)
                if (choice == 1 && data[i].contains(doubleKey) ||
                    choice != 1 && data[i].contains(stringKey))
                {
                    somethingHadBeenFound = true;
                    searchResults.AddOperation(data[i]);
                }

            if (!somethingHadBeenFound)
                Console.WriteLine("Nothing had been found!");
            else
            {
                DailyReport temp = data;
                data = searchResults;
                changesHandler.Call();
                this.output();
                data = temp;
                changesHandler.Call();
            }
        }
        void            demonstrate()
        {
            /**
             * 6.	перевантаження операторів (застосування);
            */
            Console.WriteLine("Demonstrate operators:\n");
            BuyingOperation op1 = new BuyingOperation(new Date(1999,12,12), new Buyer(1), new List<Product>(3));
            BuyingOperation op2 = new BuyingOperation(new Date(2005,10,12), new Buyer(4), new List<Product>(1));
            BuyingOperation op3 = new BuyingOperation(new Date(2010,01,01), new Buyer(7), new List<Product>(1));
            Console.Write("op1 = ");
            ((IDisplayable)op1).show();
            Console.WriteLine();
            Console.Write("op2 = ");
            ((IDisplayable)op2).show();
            Console.WriteLine();
            Console.Write("op3 = ");
            ((IDisplayable)op3).show();
            Console.WriteLine();

            Console.WriteLine("\nop1+op2+op3 gives us a daily report:\n");
            DailyReport rep = op1+op2+op3;
            ((IDisplayable)rep).show();
            Console.WriteLine("\n\n");
            
            /**
             * 10.	індексатори (застосування)
            */
            Console.WriteLine("Demonstrate indexators:\n");
            Console.Write("rep[1] = ");
            try
            {
                ((IDisplayable)rep[1]).show();
            }
            catch(ArgumentOutOfRangeException)
            {
                Console.WriteLine("Index out of bounds!");
            }
            Console.WriteLine("\n\n");
        }
        void            exit() {}
    }

    class Program
    {
        public const string SPLIT_SYMBOLS = "╬╗╝╛┐└╚";
        public const int WIN_WIDTH = 200, WIN_HEIGHT = 60;

        public static int safeInputInt32(int min = int.MinValue, int max = int.MaxValue)
        {
            int result = 0;

            for (bool wrong = true; wrong; )
            {
                /**
                 * 11.	виключні ситуації
                */
                try
                {
                    wrong = false;
                    result = Convert.ToInt32(Console.ReadLine());
                    if (result>max || result<min)
                        throw new Exception("Int32 value out of bounds!");
                }
                catch(Exception e)
                {
                    wrong = true;
                    Console.WriteLine(e.Message);
                }
            }

            return result;
        }
        public static double safeInputDouble(double min = double.MinValue, double max = double.MaxValue)
        {
            double result = 0;

            for (bool wrong = true; wrong; )
            {
                try
                {
                    wrong = false;
                    result = Convert.ToDouble(Console.ReadLine());
                    if (result>max || result<min)
                        throw new Exception("Double value out of bounds!");
                }
                catch(Exception e)
                {
                    wrong = true;
                    Console.WriteLine(e.Message);
                }
            }

            return result;
        }

        static void Main(string[] args)
        {
            Console.WindowWidth = WIN_WIDTH;
            Console.WindowHeight = WIN_HEIGHT;
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Menu mainMenu = new Menu();
            mainMenu.engage();
        }
    }
}
