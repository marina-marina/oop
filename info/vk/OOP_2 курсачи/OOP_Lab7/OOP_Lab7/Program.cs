﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace OOP_Lab6
{
    interface IDisplayable
    {
        void Display();
    }

    [Serializable()]class Date : IDisplayable
    {
        const int
            DEFAULT_YEAR = 0,
            DEFAULT_MONTH = 1,
            DEFAULT_DAY = 1,
            MIN_MONTH = 1,
            MAX_MONTH = 12,
            MIN_DAY = 1;
        int month, day;
        public int Year { get; set; }
        public int Month
        {
            get{ return month; }
            set{ month = (value<MIN_MONTH || value>MAX_MONTH ? DEFAULT_MONTH : value); }
        }
        public int Day
        {
            get{ return day; }
            set{ day = (value<MIN_DAY || value>DaysInMonth(Year, Month) ? DEFAULT_DAY : value); }
        }

        public Date(int y = DEFAULT_YEAR, int m = DEFAULT_MONTH, int d = DEFAULT_DAY)
        {
            Year = y;
            Month = m;
            Day = d;
        }

        public static int DaysInMonth(int year, int month)
        {
            int[] days = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            if (year%400==0 || (year%4==0 && year%100!=0)) //leap year
                days[1] = 29;
            return days[month-1];
        }

        public void Display()
        {
            Console.Write(Month+"/"+Day+"/"+Year);
        }
    }
    [Serializable()]class Person : IDisplayable
    {
        string name, surname;
        Date birthDate;
        long telephone;
        public string Name {get{return name;}}
        public string Surname {get{return surname;}}
        public Date BirthDate {get{return birthDate;}}
        public long Telephone {get{return telephone;}}

        public Person() : this("N/A", "N/A", new Date(), 1000000) {}
        public Person(Person p)
        {
            this.name = p.Name;
            this.surname = p.Surname;
            this.birthDate = p.BirthDate;
            this.telephone = p.Telephone;
        }
        public Person(string n, string sur, Date birth, long phone)
        {
            this.name = n;
            this.surname = sur;
            this.birthDate = birth;
            this.telephone = phone;
        }

        public void Display()
        {
            Console.Write("{0},{1} (",surname,name);
            birthDate.Display();
            Console.Write(") - {0}",telephone);
        }
    }
    [Serializable()]class Firm : IDisplayable
    {
        string name;
        double fond;
        Person contactPerson;

        public Firm() : this("N/A", 0.0, new Person()) {}
        public Firm(string n, double f, Person cp)
        {
            this.name = n;
            this.fond = f;
            this.contactPerson = cp;
        }

        public void Display()
        {
            Console.Write("{0}, fond of ${1}, contact person - ",name,fond);
            contactPerson.Display();
        }

        public bool contains(string key)
        {
            return name == key;
        }

        public string toString()
        {
            string result = "";

            result += name+String.Format("{0}", fond);

            return result;
        }
    }
    [Serializable()]enum Position { manager=1, marketer, headOfSector, branchManager };
    [Serializable()]class Employee : Person, IDisplayable
    {
        Position position;
        
        public Employee() : this(new Person(), Position.marketer) {}
        public Employee(Person p, Position pos) : base(p)
        {
            this.position = pos;
        }

        public void Display()
        {
            base.Display();
            Console.Write(", ");
            switch (position)
            {
                case Position.manager: Console.Write("manager"); break;
                case Position.marketer: Console.Write("marketer"); break;
                case Position.headOfSector: Console.Write("head of sector"); break;
                case Position.branchManager: Console.Write("branch manager"); break;
                default: Console.Write("UNKNOWN POSITION"); break;
            }
        }

        public bool contains(string key)
        {
            return base.Name==key || base.Surname==key;
        }

        public string toString()
        {
            string result = "";

            switch(position)
            {
                case Position.manager: result += "manager"; break;
                case Position.marketer: result += "marketer"; break;
                case Position.headOfSector: result += "head of sector"; break;
                case Position.branchManager: result += "branch manager"; break;
                default: break;
            }

            return result;
        }
    }
    [Serializable()]class Contract : IDisplayable
    {
        Firm firm;
        Date date;
        double price;
        Employee representative;

        public Contract() : this(new Firm(), new Date(), 0.0, new Employee()) {}
        public Contract(Firm f, Date d, double p, Employee e)
        {
            this.firm = f;
            this.date = d;
            this.price = p;
            this.representative = e;
        }

        public void Display()
        {
            firm.Display();
            Console.Write(" made contract at ");
            date.Display();
            Console.Write(", total price = {0}, representative - ",price);
            representative.Display();
        }

        public bool contains(string key)
        {
            return firm.contains(key) || representative.contains(key);
        }

        public string toString()
        {
            string result = "";

            result += firm.toString()+" "+String.Format("{0}", price);

            return result;
        }
    }
    [Serializable()]class EmployeeActivity : IDisplayable
    {
        Employee employee;
        List<Contract> contracts;

        public EmployeeActivity() : this(new Employee(), new List<Contract>()) {}
        public EmployeeActivity(Employee emp, List<Contract> li)
        {
            this.employee = emp;
            this.contracts = li;
        }

        public void Display()
        {
            employee.Display();
            Console.WriteLine(":");
            for (int i = 0; i < contracts.Count; ++i)
            {
                Console.Write("\t-");
                contracts[i].Display();
                Console.WriteLine();
            }
        }
    }
    [Serializable()]class ResearchUnit : IDisplayable, IComparable
    {
        List<Employee> staff;
        List<Contract> contracts;
        public double totalPrice;

        public ResearchUnit()
        {
            List<string> namesBase = new List<string>();
            namesBase.Add("Bill");
            namesBase.Add("Will");
            namesBase.Add("James");
            namesBase.Add("Sam");
            namesBase.Add("John");
            namesBase.Add("Daren");
            namesBase.Add("Philip");
            namesBase.Add("Sean");
            namesBase.Add("Jack");
            namesBase.Add("Jill");
            namesBase.Add("Ann");
            namesBase.Add("Julia");
            namesBase.Add("Rachell");
            namesBase.Add("Callum");
            namesBase.Add("Simon");
            namesBase.Add("Josh");
            namesBase.Add("Vikk");
            namesBase.Add("Harry");
            namesBase.Add("Dan");
            namesBase.Add("Tiffany");
            namesBase.Add("Walter");
            namesBase.Add("Richard");
            namesBase.Add("Lester");
            namesBase.Add("Michael");
            namesBase.Add("Andrew");

            List<string> surnamesBase = new List<string>();
            surnamesBase.Add("Smith");
            surnamesBase.Add("Turner");
            surnamesBase.Add("Williams");
            surnamesBase.Add("Connery");
            surnamesBase.Add("Jones");
            surnamesBase.Add("Roberts");
            surnamesBase.Add("Gates");
            surnamesBase.Add("Conrad");
            surnamesBase.Add("Simons");
            surnamesBase.Add("O'Neil");
            surnamesBase.Add("Oven");
            surnamesBase.Add("Tako");
            surnamesBase.Add("Lee");
            surnamesBase.Add("Swan");
            surnamesBase.Add("Rachkowsky");
            surnamesBase.Add("Hope");
            surnamesBase.Add("Phelps");
            surnamesBase.Add("Walden");
            surnamesBase.Add("Robertson");
            surnamesBase.Add("Cole");

            List<string> titlesBase = new List<string>();
            titlesBase.Add("Oak Inc.");
            titlesBase.Add("Sunrise");
            titlesBase.Add("M&Q");
            titlesBase.Add("Simon Industries");
            titlesBase.Add("Pawn");
            titlesBase.Add("ROG");
            titlesBase.Add("ASUS");
            titlesBase.Add("Maestro");

            Random rnd = new Random();

            totalPrice = 0.0;

            staff = new List<Employee>();
            int staffNum = rnd.Next(1, 5+1), bufY, bufM;
            for (int i = 0; i < staffNum; ++i)
            {
                staff.Add
                (
                    new Employee
                    (
                        new Person
                        (
                            namesBase[rnd.Next(0, namesBase.Count)],
                            surnamesBase[rnd.Next(0, surnamesBase.Count)],
                            new Date
                            (
                                bufY = rnd.Next(1940, 2000+1),
                                bufM = rnd.Next(1, 12+1),
                                rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                            ),
                            rnd.Next(1111111,9999999)
                        ),
                        (Position)(rnd.Next(1,5))
                    )
                );
                Thread.Sleep(11);
            }

            contracts = new List<Contract>();
            int contractsNum = rnd.Next(1, 5+1);
            double bufP;
            for (int i = 0; i < contractsNum; ++i)
            {
                contracts.Add
                (
                    new Contract
                    (
                        new Firm
                        (
                            titlesBase[rnd.Next(0, titlesBase.Count)],
                            rnd.Next(0, 1000+1) * rnd.NextDouble(),
                            new Person
                            (
                                namesBase[rnd.Next(0, namesBase.Count)],
                                surnamesBase[rnd.Next(0, surnamesBase.Count)],
                                new Date
                                (
                                    bufY = rnd.Next(1940, 2000+1),
                                    bufM = rnd.Next(1, 12+1),
                                    rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                                ),
                                rnd.Next(1111111,9999999)
                            )
                        ),
                        new Date
                        (
                            bufY = rnd.Next(1940, 2000+1),
                            bufM = rnd.Next(1, 12+1),
                            rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                        ),
                        bufP = rnd.Next(0, 1000+1) * rnd.NextDouble(),
                        new Employee
                        (
                            new Person
                            (
                                namesBase[rnd.Next(0, namesBase.Count)],
                                surnamesBase[rnd.Next(0, surnamesBase.Count)],
                                new Date
                                (
                                    bufY = rnd.Next(1940, 2000+1),
                                    bufM = rnd.Next(1, 12+1),
                                    rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                                ),
                                rnd.Next(1111111,9999999)
                            ),
                            (Position)(rnd.Next(1,5))
                        )
                    )
                );

                totalPrice += bufP;

                Thread.Sleep(7);
            }
        }
        public ResearchUnit(List<Employee> st, List<Contract> con, double tP)
        {
            this.staff = st;
            this.contracts = con;
            this.totalPrice = tP;
        }

        public void Display()
        {
            Console.WriteLine("Staff:");
            for (int i = 0; i < staff.Count; ++i)
            {
                Console.Write("\t-");
                staff[i].Display();
                Console.WriteLine();
            }
            Console.WriteLine("Contracts:");
            for (int i = 0; i < contracts.Count; ++i)
            {
                Console.Write("\t-");
                contracts[i].Display();
                Console.WriteLine();
            }
            Console.WriteLine("Total Price: ${0}", totalPrice);
        }

        public bool contains(string key)
        {
            bool result = false;
            for (int i = 0; i < staff.Count; ++i)
                if (staff[i].contains(key))
                    result = true;
            
            for (int i = 0; i < contracts.Count; ++i)
                if (contracts[i].contains(key))
                    result = true;

            return result;
        }
        public int CompareTo(Object cmp)
        {
            ResearchUnit buf = (ResearchUnit) cmp;
            if (this.totalPrice > buf.totalPrice)
                return 1;
            if (this.totalPrice < buf.totalPrice)
                return -1;
            return 0;
        }

        public string toString()
        {
            string result = "";

            for (int i = 0; i < staff.Count(); ++i)
                result += staff[i].toString()+"\r\n";
            result += "\r\n";

            for (int i = 0; i < contracts.Count(); ++i)
                result += contracts[i].toString()+"\r\n";
            result += "\r\n";

            result += "total price = "+String.Format("{0}", this.totalPrice);

            return result;
        }
    }

    class Observable : IObservable<ResearchUnit[]>
    {
        private class Unsubscriber : IDisposable
        {
            OnNextDelegate oRefresh;
            OnCompletedDelegate oRelease;
            IObserver<ResearchUnit[]> observer;

            public Unsubscriber(OnNextDelegate ond, OnCompletedDelegate or, IObserver<ResearchUnit[]> ob)
            {
                oRefresh = ond;
                oRelease = or;
                observer = ob;
            }

            public void Dispose()
            {
                oRelease -= observer.OnCompleted;
            }
        }

        delegate void OnNextDelegate(ResearchUnit[] a);
        delegate void OnCompletedDelegate();
        OnNextDelegate          observersRefresh;
        OnCompletedDelegate     observersRelease;   

        public IDisposable      Subscribe(IObserver<ResearchUnit[]> observer)
        {
            observersRefresh += observer.OnNext;
            observersRelease += observer.OnCompleted;

            return new Unsubscriber(observersRefresh, observersRelease, observer);
        }
        public void             Call(ResearchUnit[] a)
        {
            observersRefresh(a);
        }
        public void             Finish()
        {
            observersRelease();
        }
    }
    class Observer : IObserver<ResearchUnit[]>
    {
        public double value;

        public delegate double StatisticsDelegate(ResearchUnit[] a);
        StatisticsDelegate statisticsFunction;

        private IDisposable     unsubscriber;

        public                  Observer(IObservable<ResearchUnit[]> provider, StatisticsDelegate sFunc)
        {
            statisticsFunction = sFunc;
            this.Subscribe(provider);
        }

        public void             Subscribe(IObservable<ResearchUnit[]> provider)
        {
            unsubscriber = provider.Subscribe(this);
        }
        public void             Unsubscribe()
        {
            unsubscriber.Dispose();
        }

        public void             OnNext(ResearchUnit[] a)
        {
            value = statisticsFunction(a);
        }
        public void             OnError(Exception e){}
        public void             OnCompleted()
        {
            // Nothing to do
        }
    }

    class Program
    {
        static double totalPrice(ResearchUnit[] a)
        {
            double result = 0.0;
            foreach (ResearchUnit x in a)
                result += x.totalPrice;
            return result;
        }
        static double averagePrice(ResearchUnit[] a)
        {
            double result = 0.0;
            foreach (ResearchUnit x in a)
                result += x.totalPrice;
            return result/a.Length;
        }

        static void writeToBinaryFile(ResearchUnit[] a, string fileName)
        {
            Stream stream = File.Open(fileName, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, a);
            stream.Close();
        }
        static void writeToTextFile(ResearchUnit[] a, string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            StreamWriter writer = file.CreateText();
            writer.Write(toString(a));
            writer.Close();
        }
 
        static string toString(ResearchUnit[] a)
        {
            string result = "";

            for (int i = 0; i < a.Count(); ++i)
                result += a[i].toString()+"\r\n-------------------------------------------\r\n\r\n";

            return result;
        }

        static void Main(string[] args)
        {
            //Console.WindowHeight = 80;
            //Console.WindowWidth = 230;
    
            Observable changesHandler = new Observable();
            List<Observer> statisticians = new List<Observer>();
            statisticians.Add(new Observer(changesHandler, totalPrice));
            statisticians.Add(new Observer(changesHandler, averagePrice));

            const int staticArraySize = 2;
            ResearchUnit[] staticArray = new ResearchUnit[staticArraySize];
            for (int i = 0; i < staticArraySize; ++i)
                staticArray[i] = new ResearchUnit();

            changesHandler.Call(staticArray);
            Console.WriteLine("total = {0:0.00}; average = {1:0.00}", statisticians[0].value, statisticians[1].value);
            Console.WriteLine("Adding elements...");
            addToStatic(ref staticArray, new ResearchUnit());
            changesHandler.Call(staticArray);
            Console.WriteLine("total = {0:0.00}; average = {1:0.00}", statisticians[0].value, statisticians[1].value);
            
            writeToBinaryFile(staticArray, "D:\\RESULT\\binary.bin");
            writeToTextFile(staticArray, "D:\\RESULT\\text.txt");

            Console.ReadKey();
        }

        static void show(ResearchUnit[] a)
        {
            for (int i = 0; i < a.Length; ++i)
            {
                Console.WriteLine("-------------------------------------- {0} research unit [static array]--------------------------------------", i+1);
                a[i].Display();
            }
        }
        static void show(ArrayList a)
        {
            for (int i = 0; i < a.Count; ++i)
            {
                Console.WriteLine("-------------------------------------- {0} research unit [dynamic array]--------------------------------------", i+1);
                ((ResearchUnit)a[i]).Display();
            }
        }

        static void insertElementFromStaticToDynamic(ResearchUnit[] staticArray, int index, ref ArrayList dynamicArray, int position)
        {
            dynamicArray.Insert(position, staticArray[index]);
        }

        static ResearchUnit findInStatic(ResearchUnit[] staticArray, string key)
        {
            for (int i = 0; i < staticArray.Length; ++i)
            {
                if (staticArray[i].contains(key))
                    return staticArray[i];
            }

            return null;
        }
        static ResearchUnit findInDynamic(ArrayList dynamicArray, string key)
        {
            for (int i = 0; i < dynamicArray.Count; ++i)
            {
                if (((ResearchUnit)dynamicArray[i]).contains(key))
                    return ((ResearchUnit)dynamicArray[i]);
            }

            return null;
        }

        static void removeFromStatic(ref ResearchUnit[] staticArray, string key)
        {
            int index = -1;
            for (int i = 0; i < staticArray.Length; ++i)
                if (staticArray[i].contains(key))
                    index = i;
            
            ResearchUnit[] newArray = new ResearchUnit[staticArray.Length-(index==-1?0:1)];
            for (int i = 0; i < staticArray.Length; ++i)
            {
                newArray[i-(i>=index?1:0)] = staticArray[i];
            }
            staticArray = newArray;
        }
        static void removeFromDynamic(ref ArrayList dynamicArray, string key)
        {
            for (int i = 0; i < dynamicArray.Count; ++i)
            {
                if (((ResearchUnit)dynamicArray[i]).contains(key))
                    dynamicArray.RemoveAt(i);
            }
        }

        static void sortStatic(ref ResearchUnit[] staticArray)
        {
            Array.Sort(staticArray);
        }
        static void sortDynamic(ref ArrayList dynamicArray)
        {
            dynamicArray.Sort();
        }

        static void addToStatic(ref ResearchUnit[] staticArray, ResearchUnit elem)
        {
            ResearchUnit[] newArray = new ResearchUnit[staticArray.Length+1];

            for (int i = 0; i < staticArray.Length; ++i)
                newArray[i] = staticArray[i];
            newArray[staticArray.Length] = elem;

            staticArray = newArray;
        }
    }
}
