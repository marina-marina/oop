﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Lab3
{
    class Program
    {
        public struct Date
        {
            public byte day;
            public byte month;
            public int year;
            
            public Date(byte d, byte m, int y)
            {
                day = d>0&&d<31 ? d : (byte)(0);
                month = m>0&&m<12 ? m : (byte)(0);
                year = y<-1000&&y<3000 ? y : 0;
            }
        }

        public class File
        {
            string name;
            string extension;
            string Path;
            Date creationDate;
            uint size = 0;

            
            const uint maxSize = 100000;
            public string   Extension   { get { return extension; } }
            public string   Name        { set { name = value; } }
            public uint     Size        { get { return size; } set{ size = value<maxSize? value : maxSize; } }



            public File() : this("")
            {
                //Console.WriteLine("1st ctor called");
            }
            public File(string n) : this(n, "", "", new Date(1,1,1), 1)
            {
                //Console.WriteLine("2nd ctor called");
            }
            public File(string n, string ext, string pth, Date date, uint sz)
            {
                name = n;
                extension = ext;
                Path = pth;
                creationDate = date;
                size = sz;
                
                //Console.WriteLine("3rd ctor called");
            }

            public void show() { Console.Write("\""+Path+name+"."+extension+"\" ["+size+"b]"); }

            public static File operator+(File a, File b)
            {
                File result = new File();
                result.Size = a.Size + b.Size;
                return result;
            }
            public static bool operator==(File a, File b) { return a.Size == b.Size; }
            public static bool operator!=(File a, File b) { return !(a==b); }
            public static bool operator!(File a) { return a.Size == 0; }
            public static explicit operator File(string s) { return new File(s); }
        }

        public class AttribFile : File
        {
            public string Attributes { get; set; }

            static ulong totalMem;

            private static void refreshTotalMem(uint s) { totalMem += s; }
            public void showTotalMem() { Console.Write(totalMem); }

            public AttribFile()
            {        
                Attributes = "";
                refreshTotalMem(this.Size);
                //Console.WriteLine("Derived ctor called");
            }
        }
       
        public class Catalog
        {
            AttribFile[] a;
            uint sz;

            public Catalog() { sz = 0; }
            public Catalog(uint n) { sz = n; a = new AttribFile[n]; } 

            public AttribFile this[int i]
            {
                get { return a[i]; }
                set { a[i] = value; }
            }
        }

        static void Main(string[] args)
        {
            File test = new File("index","html","C:\\",new Date(1,12,2015),1024); 
            test.show();
            Console.WriteLine();
            test.Name = "page1";
            test.show();
            Console.WriteLine();

            Catalog testCatalog = new Catalog(10);
            testCatalog[4] = new AttribFile();
            testCatalog[4].show();

            Console.ReadKey();
        }
    }
}
