﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Lab2
{
    class Program
    {
        public struct Date
        {
            public byte day;
            public byte month;
            public int year;
            
            public Date(byte d, byte m, int y)
            {
                day = d>0&&d<31 ? d : (byte)(0);
                month = m>0&&m<12 ? m : (byte)(0);
                year = y<-1000&&y<3000 ? y : 0;
            }
        }

        public class File
        {
            string name;
            string extension;
            string attributes;
            string path;
            Date creationDate;
            uint size;
            static ulong totalMem;

            public File() : this("")
            {
                Console.WriteLine("1st ctor called");
            }
            public File(string n) : this(n, "", "", "", new Date(1,1,1), 0)
            {
                Console.WriteLine("2nd ctor called");
            }
            public File(string n, string ext, string attr, string pth, Date date, uint sz)
            {
                name = n;
                extension = ext;
                attributes = attr;
                path = pth;
                creationDate = date;
                size = sz;
                totalMem += size;
                
                Console.WriteLine("3rd ctor called");
            }
        }

        public class ConfigureFile : File
        {
            string destPath;

            public ConfigureFile()
            {        
                destPath = "";
                Console.WriteLine("Derived ctor called");
            }
        }

        static void Main(string[] args)
        {
            ConfigureFile test = new ConfigureFile();
            Console.ReadKey();
        }
    }
}
