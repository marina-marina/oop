﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace OOP_Lab6
{
    interface IDisplayable
    {
        void Display();
    }

    class Date : IDisplayable
    {
        const int
            DEFAULT_YEAR = 0,
            DEFAULT_MONTH = 1,
            DEFAULT_DAY = 1,
            MIN_MONTH = 1,
            MAX_MONTH = 12,
            MIN_DAY = 1;
        int month, day;
        public int Year { get; set; }
        public int Month
        {
            get{ return month; }
            set{ month = (value<MIN_MONTH || value>MAX_MONTH ? DEFAULT_MONTH : value); }
        }
        public int Day
        {
            get{ return day; }
            set{ day = (value<MIN_DAY || value>DaysInMonth(Year, Month) ? DEFAULT_DAY : value); }
        }

        public Date(int y = DEFAULT_YEAR, int m = DEFAULT_MONTH, int d = DEFAULT_DAY)
        {
            Year = y;
            Month = m;
            Day = d;
        }

        public static int DaysInMonth(int year, int month)
        {
            int[] days = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            if (year%400==0 || (year%4==0 && year%100!=0)) //leap year
                days[1] = 29;
            return days[month-1];
        }

        public void Display()
        {
            Console.Write(Month+"/"+Day+"/"+Year);
        }
    }
    class Person : IDisplayable
    {
        string name, surname;
        Date birthDate;
        long telephone;
        public string Name {get{return name;}}
        public string Surname {get{return surname;}}
        public Date BirthDate {get{return birthDate;}}
        public long Telephone {get{return telephone;}}

        public Person() : this("N/A", "N/A", new Date(), 1000000) {}
        public Person(Person p)
        {
            this.name = p.Name;
            this.surname = p.Surname;
            this.birthDate = p.BirthDate;
            this.telephone = p.Telephone;
        }
        public Person(string n, string sur, Date birth, long phone)
        {
            this.name = n;
            this.surname = sur;
            this.birthDate = birth;
            this.telephone = phone;
        }

        public void Display()
        {
            Console.Write("{0},{1} (",surname,name);
            birthDate.Display();
            Console.Write(") - {0}",telephone);
        }
    }
    class Firm : IDisplayable
    {
        string name;
        double fond;
        Person contactPerson;

        public Firm() : this("N/A", 0.0, new Person()) {}
        public Firm(string n, double f, Person cp)
        {
            this.name = n;
            this.fond = f;
            this.contactPerson = cp;
        }

        public void Display()
        {
            Console.Write("{0}, fond of ${1}, contact person - ",name,fond);
            contactPerson.Display();
        }

        public bool contains(string key)
        {
            return name == key;
        }
    }
    enum Position { manager=1, marketer, headOfSector, branchManager };
    class Employee : Person, IDisplayable
    {
        Position position;
        
        public Employee() : this(new Person(), Position.marketer) {}
        public Employee(Person p, Position pos) : base(p)
        {
            this.position = pos;
        }

        public void Display()
        {
            base.Display();
            Console.Write(", ");
            switch (position)
            {
                case Position.manager: Console.Write("manager"); break;
                case Position.marketer: Console.Write("marketer"); break;
                case Position.headOfSector: Console.Write("head of sector"); break;
                case Position.branchManager: Console.Write("branch manager"); break;
                default: Console.Write("UNKNOWN POSITION"); break;
            }
        }

        public bool contains(string key)
        {
            return base.Name==key || base.Surname==key;
        }
    }
    class Contract : IDisplayable
    {
        Firm firm;
        Date date;
        double price;
        Employee representative;

        public Contract() : this(new Firm(), new Date(), 0.0, new Employee()) {}
        public Contract(Firm f, Date d, double p, Employee e)
        {
            this.firm = f;
            this.date = d;
            this.price = p;
            this.representative = e;
        }

        public void Display()
        {
            firm.Display();
            Console.Write(" made contract at ");
            date.Display();
            Console.Write(", total price = {0}, representative - ",price);
            representative.Display();
        }

        public bool contains(string key)
        {
            return firm.contains(key) || representative.contains(key);
        }
    }
    class EmployeeActivity : IDisplayable
    {
        Employee employee;
        List<Contract> contracts;

        public EmployeeActivity() : this(new Employee(), new List<Contract>()) {}
        public EmployeeActivity(Employee emp, List<Contract> li)
        {
            this.employee = emp;
            this.contracts = li;
        }

        public void Display()
        {
            employee.Display();
            Console.WriteLine(":");
            for (int i = 0; i < contracts.Count; ++i)
            {
                Console.Write("\t-");
                contracts[i].Display();
                Console.WriteLine();
            }
        }
    }
    class ResearchUnit : IDisplayable, IComparable
    {
        List<Employee> staff;
        List<Contract> contracts;
        double totalPrice;

        public ResearchUnit()
        {
            List<string> namesBase = new List<string>();
            namesBase.Add("Bill");
            namesBase.Add("Will");
            namesBase.Add("James");
            namesBase.Add("Sam");
            namesBase.Add("John");
            namesBase.Add("Daren");
            namesBase.Add("Philip");
            namesBase.Add("Sean");
            namesBase.Add("Jack");
            namesBase.Add("Jill");
            namesBase.Add("Ann");
            namesBase.Add("Julia");
            namesBase.Add("Rachell");
            namesBase.Add("Callum");
            namesBase.Add("Simon");
            namesBase.Add("Josh");
            namesBase.Add("Vikk");
            namesBase.Add("Harry");
            namesBase.Add("Dan");
            namesBase.Add("Tiffany");
            namesBase.Add("Walter");
            namesBase.Add("Richard");
            namesBase.Add("Lester");
            namesBase.Add("Michael");
            namesBase.Add("Andrew");

            List<string> surnamesBase = new List<string>();
            surnamesBase.Add("Smith");
            surnamesBase.Add("Turner");
            surnamesBase.Add("Williams");
            surnamesBase.Add("Connery");
            surnamesBase.Add("Jones");
            surnamesBase.Add("Roberts");
            surnamesBase.Add("Gates");
            surnamesBase.Add("Conrad");
            surnamesBase.Add("Simons");
            surnamesBase.Add("O'Neil");
            surnamesBase.Add("Oven");
            surnamesBase.Add("Tako");
            surnamesBase.Add("Lee");
            surnamesBase.Add("Swan");
            surnamesBase.Add("Rachkowsky");
            surnamesBase.Add("Hope");
            surnamesBase.Add("Phelps");
            surnamesBase.Add("Walden");
            surnamesBase.Add("Robertson");
            surnamesBase.Add("Cole");

            List<string> titlesBase = new List<string>();
            titlesBase.Add("Oak Inc.");
            titlesBase.Add("Sunrise");
            titlesBase.Add("M&Q");
            titlesBase.Add("Simon Industries");
            titlesBase.Add("Pawn");
            titlesBase.Add("ROG");
            titlesBase.Add("ASUS");
            titlesBase.Add("Maestro");

            Random rnd = new Random();

            totalPrice = 0.0;

            staff = new List<Employee>();
            int staffNum = rnd.Next(1, 5+1), bufY, bufM;
            for (int i = 0; i < staffNum; ++i)
            {
                staff.Add
                (
                    new Employee
                    (
                        new Person
                        (
                            namesBase[rnd.Next(0, namesBase.Count)],
                            surnamesBase[rnd.Next(0, surnamesBase.Count)],
                            new Date
                            (
                                bufY = rnd.Next(1940, 2000+1),
                                bufM = rnd.Next(1, 12+1),
                                rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                            ),
                            rnd.Next(1111111,9999999)
                        ),
                        (Position)(rnd.Next(1,5))
                    )
                );
                Thread.Sleep(11);
            }

            contracts = new List<Contract>();
            int contractsNum = rnd.Next(1, 5+1);
            double bufP;
            for (int i = 0; i < contractsNum; ++i)
            {
                contracts.Add
                (
                    new Contract
                    (
                        new Firm
                        (
                            titlesBase[rnd.Next(0, titlesBase.Count)],
                            rnd.Next(0, 1000+1) * rnd.NextDouble(),
                            new Person
                            (
                                namesBase[rnd.Next(0, namesBase.Count)],
                                surnamesBase[rnd.Next(0, surnamesBase.Count)],
                                new Date
                                (
                                    bufY = rnd.Next(1940, 2000+1),
                                    bufM = rnd.Next(1, 12+1),
                                    rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                                ),
                                rnd.Next(1111111,9999999)
                            )
                        ),
                        new Date
                        (
                            bufY = rnd.Next(1940, 2000+1),
                            bufM = rnd.Next(1, 12+1),
                            rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                        ),
                        bufP = rnd.Next(0, 1000+1) * rnd.NextDouble(),
                        new Employee
                        (
                            new Person
                            (
                                namesBase[rnd.Next(0, namesBase.Count)],
                                surnamesBase[rnd.Next(0, surnamesBase.Count)],
                                new Date
                                (
                                    bufY = rnd.Next(1940, 2000+1),
                                    bufM = rnd.Next(1, 12+1),
                                    rnd.Next(1, Date.DaysInMonth(bufY, bufM))
                                ),
                                rnd.Next(1111111,9999999)
                            ),
                            (Position)(rnd.Next(1,5))
                        )
                    )
                );

                totalPrice += bufP;

                Thread.Sleep(7);
            }
        }
        public ResearchUnit(List<Employee> st, List<Contract> con, double tP)
        {
            this.staff = st;
            this.contracts = con;
            this.totalPrice = tP;
        }

        public void Display()
        {
            Console.WriteLine("Staff:");
            for (int i = 0; i < staff.Count; ++i)
            {
                Console.Write("\t-");
                staff[i].Display();
                Console.WriteLine();
            }
            Console.WriteLine("Contracts:");
            for (int i = 0; i < contracts.Count; ++i)
            {
                Console.Write("\t-");
                contracts[i].Display();
                Console.WriteLine();
            }
            Console.WriteLine("Total Price: ${0}", totalPrice);
        }

        public bool contains(string key)
        {
            bool result = false;
            for (int i = 0; i < staff.Count; ++i)
                if (staff[i].contains(key))
                    result = true;
            
            for (int i = 0; i < contracts.Count; ++i)
                if (contracts[i].contains(key))
                    result = true;

            return result;
        }
        public int CompareTo(Object cmp)
        {
            ResearchUnit buf = (ResearchUnit) cmp;
            if (this.totalPrice > buf.totalPrice)
                return 1;
            if (this.totalPrice < buf.totalPrice)
                return -1;
            return 0;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowHeight = 80;
            Console.WindowWidth = 230;
    
            const int staticArraySize = 10;
            ResearchUnit[] staticArray = new ResearchUnit[staticArraySize];
            for (int i = 0; i < staticArraySize; ++i)
                staticArray[i] = new ResearchUnit();
       

            Console.Write("Input size of the dynamic array: ");
            int dynamicArraySize = Convert.ToInt32(Console.ReadLine());
            ArrayList dynamicArray = new ArrayList(dynamicArraySize);
            for (int i = 0; i < dynamicArraySize; ++i)
                dynamicArray.Add(new ResearchUnit());

            Console.WriteLine("Created arrays");
            show(staticArray);
            show(dynamicArray);
            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("Insert third element of static array before second of dynamic:");
            insertElementFromStaticToDynamic(staticArray, 2, ref dynamicArray, 1);
            show(dynamicArray);
            Console.ReadKey();
            Console.Clear();

            ResearchUnit searchResult;
            searchResult = findInStatic(staticArray, "Bill");
            Console.WriteLine("Finded 'Bill' in static array: ");
            if (searchResult != null)
                searchResult.Display();
            else
                Console.WriteLine("Nothing was found");
            searchResult = findInDynamic(dynamicArray, "Bill");
            Console.WriteLine("Finded 'Bill' in dynamic array: ");
            if (searchResult != null)
                searchResult.Display();
            else
                Console.WriteLine("Nothing was found");
            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("Delete 'Bill' from arrays");
            removeFromStatic(ref staticArray, "Bill");
            removeFromDynamic(ref dynamicArray, "Bill");
            show(staticArray);
            show(dynamicArray);
            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("Sort arrays");
            sortStatic(ref staticArray);
            sortDynamic(ref dynamicArray);
            show(staticArray);
            show(dynamicArray);
            Console.ReadKey();
        }

        static void show(ResearchUnit[] a)
        {
            for (int i = 0; i < a.Length; ++i)
            {
                Console.WriteLine("-------------------------------------- {0} research unit [static array]--------------------------------------", i+1);
                a[i].Display();
            }
        }
        static void show(ArrayList a)
        {
            for (int i = 0; i < a.Count; ++i)
            {
                Console.WriteLine("-------------------------------------- {0} research unit [dynamic array]--------------------------------------", i+1);
                ((ResearchUnit)a[i]).Display();
            }
        }

        static void insertElementFromStaticToDynamic(ResearchUnit[] staticArray, int index, ref ArrayList dynamicArray, int position)
        {
            dynamicArray.Insert(position, staticArray[index]);
        }

        static ResearchUnit findInStatic(ResearchUnit[] staticArray, string key)
        {
            for (int i = 0; i < staticArray.Length; ++i)
            {
                if (staticArray[i].contains(key))
                    return staticArray[i];
            }

            return null;
        }
        static ResearchUnit findInDynamic(ArrayList dynamicArray, string key)
        {
            for (int i = 0; i < dynamicArray.Count; ++i)
            {
                if (((ResearchUnit)dynamicArray[i]).contains(key))
                    return ((ResearchUnit)dynamicArray[i]);
            }

            return null;
        }

        static void removeFromStatic(ref ResearchUnit[] staticArray, string key)
        {
            int index = -1;
            for (int i = 0; i < staticArray.Length; ++i)
                if (staticArray[i].contains(key))
                    index = i;
            
            ResearchUnit[] newArray = new ResearchUnit[staticArray.Length-(index==-1?0:1)];
            for (int i = 0; i < staticArray.Length; ++i)
            {
                newArray[i-(i>=index?1:0)] = staticArray[i];
            }
            staticArray = newArray;
        }
        static void removeFromDynamic(ref ArrayList dynamicArray, string key)
        {
            for (int i = 0; i < dynamicArray.Count; ++i)
            {
                if (((ResearchUnit)dynamicArray[i]).contains(key))
                    dynamicArray.RemoveAt(i);
            }
        }

        static void sortStatic(ref ResearchUnit[] staticArray)
        {
            Array.Sort(staticArray);
        }
        static void sortDynamic(ref ArrayList dynamicArray)
        {
            dynamicArray.Sort();
        }
    }
}
