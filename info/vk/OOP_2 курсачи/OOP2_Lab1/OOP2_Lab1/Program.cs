﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Lab1
{
    class Program
    {
        static double L = -0.2, R = 0.2, STEP = 0.01;

        static void Main(string[] args)
        {
            showTable();
            Console.WriteLine();
            showHistogram();
            Console.Read();
        }

        static double f(double x) { return (1.0+6*x+x*x)/Math.Pow(1-x, 3); }
        static double taylor(double x, int n) { return 1.0 + (n>1 ? 9*x : 0) + (n>2 ? 25*x*x : 0) + (n>3 ? 49*x*x*x : 0); }
        //static double f(double x) { return Math.Log((1+x)/(1-x)); }
        //static double taylor(double x, int n) { return 2.0*x + (n>1 ? 2.0/3.0*x*x*x : 0) + (n>2 ? 2.0/5.0*x*x*x*x*x : 0) + (n>3 ? 2.0/7.0*x*x*x*x*x*x*x : 0); }


        static double error(double a, double b) { return 100.0*Math.Abs(1-a/b); }

        static void showTable()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("╔═══════╦════════════╦═════════════════════════╦═════════════════════════╦═════════════════════════╦═════════════════════════╗");
            Console.WriteLine("║       ║            ║         1 Elem          ║         2 Elem          ║         3 Elem          ║         4 Elem          ║");
            Console.WriteLine("║   x   ║    f(x)    ╠════════════╦════════════╬════════════╦════════════╬════════════╦════════════╬════════════╦════════════╣");
            Console.WriteLine("║       ║            ║    ~f(x)   ║   err, %   ║    ~f(x)   ║   err, %   ║    ~f(x)   ║   err, %   ║    ~f(x)   ║   err, %   ║");
            for ( double x = L+STEP ; x < R-1e-6 ; x += STEP )
            {
                double fun = f(x), t1 = taylor(x, 1), t2 = taylor(x, 2), t3 = taylor(x, 3), t4 = taylor(x, 4);
                Console.WriteLine("╠═══════╬════════════╬════════════╬════════════╬════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣");
                Console.WriteLine("║ {0,5:0.0} ║ {1,10:0.000} ║ {2,10:0.000} ║ {3,10:0.000} ║ {4,10:0.000} ║ {5,10:0.000} ║ {6,10:0.000} ║ {7,10:0.000} ║ {8,10:0.000} ║ {9,10:0.000} ║",
                                      x,          fun,           t1,            error(t1, fun),t2,            error(t2, fun),t3,            error(t3, fun),t4,            error(t4, fun));
            }
            Console.WriteLine("╚═══════╩════════════╩═════════════════════════╩═════════════════════════╩═════════════════════════╩═════════════════════════╝");
        }

        static void drawLine(int length, ConsoleColor color)
        {
            Console.ForegroundColor = color; 
            for ( int i = 0 ; i < length ; ++i )
                Console.Write("█");
            Console.WriteLine();
        }

        static void showHistogram()
        {
            double average1 = 0, average2 = 0, average3 = 0, average4 = 0;
            for ( double x = L+STEP ; x < R-1e-6 ; x += STEP )
            {
                average1 += error(taylor(x, 1), f(x))/(R-L)*STEP;
                average2 += error(taylor(x, 2), f(x))/(R-L)*STEP;
                average3 += error(taylor(x, 3), f(x))/(R-L)*STEP;
                average4 += error(taylor(x, 4), f(x))/(R-L)*STEP;
                Console.Write("      ");       drawLine((int)(error(taylor(x, 1), f(x))/10.0+0.5), ConsoleColor.Yellow);
                Console.Write("      ");       drawLine((int)(error(taylor(x, 2), f(x))/10.0+0.5), ConsoleColor.DarkRed);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write("{0,5:0.0} ",x); drawLine((int)(error(taylor(x, 3), f(x))/10.0+0.5), ConsoleColor.DarkGreen);
                Console.Write("      ");       drawLine((int)(error(taylor(x, 4), f(x))/10.0+0.5), ConsoleColor.DarkBlue);
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("\nAverage errors:");
            Console.WriteLine("   1: {0,5:0.000}",average1);
            Console.WriteLine("   2: {0,5:0.000}",average2);
            Console.WriteLine("   3: {0,5:0.000}",average3);
            Console.WriteLine("   4: {0,5:0.000}",average4);
        }
    }
}
