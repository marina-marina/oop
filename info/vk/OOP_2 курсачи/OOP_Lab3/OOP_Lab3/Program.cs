﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Lab3
{
    class Program
    {
        public struct Date
        {
            public byte day;
            public byte month;
            public int year;
            
            public Date(byte d, byte m, int y)
            {
                day = d>0&&d<31 ? d : (byte)(0);
                month = m>0&&m<12 ? m : (byte)(0);
                year = y<-1000&&y<3000 ? y : 0;
            }
        }

        public class File
        {
            string name;
            string extension;
            string path;
            Date creationDate;
            protected uint size = 0;
            public void setSize(uint value) { size = value; }
            public uint getSize() { return size; }

            public File() : this("")
            {
                //Console.WriteLine("1st ctor called");
            }
            public File(string n) : this(n, "", "", new Date(1,1,1), 1)
            {
                //Console.WriteLine("2nd ctor called");
            }
            public File(string n, string ext, string pth, Date date, uint sz)
            {
                name = n;
                extension = ext;
                path = pth;
                creationDate = date;
                size = sz;
                
                //Console.WriteLine("3rd ctor called");
            }

            public void show() { Console.Write("\""+path+name+"."+extension+"\" ["+size+"]"); }

            public static File operator+(File a, File b)
            {
                File result = new File();
                result.setSize(a.getSize() + b.getSize());
                return result;
            }
            public static bool operator==(File a, File b) { return a.getSize() == b.getSize(); }
            public static bool operator!=(File a, File b) { return !(a==b); }
            public static bool operator!(File a) { return a.getSize() == 0; }
            public static explicit operator File(string s) { return new File(s); }
        }

        public class AttribFile : File
        {
            string attributes;
            static ulong totalMem;

            private static void refreshTotalMem(uint s) { totalMem += s; }
            public void showTotalMem() { Console.Write(totalMem); }

            public AttribFile()
            {        
                attributes = "";
                refreshTotalMem(size);
                //Console.WriteLine("Derived ctor called");
            }
        }

        static void Main(string[] args)
        {
            AttribFile test1 = new AttribFile();
            Console.Write("Total memory after creating first file: "); test1.showTotalMem(); Console.WriteLine();
            AttribFile test2 = new AttribFile();
            Console.Write("Total memory after creating second file: "); test2.showTotalMem(); Console.WriteLine();
            AttribFile test3 = new AttribFile();
            Console.Write("Total memory after creating third file: "); test3.showTotalMem(); Console.WriteLine();

            File a = new File("main", "cs", "C:\\", new Date(3,3,2015), 20000);
            File b = new File("libr", "java", "C:\\", new Date(1,1,2014), 35000);
            File c;
            c = a+b;

            Console.WriteLine();
            Console.Write("a = "); a.show();
            Console.Write("\nb = "); b.show();
            Console.Write("\nc = a+b = "); c.show();

            Console.WriteLine();
            Console.Write("a == b : "+(a==b));

            Console.WriteLine();
            Console.Write("!a : "+(!a));

            Console.WriteLine();
            Console.Write("(File)\"settings_word_2013\" : ");
            ((File)"settings_word_2013").show();

            Console.ReadKey();
        }
    }
}
