#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
using namespace std;

class Date {
protected:
	int month, day;
public:
	Date();
	Date(int, int);
	int GetDay() { return day; }
	int GetMonth() { return month; }
	bool SetDay(int);
	bool SetMonth(int);
};

class Article : virtual public Date {
protected:
	string article_name;
	int type;
	double cost;
public:
	Date earnings;
	Article();
	Article(string, int, double, Date, int, int);
	bool SetArticleName(string);
	string GetArticleName() { return article_name; };
	bool SetType(int);
	int GetType() { return type; };
	bool SetCost(double);
	double GetCost(){ return cost; };
};

class Seller : virtual public Date {
protected:
	string seller_name, seller_surname;
	
public:
	Date placement;
	Seller();
	Seller(string, string, Date, int, int);
	bool SetSellerName(string);
	string GetSellerName() { return seller_name; };
	bool SetSellerSurname(string);
	string GetSellerSurname() { return seller_surname; };
};

class Operation : public Seller, public Article {
protected:
	int amount;
public:
	Operation();
	Operation(int, int, string, int, double, Date, string, string, Date, int);
	bool SetAmount(int);
	int GetAmount() { return amount; };

	friend ostream& operator<<(ostream& os, Operation& dt);
};

// ������ ����� Date
Date::Date(){
	month = 1;
	day = 1;
}

Date::Date(int day, int month){
	this->day = day;
	this->month = month;
}

bool Date::SetMonth(int month){
	bool successful = 0;
	if (month >= 1 || month <= 12) {
		successful = 1;
		this->month = month;
	}
	return successful;
}

bool Date::SetDay(int day){
	bool successful = 0;
	int max_day = 0;
	switch (month){
	case 1: case 3: case 5: case 7: case 8: case 10: case 12:
		max_day = 31; break;
	case 2:
		max_day = 28;
		break;
	case 4: case 6: case 9: case 11:
		max_day = 30; break;
	}

	if (day >= 1 || day <= max_day) { 
		successful = 1;
		this->day = day;
	}
	return successful;
}

// ������ ����� Article
Article::Article() : Date(){
	article_name = "No name";
	type = 0;
	cost = 0.0;
	earnings = { 0, 0 };
}

Article::Article(string article_name, int type, double cost, Date earnings, int day, int month) : Date(day, month){
	this->article_name = article_name;
	this->type = type;
	this->cost = cost;
	this->earnings = earnings;
}

bool Article::SetArticleName(string article_name){
	bool successful = 0;
	if ((article_name.size() <= 30) || (article_name.size() >= 3)) {
		successful = 1;
		this->article_name = article_name;
	}
	return successful;
}

bool Article::SetType(int type){
	bool successful = 0;
	// 0 - �������
	// 1 - ������������

	if (type >= 1 || type <= 2) {
		successful = 1;
		this->type = type;
	}
	return successful;
}

bool Article::SetCost(double cost){
	bool successful = 0;
	if (cost >= 1.0 || cost <= 10000.0) {
		successful = 1;
		this->cost = cost;
	}
	return successful;
}

// ������ ����� Seller
Seller::Seller() : Date(){
	seller_name = "No Name";
	seller_surname = "No Surname";
	placement = { 0, 0 };
}

Seller::Seller(string seller_name, string seller_surname, Date placement, int day, int month) : Date(day, month){
	this->seller_name = seller_name;
	this->seller_surname = seller_surname;
	this->placement = placement;
}

bool Seller::SetSellerName(string name){
	bool successful = 0;
	if ((name.size() <= 30) || (name.size() >= 3)) {
		successful = 1;
		this->seller_name = name;
	}
	return successful;
}

bool Seller::SetSellerSurname(string surname){
	bool successful = 0;
	if ((surname.size() <= 30) || (surname.size() >= 3)) {
		successful = 1;
		this->seller_surname = surname;
	}
	return successful;
}

// ������ ����� Operation
Operation::Operation() : Seller(), Article() {
	amount = 0;
}

Operation::Operation(int day, int month, string article_name, int type, double cost, Date earnings, string seller_name, string seller_surname, Date placement, int amount) : Seller(seller_name, seller_surname, placement, day, month), Article(article_name, type, cost, earnings, day, month) {
	this->amount = amount;
}

bool Operation::SetAmount(int amount){
	bool successful = 0;

	if (amount >= 1 || amount <= 500) {
		successful = 1;
		this->amount = amount;
	}
	return successful;
}

ostream& operator<<(ostream& os, Operation& dt)
{
	os << "DATE: " << setfill('0') << setw(2) << dt.GetDay() << "." << setw(2) << dt.GetMonth() << endl;
	os << "ARTICLE" << endl;
	os << "	Name                                : " << dt.GetArticleName() << endl;
	os << "	Date of receipt                     : " << setfill('0') << setw(2) << dt.earnings.GetDay() << "." << setw(2) << dt.earnings.GetMonth() << endl;
	os << "	Type: (0 - weighable, 1 - unpacked) : " << dt.GetType() << endl;
	os << "	Cost                                : " << dt.GetCost() << " $" << endl;
	os << "SELLER: " << endl;
	os << "	Name                                : " << dt.GetSellerName() << endl;
	os << "	Surname                             : " << dt.GetSellerSurname() << endl;
	os << "	Date of placement                   : " << setfill('0') << setw(2) << dt.placement.GetDay() << "." << setw(2) << dt.placement.GetMonth() << endl;
	os << "AMOUNT: " << dt.GetAmount() << endl;

	return os;
}

Operation* DeleteObject(Operation* arr, int length, int pos)
{
	int j = -1; // �������� tmp
	Operation* temp = new Operation[length - 1]; // tmp �����, ���� ����� ������������ �� 1 
	for (int i = 0; i < length; i++)
	{
		if (i != pos)
		{
			j++;  // �������� tmp ��������
			temp[j] = arr[i]; // ������� ������ � ������� ������ � �����
		}
	}

	return temp;
}

Operation* AddObject(Operation* arr, int length, int pos, int day, int month, string article_name, int type, double cost, Date earnings, string seller_name, string seller_surname, Date placement, int amount)
{
	int j = -1; // �������� ������� ������
	Operation* temp = new Operation[length + 1]; // tmp �����, ���� ����� ������������ �� 1 
	for (int i = 0; i < length; i++)
	{
		if (i != pos)
		{
			j++;  // �������� �������� ��� ������� ������
			temp[i] = arr[j]; // ������� ������ � ������� ������ � �����
		}
		else {
			temp[i].SetDay(day);
			temp[i].SetMonth(month);
			temp[i].SetArticleName(article_name);
			temp[i].SetType(type);
			temp[i].SetCost(cost);
			temp[i].earnings = earnings;
			temp[i].SetSellerName(seller_name);
			temp[i].SetSellerSurname(seller_surname);
			temp[i].placement = placement;
			temp[i].SetAmount(amount);
		}
	}

	return temp;
}

template <class T>
void countSum(T * O, int length, string name, string surname)
{
	double sum = 0.0;
	int count = 0;
	for (int i = 0; i < length; i++){
		if ((O[i].GetSellerName() == name) && (O[i].GetSellerSurname() == surname)){
			sum += O[i].GetCost() * O[i].GetAmount();
			count += O[i].GetAmount();
		}
	}
	cout << "Suma : " << sum << endl;
	cout << "Tovarov : " << count << endl;
} 

int main(){
	string path;
	int n = 10;

	Operation * O = new Operation[n];

	O[0] = Operation(1, 12, "Sweets", 2, 12.7 , { 02, 02 }, "Eugene", "Semenov", { 28, 05 }, 1);
	O[1] = Operation(2, 12, "Cup of tea", 1, 5, { 02, 02 }, "Eugene", "Semenov", { 28, 05 }, 1);
	O[2] = Operation(3, 12, "Apples", 1, 10.2, { 02, 02 }, "Ivan", "Petrenko", { 12, 10 }, 1);
	O[3] = Operation(4, 12, "Bananas", 1, 13.99, { 02, 02 }, "Ivan", "Petrenko", { 12, 10 }, 1);
	O[4] = Operation(5, 12, "Tea", 0, 10.5, { 02, 02 }, "Net", "Net", { 22, 02 }, 1);
	O[5] = Operation(6, 12, "Cookies", 1, 20.99, { 02, 02 }, "Eugene", "Semenov", { 28, 05 }, 1);
	O[6] = Operation(7, 12, "Cucumbers", 0, 8.7, { 02, 02 }, "Johnn", "Jhonson", { 23, 04 }, 1);
	O[7] = Operation(8, 12, "Chocolates", 1, 9, { 02, 02 }, "Andriy", "Andreev", { 11, 8 }, 1);
	O[8] = Operation(9, 12, "Icecream", 1, 6.7 , { 02, 02 }, "Denis", "Ivanov", { 11, 11 }, 1);
	O[9] = Operation(10, 12, "Rise", 1, 12.22, { 02, 02 }, "Eugene", "Semenov", { 28, 05 }, 1);



	cout << "Path to binary: ";
	cin >> path;

	fstream file;
	file.open(path, ios::in | ios::out | ios::app | ios::binary);
	for (int i = 0; i < n; i++){
		file.write(reinterpret_cast<char*>(&O[i]), sizeof(O[i]));
	}
	file.seekp(ios::beg);

	countSum(O, n, "Eugene", "Semenov");

	O = AddObject(O, n, 0, 3, 3, "Google", 1, 300, { 10, 10 }, "Ivan", "Petrenko", { 5, 5 }, 1);

	cout << O[0];
	system("pause");
	return 0;
}