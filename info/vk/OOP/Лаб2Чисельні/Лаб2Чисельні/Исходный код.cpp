#include <locale.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>
#include <windows.h>


double round(double double_value)
{
	return floor(double_value + 0.5);
}

void main()
{
	const int n = 4;
	int i, j, k, l;
	double a[5][6];
	double b[1][5];
	double z;
	//zapovnenia i vyvid pochtkovoi matryci
	a[1][1] = -5;
	a[2][1] = 34;
	a[3][1] = 100;
	a[4][1] = 1000;
	a[1][2] = 9;
	a[2][2] = 0;
	a[3][2] = 67;
	a[4][2] = 9;
	a[1][3] = 35;
	a[2][3] = 400;
	a[3][3] = 78;
	a[4][3] = 4;
	a[1][4] = 700;
	a[2][4] = 1;
	a[3][4] = 8;
	a[4][4] = 5;
	a[1][5] = 7;
	a[2][5] = 4;
	a[3][5] = 8;
	a[4][5] = 3;
	printf("Pochatkova matrycia: \n\n");
	for (i = 1; i <= 4; i++)
	{
		for (j = 1; j <= 5; j++)
		{
			printf(" %4.0lf", a[i][j]);
		}
		printf("\n\n");
	}

	//obchislenia
	for (i = 1; i <= n; i++)
	{
		z = a[i][i];
		for (j = i; j <= n + 1; j++)
		{
			a[i][j] = a[i][j] / z;
		}
		for (l = i + 1; l <= n; l++)
		{
			z = a[l][i];
			for (j = i + 1; j <= n + 1; j++)
			{
				a[l][j] = a[l][j] - (a[i][j] * z);
			}
			a[l][i] = 0;
		}
		for (l = i - 1; l >= 1; l--)
		{
			z = a[l][i];
			for (j = i + 1; j <= n + 1; j++)
			{
				a[l][j] = a[l][j] - (a[i][j] * z);
			}
			a[l][i] = 0;
		}
	}

	printf("Kinechna matrycia: \n\n");
	for (i = 1; i <= 4; i++)
	{
		for (j = 1; j <= 5; j++)
		{
			printf(" %4.5lf", a[i][j]);
		}
		printf("\n\n");
	}
	printf("Vector rozviazku: \n\n");
	for (i = 1; i <= n; i++)
	{
		printf("X%d", i); printf("=%3.6lf\n", a[i][n + 1]);
	}
	printf("\n");
	printf("Vector vidhylu: \n\n");
	printf(" %4.6lf\n", (7 - (-5 * a[1][5] + 9 * a[2][5] + 35 * a[3][5] + 700 * a[4][5])));
	printf(" %4.6lf\n", (4 - (34 * a[1][5] + 0 * a[2][5] + 400 * a[3][5] + 1 * a[4][5])));
	printf("%4.6lf\n", (8 - (100 * a[1][5] + 67 * a[2][5] + 78 * a[3][5] + 8 * a[4][5])));
	printf("%4.6lf\n", (3 - (1000 * a[1][5] + 9 * a[2][5] + 4 * a[3][5] + 5 * a[4][5])));
	_getch();
}