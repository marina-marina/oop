#include "lab03.h"

WebPage::WebPage()
{
	cout << "	>>> Default constructor is started!" << endl;
	name = "No name";
	url = "No url";
	adv = 0; 
	visitors = 0;
	day = 01;
	month = 01, 
	year = 1990;
	char * lozung = new char[7];
	this->lozung = "not set";
}

WebPage::WebPage(string name, string url, int year, int month, int day, int adv, int visitors, char *lozung){
	cout << "	>>> Constructor initialization for " << name << " started!" << endl;
	this->name = name;
	this->url = url;
	this->adv = adv;
	this->visitors = visitors;
	this->day = day;
	this->year = year;
	this->month = month;
	this->lozung = new char[sizeof *lozung];
	this->lozung = lozung;
}

WebPage::WebPage(const WebPage & obj)
{
	cout << "	>>> Copy constructor for " << obj.name << " started!" << endl;
	name = obj.name;
	url = obj.url;
	adv = obj.adv;
	visitors = obj.visitors;
	day = obj.day;
	month = obj.month;
	year = obj.year;
	lozung = new char[strlen(obj.lozung) + 1];
	strcpy(lozung, obj.lozung);
}


WebPage::~WebPage()
{
	cout << "	>>> Destructor for " << name << endl;
	if (!lozung)
		delete [] lozung;
}

void WebPage::InputName()
{
	cout << "Enter the name of the company: ";
	name = StringDataCheck(1, 25);
}

string WebPage::GetName(){
	return name; 
}

void WebPage::InputUrl()
{
	string url;
	bool fail;
	do {
		fail = 0;
		cout << "Enter the URL address: ";
		url = StringDataCheck(1, 25);
		if ((url.find(".com") == string::npos) &&
			(url.find(".ua") == string::npos) &&
			(url.find(".ru") == string::npos) &&
			(url.find(".net") == string::npos) &&
			(url.find(".org") == string::npos)){
			fail = 1;
			cout << "The wrong type of URL. URL must include '.com' or '.net' for example" << endl;
		}

	} while (fail);
	
	this->url = url;
}

string WebPage::GetUrl(){
	return url;
}

void WebPage::InputYear()
{
	cout << "Year of last update: ";
	year = IntDataCheck(1990, 2014);
}

int WebPage::GetYear(){
	return year;
}

void WebPage::InputMonth(){
	cout << "Month of last update: ";
	month = IntDataCheck(1, 12);
}

int WebPage::GetMonth(){
	return month;
}

void WebPage::InputDay(){
	int max_day = 0;
	switch (month){
	case 1: case 3: case 5: case 7: case 8: case 10: case 12:
		max_day = 31; break;
	case 2:
		if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
			max_day = 29;
		else
			max_day = 28;
		break;
	case 4: case 6: case 9: case 11:
		max_day = 30; break;
	}
	cout << "Day of last update: ";
	day = IntDataCheck(1, max_day);
}

int WebPage::GetDay(){
	return day;
}

void WebPage::InputAdv(){
	cout << "Number of adv banners: ";
	adv = IntDataCheck(0, 5000);
}

int WebPage::GetAdv(){
	return adv;
}

void WebPage::InputVisitors(){
	cout << "Number of Visitors: ";
	visitors= IntDataCheck(0, 10000000);
}

int WebPage::GetVisitors(){
	return visitors;
}


int WebPage::IntDataCheck(int min, int max){
	int data; bool successful = 0;
	do{
		cin.clear(); cin.sync();
		cin >> data;
		if (cin.fail()) { cout << "Invalid type. Try again!" << endl; }
		else {
			if (data < min || data > max) { cout << "Out of range" << endl; }
			else{ successful = 1; }
		}
	} while (!successful);
	return data;
}

string WebPage::StringDataCheck(int min, int max){
	string data;
	do {
		cin.clear(); cin.sync();
		getline(cin, data);
		if (data.size() > max){ cout << "Too long. Please try again." << endl; }
		if (data.size() < min){ cout << "Too short. Please try again." << endl; }
	} while ((data.size() > max) || (data.size() < min));
	return data;
}

void WebPage::InputLozung(){
	if (!lozung)
		delete [] lozung;
	lozung = new char[25];
	cin.clear(); cin.sync();
	do {
		cout << "Enter the lozung: ";
		gets(this->lozung);
		if (strlen(this->lozung) > 25){ cout << "Too long. Please try again." << endl; }
		if (strlen(this->lozung) < 3){ cout << "Too short. Please try again." << endl; }
	} while ((strlen(this->lozung) > 25) || (strlen(this->lozung) < 3));

}

char * WebPage::GetLozung(){
	return this->lozung;
}
