#include "lab03.h"
using namespace std;


void DataOpenTable(){
	cout << endl << "\xDA";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 25; i++) cout << "\xC4"; cout << "\xBF" << endl;
	cout << fixed << "\xB3" << setw(29) << "Name of the company" << " \xB3" << setw(30) << "URL address" << "\xB3" << setw(15) << "Last upd." << "\xB3" << setw(15) << "Banners" << "\xB3" << setw(15) << "Visitors" << "\xB3" << setw(25) << "Lozung" << "\xB3" << endl;
}

void DataCloseTable(){
	cout << "\xC0";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 25; i++) cout << "\xC4"; cout << "\xD9" << endl;
}


int IntDataCheck(int min, int max){
	int data; bool successful = 0;
	do{
		cin.clear(); cin.sync();
		cin >> data;
		if (cin.fail()) { cout << "Invalid type. Try again!" << endl; }
		else {
			if (data < min || data > max) { cout << "Out of range" << endl; }
			else{ successful = 1; }
		}
	} while (!successful);
	return data;
}


int main(){
	WebPage Page;
	WebPage Apple("Apple", "Apple.com", 2014, 10, 20, 100, 2500000, "Think different");
	WebPage Copy(Apple);
	int n;
	int ind_menu;
	int ind = 1;

	do {
		//system("cls");
		cout << "=======================================================" << endl;
		// ����
		cout << "Main menu" << endl << endl;
		cout << "0 - Exit" << endl;
		cout << "1 - Enter the data" << endl;
		cout << "2 - Output data" << endl;
		cout << "3 - Copy the object Apple to Page" << endl;

		cout << "Your choice: ";
		ind_menu = IntDataCheck(0, 4);

		switch (ind_menu)
		{
		case 0:
			Page.~Page();
			Apple.~Apple();
			Copy.~Copy();
			system("pause");
			return 0;
			
		case 1:
			Page.InputName();
			Page.InputUrl();
			Page.InputYear();
			Page.InputMonth();
			Page.InputDay();
			Page.InputAdv();
			Page.InputVisitors();
			Page.InputLozung();
			break;
		case 2:
			DataOpenTable();
			
			cout << "\xC3";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 25; i++) cout << "\xC4"; cout << "\xB4" << endl;
			cout << fixed << "\xB3" << setw(29) << Apple.GetName() << " \xB3" << setw(30) << Apple.GetUrl() << "\xB3" << setw(7) << Apple.GetDay() << "." << setw(2) << Apple.GetMonth() << "." << Apple.GetYear() << "\xB3" << setw(15) << Apple.GetAdv() << "\xB3" << setw(15) << Apple.GetVisitors() << "\xB3" << setw(25) << Apple.GetLozung() << "\xB3" << endl;
			cout << "\xC3";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 25; i++) cout << "\xC4"; cout << "\xB4" << endl;
			cout << fixed << "\xB3" << setw(29) << Page.GetName() << " \xB3" << setw(30) << Page.GetUrl() << "\xB3" << setw(7) << Page.GetDay() << "." << setw(2) << Page.GetMonth() << "." << Page.GetYear() << "\xB3" << setw(15) << Page.GetAdv() << "\xB3" << setw(15) << Page.GetVisitors() << "\xB3" << setw(25) << Page.GetLozung() << "\xB3" << endl;


			DataCloseTable();
			break;
		case 3:
			cout << "Object successfully copied!" << endl;

			DataOpenTable();

			cout << "\xC3";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 25; i++) cout << "\xC4"; cout << "\xB4" << endl;
			cout << fixed << "\xB3" << setw(29) << Copy.GetName() << " \xB3" << setw(30) << Copy.GetUrl() << "\xB3" << setw(7) << Copy.GetDay() << "." << setw(2) << Copy.GetMonth() << "." << Copy.GetYear() << "\xB3" << setw(15) << Copy.GetAdv() << "\xB3" << setw(15) << Copy.GetVisitors() << "\xB3" << setw(25) << Copy.GetLozung() << "\xB3" << endl;

			DataCloseTable();

			break;
		default:
			cout << "Incorrect button. Try again." << endl; break;
		}
		//���� ����������
		
		cout << endl << "Open the menu again? (0 - No / 1 - Yes)" << endl;
		cout << "Your choice: ";
		ind = IntDataCheck(0, 1);
	} while (ind);
	Page.~Page();
	Apple.~Apple();
	Copy.~Copy();
	system("pause");
	return 0;
}