#include "lab03.h"
using namespace std;

void funcShowCopyConstructor(WebPage object)
{
	cout << "	>>> The function takes an object as a parameter" << endl;
}

void DataOpenTable(){
	cout << endl << "\xDA";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
	for (int i = 1; i <= 25; i++) cout << "\xC4"; cout << "\xBF" << endl;
	cout << fixed << "\xB3" << setw(29) << "Name of the company" << " \xB3" << setw(30) << "URL address" << "\xB3" << setw(15) << "Last upd." << "\xB3" << setw(15) << "Banners" << "\xB3" << setw(15) << "Visitors" << "\xB3" << setw(25) << "Lozung" << "\xB3" << endl;
}

void DataCloseTable(){
	cout << "\xC0";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
	for (int i = 1; i <= 25; i++) cout << "\xC4"; cout << "\xD9" << endl;
}


int main(){
	//WebPage Object;
	//funcShowCopyConstructor(Object);

	WebPage Object(1000, 1000, 12, 12, 1992, "Apple", "Apple.com", "Think");

	bool filled = 1;
	struct PageData {
		string name;
		string url;
		int adv, visitors;
		int day, month, year;
		struct PageData* next;
		char *lozung[25];
	};
	typedef struct PageData *Page_ptr;
	Page_ptr s1 = NULL;
	Page_ptr p = NULL, pred = NULL;
	int n;
	int ind_menu;
	int ind = 1;

	do {
		//system("cls");
		cout << "=======================================================" << endl;
		// ����
		cout << "Main menu" << endl << endl;
		cout << "0 - Exit" << endl;
		cout << "1 - Enter the data" << endl;
		if (filled) { cout << "2 - Output data" << endl; }

		cout << "Your choice: ";
		ind_menu = Object.IntDataCheck(0, 10);

		switch (ind_menu)
		{
		case 0:
			exit(0);
		case 1:
			cout << endl << "	How many pages do you want to add?: ";
			n = Object.IntDataCheck(1, 10);
			
			// qweqwe
			filled = 1;
			for (int i = 0; i<n; i++)
			{
				p = new struct PageData;
				cout << endl << "Web Page number " << i + 1 << ": " << endl;
				char * lozung = new char[25];
				p->name = Object.InputName();
				p->url = Object.InputUrl();
				p->year = Object.InputYear();
				p->month = Object.InputMonth();
				p->day = Object.InputDay(p->month, p->year);
				p->adv = Object.InputAdv();
				p->visitors = Object.InputVisitors();
				*p->lozung = Object.InputLozung();

				p->next = pred; 
				pred = p;
			}
			s1 = p;

			//qweqwe

			break;
		case 2:
			if (filled) { 
				DataOpenTable();
				while (s1 != NULL){
					cout << "\xC3";
					for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
					for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
					for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
					for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
					for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
					for (int i = 0; i < 25; i++) cout << "\xC4"; cout << "\xB4" << endl;
					cout << fixed << "\xB3" << setw(29) << s1->name << " \xB3" << setw(30) << s1->url << "\xB3" << setw(7) << s1->day << "." << setw(2) << s1->month << "." << s1->year << "\xB3" << setw(15) << s1->adv << "\xB3" << setw(15) << s1->visitors << "\xB3" << setw(25) << *s1->lozung << "\xB3" << endl;

					s1 = s1->next;
				}
				DataCloseTable();
			}
			else 
			{ cout << "Incorrect button. Try again." << endl; break; }
			break;
		default:
			cout << "Incorrect button. Try again." << endl; break;
		}
		//���� ����������
		
		cout << endl << "Open the menu again? (0 - No / 1 - Yes)" << endl;
		cout << "Your choice: ";
		ind = Object.IntDataCheck(0, 1);
	} while (ind);

	system("pause");
}