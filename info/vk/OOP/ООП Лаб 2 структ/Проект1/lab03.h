//#pragma once
#include <iostream>
#include <cstdio>
#include <iomanip>
#include <string>
using namespace std;
class WebPage {
private:
	string name, url;
	int adv, visitors, day, month, year;
	char *lozung;
public:
	WebPage();
	WebPage(const WebPage & obj);
	WebPage(string name, string url, int year, int month, int day, int adv, int visitors, char *lozung);
	~WebPage();
	void InputName();
	string GetName();
	void InputUrl();
	string GetUrl();
	void InputYear();
	int GetYear();
	void InputMonth();
	int GetMonth();
	void InputDay();
	int GetDay();
	void InputAdv();
	int GetAdv();
	void InputVisitors();
	int GetVisitors();
	int IntDataCheck(int min, int max);
	string StringDataCheck(int min, int max);
	void InputLozung();
	char *GetLozung();
};
