#include <iostream>
#include <iomanip>
#include <ctime> 
#include <cstdlib>
#include <fstream>
using namespace std;

class Lab1 {
	public:
		int size, s1, s2; // size - ����� �������
		float *current, *newArray; // current - ����������� �����, newArray - ����� ����� � �������������� ����������
		const float eps = 0.01; // ��� ����������� ���������� ������ �������
		const float k = 0.02; // ���������� ��� ������� �������

		struct Var12 {
			short int a, b, x1, x2;
			float dx;
			float *ptrIndex; // �������� �� ������� ������ ��� ���������� ���������� �������

			// �������� ���������
			void input_struct(struct Var12){
				cout << "Enter a  > "; cin >> a;
				cout << "Enter b  > "; cin >> b;
				cout << "Enter x1 > "; cin >> x1;
				cout << "Enter x2 > "; cin >> x2;
				cout << "Enter dx > "; cin >> dx;
				cout << endl;
			}
			// ��������� ��������� � ��������
			void output_struct(struct Var12){
				cout << "Input parameters:" << endl;
				cout << "\xDA";
				for (int j = 0; j < 4; j++){
					for (int i = 0; i < 8; i++) cout << "\xC4"; cout << "\xC2";
				}
				for (int i = 0; i < 8; i++) cout << "\xC4"; cout << "\xBF" << endl;

				cout << "\xB3     a  " << "\xB3     b  " << "\xB3     x1 " << "\xB3     x2 " << "\xB3     dx \xB3" << endl;
				cout << "\xC3";
				for (int j = 0; j < 4; j++){
					for (int i = 0; i < 8; i++) cout << "\xC4"; cout << "\xC5";
				}
				for (int i = 0; i < 8; i++) cout << "\xC4"; cout << "\xB4" << endl;
				cout << "\xB3  " << setw(4) << a << "  \xB3  " << setw(4) << b << "  \xB3  " << setw(5) << x1 << " \xB3" << setw(7) << x2 << " \xB3" << setw(8) << dx << "\xB3" << endl;
				cout << "\xC0";
				
				for (int j = 0; j < 4; j++){
					for (int i = 0; i < 8; i++) cout << "\xC4"; cout << "\xC1";
				}
				for (int i = 0; i < 8; i++) cout << "\xC4"; cout << "\xD9" << endl;
			}
		} data;

		// ���������� �������
		void calculation (float x){
			float *ptrI, *ptrJ;
			for (ptrI = this->data.ptrIndex; ptrI < this->data.ptrIndex + this->size; ptrI++, x = x + this->data.dx){
				if (x <= 1){
					*ptrI = this->data.a * (x * x) + this->data.b;
				}
				else {
					*ptrI = (this->data.b - 2) / x;
				}
			}
		}

		// ���� �������
		void cout_table(float * f, float x){
			cout << endl << "\xDA";
			for (int i = 1; i <= 7; i++) cout << "\xC4"; cout << "\xC2";
			for (int i = 1; i <= 10; i++) cout << "\xC4"; cout << "\xBF" << endl;
			cout << "\xB3" << "     x " << "\xB3" << "    f(x)  " << "\xB3" << endl;
			for (int idx = 0; idx <= this->size - 1; idx++){
				cout << "\xC3";
				for (int i = 0; i < 7; i++) cout << "\xC4"; cout << "\xC5";
				for (int i = 0; i < 10; i++) cout << "\xC4"; cout << "\xB4" << endl;
				cout << fixed << setprecision(1) << "\xB3" << setw(6) << x << " \xB3" << setprecision(3) << setw(8) << f[idx] << "  \xB3" << endl;
				x += this->data.dx;
			}
			cout << "\xC0";
			
			for (int i = 1; i <= 7; i++) cout << "\xC4"; cout << "\xC1";
			for (int i = 1; i <= 10; i++) cout << "\xC4"; cout << "\xD9" << endl;
		}

		// ����������
		float * Valuation(float * f, int size){
			float min = this->data.ptrIndex[0];
			float * out = new float[this->size];
			for (int i = 1; i <= size - 1; i++)
			if (min > f[i])
				min = f[i];
			if (min <= 0)
			for (int i = 0; i <= size - 1; i++)
				out[i] = f[i] + abs(min);
			else
			for (int i = 0; i <= size - 1; i++)
				out[i] = f[i] - min;
			float max = out[0];
			for (int i = 1; i <= size - 1; i++)
			if (max < out[i])
				max = out[i];
			float k = 60. / max;
			for (int i = 0; i <= size - 1; i++)
				out[i] *= k;
			return out;
		}

		int s1_cal�(){
			float *ptrI, *ptrJ;
			float s1, counter;
			counter = 0;
			s1 = 0;
			for (ptrI = this->data.ptrIndex; ptrI < this->data.ptrIndex + this->size; ptrI++){
				if (*ptrI > 1){
					counter++;
					s1 = s1 + *ptrI;
				}
			}
			return s1 / counter;
		}

		int s2_calc(){
			float *ptrI, *ptrJ;
			float s2, counter;
			s2 = 0;
			for (ptrI = this->data.ptrIndex; ptrI < this->data.ptrIndex + this->size; ptrI++){
				s2 = s2 + *ptrI;
			}
			return s2;
		}
		
		// ����������
		void histogram(){
			int sc;
			ofstream fout("D:\\abc\\var12.txt");

			for (int i = 0; i <= 20; i++){
				sc = this->newArray[i] / 6;
				fout << setw(8) << this->newArray[i] << "  |";
				for (int j = 0; j <= sc; j++){
					fout << " ";
				}
				fout << "\xDC";
				fout << endl;
				
			}
			fout.close();
			cout << "Graph saved to a text file" << endl;
		}

		float *Random(){
			int smin, smax;
			if (this->s1 > this->s2) {
				smax = this->s1;
				smin = this->s2;
			}
			else {
				smax = this->s2;
				smin = this->s1;
			}
			float *ptr_array;
			ptr_array = new float[this->size];
			srand((unsigned)time(0));
			float step = smax * 0.02;
			int r = (smax - smin)/step;
			for (int index = 0; index < this->size; index++){
				ptr_array[index] = smin + (rand() % r + 1)*step;
				printf(" %7.2lf\n", ptr_array[index]);
			}

			return ptr_array;
		}
};


int main(){

	Lab1 obj;


	// ������� �� �������� ���������
	obj.data.input_struct(obj.data);
	obj.data.output_struct(obj.data);
	system("pause");

	// ��������� ������� ��� ������� x �� ������� �� �������� �������� ���`���
	obj.size = ((obj.data.x2 - obj.data.x1) / obj.data.dx) + 1 + 0.5;
	obj.data.ptrIndex = new float[obj.size];

	cout << endl << "Value f (x) at the points: " << endl;
	obj.calculation(obj.data.x1);
	obj.cout_table(obj.data.ptrIndex, obj.data.x1);
	system("pause");

	// �������� ������������� �����
	cout << "Normalized values: " << endl;
	obj.current = obj.Valuation(obj.data.ptrIndex, obj.size);
	obj.cout_table(obj.current, obj.data.x1);
	system("pause");

	// ������ s1 �� s2
	obj.s1 = obj.s1_cal�();
	obj.s2 = obj.s2_calc();
	cout << endl << "S min = " << obj.s1 << endl << "S max = " << obj.s2 << endl;
	system("pause");

	// ������� ����� ���������� �����
	cout << endl << "Random array:" << endl;
	obj.newArray = obj.Random();
	system("pause");
	obj.histogram();

	delete[] obj.data.ptrIndex;
	delete[] obj.current;
	delete[] obj.newArray;
	
	system("pause");
	return 0;
}