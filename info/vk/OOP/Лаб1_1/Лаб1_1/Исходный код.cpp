#include <stdio.h>
#include <iostream>
#include <conio.h>
#include<iomanip>
#include "math.h"
using namespace std;
int main() {
	struct s1{
		double a, b, x1, x2, dx;
		float *mas;
	};
	double Smax, Smin, h, f, x, szn, ki;
	double *tmas;
	int i, n;
	s1 s;
	cout << "a=";
	scanf_s("%lf", &s.a);
	cout << "b=";
	scanf_s("%lf", &s.b);
	cout << "x1=";
	scanf_s("%lf", &s.x1);
	cout << "x2=";
	scanf_s("%lf", &s.x2);
	cout << "dx=";
	scanf_s("%lf", &s.dx);
	float u;
	u = 0;
	n = 0;
	double eps = 0.05;
	int k = 0;
	k = (s.x2 - s.x1 + s.dx) / s.dx;
	s.mas = new float[k];
	x = s.x1;
	i = 0;
	printf("       %c             x           f(x)%5c\n", 0xBA, 0xBA);
	while (x <= s.x2 + s.dx)
	{
		if (x <= 0) f = -s.a*x*x - s.b*s.b*x;
		else f = x - s.a / (x - s.b);
		s.mas[i] = f;
		i++; n = i;
		printf("%8c%15.1f%15.2f%4c\n", 0xBA, x, f, 0xBA);
		x = x + s.dx;
	}
	double min = s.mas[0];
	for (int i = 1; i<n; i++){
		if (i % 2 != 0)
			u += s.mas[i];
		szn = u / (ceil(n / 2));
		if ((i % 2 == 0) && (s.mas[i]<min)) min = s.mas[i - 1];
	}
	double minv = s.mas[0];
	for (i = 0; i<n; i++)
	if (s.mas[i]<minv) minv = s.mas[i];
	if (minv<0)
	for (i = 0; i<n; i++)
		s.mas[i] += -minv;
	double max = s.mas[0];
	for (i = 0; i<n; i++)
	if (s.mas[i]>max) max = s.mas[i];
	for (i = 0; i<n; i++)
		s.mas[i] = s.mas[i] / max * 75;
	int t = 0;
	for (i = 0; i<n; i++){
		for (int j = 0; j<ceil(s.mas[i]); j++)
			printf("%c", 0xB0);
		printf("\n");
	}
	printf("S1=");
	printf("%3.2f\n", min);
	printf("S2=");
	printf("%3.2f\n", szn);
	if (min>szn) { Smax = min; Smin = szn; }
	else { Smax = szn; Smin = min; }
	h = eps*Smax;
	if (Smax<0) h = -h;
	int r = (Smax - Smin) / h;
	ki = (s.x2 - s.x1) / s.dx;
	tmas = new double[1000];
	for (int j = 0; j <= ki; j++) {
		tmas[j] = Smin + (rand() % r + 1)*h;
		printf(" %7.2lf\n", tmas[j]);
		tmas[j] = (tmas[j] - Smin) / h;
		printf("%1.2f", tmas[j]);
	}
	_getch();
	return 0;
}