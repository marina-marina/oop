#include <iostream>
#include <string>
#include <stdio.h>
#include <iomanip>
using namespace std;

class Kernel {
	bool control = 0;
	struct web_page {
		char *name;
		char *url;
		int update_day, update_month, update_year;
		int adv;
		int visitors;
		bool status = 0;
	} first_page;
public:
	
	void ShowMainMenu(){
		short int ind;
		
		cout << "Main menu" << endl << endl;
		cout << "0 - Exit" << endl;
		cout << "1 - Enter the data" << endl;
		if (this->first_page.status){
			cout << "2 - Output data" << endl;
		}
		
		do{
			cout << "Your choice: ";
			cin >> ind;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			} else { this->control = 1; }
		} while (!this->control);
		this->control = 0;


		switch (ind)
		{
		case 0:
			exit(0);
		case 1:
			this->DataInput();  break;
		case 2:
			if (this->first_page.status) { this->DataOutput(); } else { cout << "Incorrect button. Try again." << endl; }
			break;
		default: 
			cout << "Incorrect button. Try again." << endl; break;
		}
	}

	void DataInput(){
		this->first_page.status = 1;
		int max_day = 0;

		// �������� ������
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());

		this->first_page.name = new char[255];
		this->first_page.url = new char[255];
		cout << endl;
		do {
			cout << "Enter the name of the company: ";
			gets(this->first_page.name);
			if (strlen(this->first_page.name) > 25){ cout << "Too long. Please try again." << endl; }
			if (strlen(this->first_page.name) < 3){ cout << "Too short. Please try again." << endl; }
		} while ((strlen(this->first_page.name) > 25) || (strlen(this->first_page.name) < 3));

		do {
			cout << "Enter the URL address: ";
			gets(this->first_page.url);
			if (strlen(this->first_page.url) > 25){ cout << "Too long. Please try again." << endl; }
			if (strlen(this->first_page.url) < 3){ cout << "Too short. Please try again." << endl; }
		} while ((strlen(this->first_page.url) > 25) || (strlen(this->first_page.url) < 3));

		do{
			cout << "Year of last update: ";
			cin >> this->first_page.update_year;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			}
			else {
				if ((this->first_page.update_year < 1800) || (this->first_page.update_year > 2014)) { cout << "Incorrect year. Please try again." << endl; }
				else { this->control = 1; }
			}
		} while (((this->first_page.update_year < 1800) || (this->first_page.update_year > 2014)) || !this->control);
		this->control = 0;
		do{
			cout << "Month of last update: ";
			cin >> this->first_page.update_month;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			}
			else {
				if ((this->first_page.update_month > 12) || (this->first_page.update_month < 1)) { cout << "Incorrect month. Please try again." << endl; }
				else { this->control = 1; }
			}
		} while (((this->first_page.update_month > 12) || (this->first_page.update_month < 1)) || !this->control);
		this->control = 0;
		do {
			cout << "Day of last update: ";
			cin >> this->first_page.update_day;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			}
			else {
				switch (this->first_page.update_month){
				case 1: case 3: case 5: case 7: case 8: case 10: case 12:
					max_day = 31; break;
				case 2:
					if ((this->first_page.update_year % 4 == 0) && ((this->first_page.update_year % 100 != 0) || (this->first_page.update_year % 400 == 0)))
						max_day = 29;
					else
						max_day = 28;
					break;
				case 4: case 6: case 9: case 11:
					max_day = 30; break;
				}
			
				if ((this->first_page.update_day > max_day) || (this->first_page.update_day < 1)) { cout << "Incorrect day. Please try again." << endl; }
				else { this->control = 1; }
			}
		} while (((this->first_page.update_day > max_day) || (this->first_page.update_day < 1)) || !this->control);
		this->control = 0;
		
		do{
			cout << "Number of adv banners: ";
			cin >> this->first_page.adv;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			}
			else { this->control = 1; }
		} while (!this->control);
		this->control = 0;

		do{
			cout << "Number of Visitors: ";
			cin >> this->first_page.visitors;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			}
			else { this->control = 1; }
		} while (!this->control);
		this->control = 0;

		cout << endl << "Ok, thx, all your information is already saved!" << endl;
	}

	void DataOutput(){
		cout << endl << "\xDA";
		for (int i = 1; i <= 20; i++) cout << "\xC4"; cout << "\xC2";
		for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xBF" << endl;
		cout << fixed << "\xB3" << setw(19) << "Name of the company" << " \xB3" << setw(28) << this->first_page.name << "  \xB3" << endl;
		cout << "\xC3";
		for (int i = 0; i < 20; i++) cout << "\xC4"; cout << "\xC5";
		for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xB4" << endl;
		cout << fixed << "\xB3" << setw(19) << "URL address" << " \xB3" << setw(28) << this->first_page.url << "  \xB3" << endl;
		cout << "\xC3";
		for (int i = 0; i < 20; i++) cout << "\xC4"; cout << "\xC5";
		for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xB4" << endl;
		cout << fixed << "\xB3" << setw(19) << "Last update" << " \xB3" << setw(20) << this->first_page.update_day << "." << setw(2) << this->first_page.update_month << "." << setw(2) << this->first_page.update_year <<"  \xB3" << endl;
		cout << "\xC3";
		for (int i = 0; i < 20; i++) cout << "\xC4"; cout << "\xC5";
		for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xB4" << endl;
		cout << fixed << "\xB3" << setw(19) << "Adv banners" << " \xB3" << setw(28) << this->first_page.adv << "  \xB3" << endl;
		cout << "\xC3";
		for (int i = 0; i < 20; i++) cout << "\xC4"; cout << "\xC5";
		for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xB4" << endl;
		cout << fixed << "\xB3" << setw(19) << "Number of Visitors" << " \xB3" << setw(28) << this->first_page.visitors << "  \xB3" << endl;
		cout << "\xC0";
		for (int i = 1; i <= 20; i++) cout << "\xC4"; cout << "\xC1";
		for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xD9" << endl;
	}
};

int main(){
	Kernel WebPage;
	bool control = 0;
	bool ind = 1;
	do {
		system("cls");
		WebPage.ShowMainMenu();
		cout << endl << "Open the menu again? (0/1)" << endl;
		do{
			cout << "Your choice: ";
			cin >> ind;
			if (cin.fail()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cout << "Invalid format! Try again!" << endl;
			}
			else { control = 1; }
		} while (!control);
		control = 0;
	} while (ind);

	system("pause");
}