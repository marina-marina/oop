﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    
    public class Date {
        protected int month, day;

        public Date(){
            month = 0;
	        day = 0;
        }

	    public Date(int day, int month){
	        this.day = day;
	        this.month = month;
            Console.WriteLine("Coustructor Date");
        }
        public void SetMonth(int month){ this.month = month; }
        public void SetDay(int day){ this.day = day; }
	    public int GetDay() { return day; }
	    public int GetMonth() { return month; }
    };

    class Article : Date {
	    protected string article_name;
	    protected int type;
	    protected double cost;

        public Article() : base(){
	        article_name = "No name";
	        type = 0;
	        cost = 0.0;
        }

        public Article(string article_name)
            : this(article_name, 0, 0.0, 0, 0)
        {
            Console.WriteLine("article name initialization");
        }

        public Article(string article_name, int type, double cost, int day, int month ) : base(day, month)
        {
	        this.article_name = article_name;
	        this.type = type;
	        this.cost = cost;
            Console.WriteLine("Coustructor Article");
        }

        public void SetArticleName(string article_name){ this.article_name = article_name; }
        public void SetType(int type){ this.type = type; }
        public void SetCost(double cost){ this.cost = cost; }
        string GetArticleName() { return article_name; }
	    int GetType() { return type; }
	    double GetCost(){ return cost; }
    };

    class Seller : Date {
	    protected string seller_name, seller_surname;
	
	    public Seller() : base(){
	        seller_name = "No Name";
	        seller_surname = "No Surname";
        }

        public Seller(string seller_name, string seller_surname, int day, int month) : base(day, month){
	        this.seller_name = seller_name;
	        this.seller_surname = seller_surname;
            Console.WriteLine("Coustructor Seller");
        }

        public void SetSellerName(string name){ this.seller_name = name; }
        public void SetSellerSurname(string surname){ this.seller_surname = surname; }
        string GetSellerName() { return seller_name; }
	    string GetSellerSurname() { return seller_surname; }
    };

    class Operation : Article {
    
	    protected int amount;
        protected Seller human;
        private static int sum = 0;

        static Operation() 
        {
            sum++;
            Console.WriteLine("Static coustructor Operation");
        } 

        //public Operation() : base()
        //{
        //    amount = 0;
        //    human = new Seller("No Name", "No Surname", 2, 2);
        //}


        public Operation()
            : this(28, 05, "Potatos", 1, 14.99 , 2, "Eugene", "Semenov", 22, 12)
        {
            Console.WriteLine("Chain of Coustructor Operation");
        }

        public Operation(int day, int month, string article_name, int type, double cost, int amount, string seller_name, string seller_surname, int day1, int month1)
            : base(article_name, type, cost, day, month)
        {
	        this.amount = amount;
            human = new Seller(seller_name, seller_surname, day1, month1);
            Console.WriteLine("Coustructor Operation");
        }

        public void SetAmount(int amount){ this.amount = amount; }
	    int GetAmount() { return amount; }

    };

    
    
    class Program
    {
        static void Main(string[] args)
        {
            Operation test = new Operation();
            //Operation O = new Operation(22, 12, "Bananas", 1, 12.33, 5, "Ivan", "Ivanov", 3,4);
            //Article A = new Article();

            Console.ReadLine();
        }
    }
}
