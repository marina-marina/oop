#include <iostream>
#include <cstdio>
#include <iomanip>
#include <string>
using namespace std;

class WebPage {
private:
	bool filled;
	struct PageData{
		
		string name;
		string url;
		int adv, visitors;
		int day, month, year;
		struct PageData* next;
	};
	
	typedef struct PageData *Page_ptr;
	

public:	
	WebPage(){
		cout << "	>>> Constructor initialization started!" << endl;
		filled = 0;
	};
	
	WebPage(const WebPage & obj)
	{
		cout << "	>>> The copy constructor is initialized !" << endl;
	};

	~WebPage(){
		cout << "	>>> Destructor" << endl;
	}
	Page_ptr s1 = NULL;
	Page_ptr input(int n){
		Page_ptr p = NULL, pred = NULL;
		filled = 1;
		for (int i = 0; i<n; i++)
		{
			p = new struct PageData;
			cout << endl << "Web Page number " << i+1 << ": "  << endl;
			//p->filled = 1;
			p->name = this->InputName();
			p->url = this->InputUrl();
			p->year = this->InputYear();
			p->month = this->InputMonth();
			p->day = this->InputDay(p->month, p->year);
			p->adv = this->InputAdv();
			p->visitors = this->InputVisitors();

			p->next = pred;
			pred = p;
		}
		return p;
	}
	
	string InputName(){
		cout << "Enter the name of the company: ";
		//name = StringDataCheck(1, 25);
		return StringDataCheck(1, 25);
	}
	
	string InputUrl(){
		cout << "Enter the URL address: ";
		//url = StringDataCheck(1, 25);
		return StringDataCheck(1, 25);
	}

	int InputYear(){
		cout << "Year of last update: ";
		//year = IntDataCheck(1900, 2014);
		return IntDataCheck(1900, 2014);
	}

	int InputMonth(){
		cout << "Month of last update: ";
		//month = IntDataCheck(1, 12);
		return IntDataCheck(1, 12);
	}

	int InputDay(int month, int year){
		int max_day = 0;
		switch (month){
		case 1: case 3: case 5: case 7: case 8: case 10: case 12:
			max_day = 31; break;
		case 2:
			if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
				max_day = 29;
			else
				max_day = 28;
			break;
		case 4: case 6: case 9: case 11:
			max_day = 30; break;
		}
		cout << "Day of last update: ";
		//day = IntDataCheck(1, max_day);
		return IntDataCheck(1, max_day);
	}

	int InputAdv(){
		cout << "Number of adv banners: ";
		//adv = IntDataCheck(0, 5000);
		return IntDataCheck(0, 5000);
	}

	int InputVisitors(){
		cout << "Number of Visitors: ";
		//visitors = IntDataCheck(0, 10000000);
		return IntDataCheck(0, 10000000);
	}

	void DataOutput(Page_ptr p){
		//object.Page_ptr x = p;
		Page_ptr x = p;

		cout << endl << "\xDA";
		for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC2";
		for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC2";
		for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
		for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC2";
		for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xBF" << endl;
		cout << fixed << "\xB3" << setw(29) << "Name of the company" << " \xB3" << setw(30) << "URL address" << "\xB3" << setw(15) << "Last upd." << "\xB3" << setw(15) << "Banners" << "\xB3" << setw(15) << "Visitors" << "\xB3" << endl;
		
		while (x != NULL){
			cout << "\xC3";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 30; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xC5";
			for (int i = 0; i < 15; i++) cout << "\xC4"; cout << "\xB4" << endl;
			cout << fixed << "\xB3" << setw(29) << x->name << " \xB3" << setw(30) << x->url << "\xB3" << setw(7) << x->day << "." << setw(2) << x->month << "." << x->year << "\xB3" << setw(15) << x->adv << "\xB3" << setw(15) << x->visitors << "\xB3" << endl;
			x = x->next;
		}
		cout << "\xC0";
		for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC1";
		for (int i = 1; i <= 30; i++) cout << "\xC4"; cout << "\xC1";
		for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
		for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xC1";
		for (int i = 1; i <= 15; i++) cout << "\xC4"; cout << "\xD9" << endl;
	}


	bool isFilled(){
		if (filled){ return 1; }
		else { return 0; }
	}

	// �������� int
	int IntDataCheck(int min, int max){
		int data; bool successful = 0;
		do{
			cin.clear(); cin.sync();
			cin >> data;
			if (cin.fail()) { cout << "Invalid type. Try again!" << endl; }
			else { 
				if (data < min || data > max) { cout << "Out of range" << endl; }
				else{ successful = 1; }
			}
		} while (!successful);
		return data;
	}
	
	// �������� string
	string StringDataCheck(int min, int max){
		string data;
		do {
			cin.clear(); cin.sync();
			getline(cin, data);
			if (data.size() > max){ cout << "Too long. Please try again." << endl; }
			if (data.size() < min){ cout << "Too short. Please try again." << endl; }
		} while ((data.size() > max) || (data.size() < min));
		return data;
	}
	

};

void funcShowCopyConstructor(WebPage object)
{
	cout << "	>>> The function takes an object as a parameter" << endl;
}


int main(){
	WebPage Object;
	funcShowCopyConstructor(Object);
	int n;
	int ind_menu;
	int ind = 1;

	do {
		//system("cls");
		cout << "=======================================================" << endl;
		// ����
		cout << "Main menu" << endl << endl;
		cout << "0 - Exit" << endl;
		cout << "1 - Enter the data" << endl;
		if (Object.isFilled()) { cout << "2 - Output data" << endl; }

		cout << "Your choice: ";
		ind_menu = Object.IntDataCheck(1, 10);

		switch (ind_menu)
		{
		case 0:
			exit(0);
		case 1:
			cout << endl << "	How many pages do you want to add?: ";
			n = Object.IntDataCheck(1, 10);
			Object.s1 = Object.input(n);
			/*Object.InputName();
			Object.InputUrl();
			Object.InputYear();
			Object.InputMonth();
			Object.InputDay();
			Object.InputAdv();
			Object.InputVisitors();*/

			break;
		case 2:
			if (Object.isFilled()) { Object.DataOutput(Object.s1); }
			else { cout << "Incorrect button. Try again." << endl; break; }
			break;
		default:
			cout << "Incorrect button. Try again." << endl; break;
		}
		//���� ����������
		
		cout << endl << "Open the menu again? (0 - No / 1 - Yes)" << endl;
		cout << "Your choice: ";
		ind = Object.IntDataCheck(0, 1);
	} while (ind);

	system("pause");
}