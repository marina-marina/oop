#pragma once
#include <iostream>
#include <string>

using namespace std;

enum CodecEnum {wav, mp3, ogg, flac };

class MusicFile
{
private:
	char* description;
	char* albumName;
	char* fileName;
	CodecEnum codek;
	char* path;
	int seconds;
	int minutes;
	double size;
	int day;
	int month;
	int year;
public:
	char*  getDescription();
	char* getAlbumName();
	char* getFileName();
	char* getPath();
	int getSeconds();
	int getMinutes();
	double getSize();
	int getDay();
	int getMonth();
	int getYear();
	void printName();
	void scanAlbumName();
	void setFileName();
	void setCodec();
	void setPath();
	void setSeconds();
	void setMinutes();
	void setSeconds(int);
	void setMinutes(int);
	void setSize(double);
	void setSize();
	void setDate();
	void table();
	char* printEnum();

	// Overload operators
	double operator+ (double); 
	MusicFile operator+(MusicFile&); 
	friend double operator+(double a,MusicFile& b) {
		return a + b.size;
	}

	bool operator>(const MusicFile& a) {
		if (size > a.size) {
			return true;
		}
		else {
			return false;
		}
	}
	operator double();
	// End oo

	// Constructors and destructor declaring
	MusicFile();
	MusicFile(char*, char*, CodecEnum, char*, int, int, double, int, int, int,char*);
	MusicFile(const MusicFile&);
	~MusicFile();
	// END

	
	
};

