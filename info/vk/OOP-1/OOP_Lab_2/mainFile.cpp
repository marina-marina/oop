#include "MusicFile.h"

void table(MusicFile** file,int N) {
	printf("|----------------------------------------------------------|\n");
	printf("|N|    Album name   | Codec |    File name   |    Path     |\n");
	printf("|----------------------------------------------------------|\n");
	for (int i = 0; i < N; i++) {
		printf("|%d|%17s|%7s|%16s|%13s|\n", i, file[i]->getAlbumName(), file[i]->printEnum(), file[i]->getFileName(), file[i]->getPath());
	}
	printf("|----------------------------------------------------------|\n");
	printf("\n");
	printf("|----------------------------------------------------------|\n");
	printf("|N|    Time     |Creation date|File size|Description       |\n");
	printf("|----------------------------------------------------------|\n");
	for (int i = 0; i < N; i++) {
		printf("|%d|%2d min %2d sec| %2d.%2d.%4d  |%9.3lf|%18s|\n",i,  file[i]->getMinutes(), file[i]->getSeconds(), file[i]->getDay(), file[i]->getMonth(), file[i]->getYear(), file[i]->getSize(),file[i]->getDescription());
	}
	printf("|----------------------------------------------------------|\n");
	printf("\n");


}

void table(MusicFile *file) {
	printf("|----------------------------------------------------------|\n");
	printf("|N|    Album name   | Codec |    File name   |    Path     |\n");
	printf("|----------------------------------------------------------|\n");
		printf("|%d|%17s|%7s|%16s|%13s|\n", 0, file->getAlbumName(), file->printEnum(), file->getFileName(), file->getPath());
	printf("|----------------------------------------------------------|\n");
	printf("\n");
	printf("|----------------------------------------------------------|\n");
	printf("|N|    Time     |Creation date|File size|Description       |\n");
	printf("|----------------------------------------------------------|\n");
	printf("|%d|%2d min %2d sec| %2d.%2d.%4d  |%9.3lf|%18s|\n",0, file->getMinutes(), file->getSeconds(), file->getDay(), file->getMonth(), file->getYear(), file->getSize(),file->getDescription());
	printf("|----------------------------------------------------------|\n");
	printf("\n");


}

void input(MusicFile* file) {
	file->scanAlbumName();
	file->setCodec();
	file->setDate();
	file->setFileName();
	file->setMinutes();
	file->setSeconds();
	file->setPath();
	file->setSize();
}


void main() {
	
	
	MusicFile initializedFile("Metallica", "firstTrack", wav, "c:\myMusic", 31, 5, 25, 11, 12, 2015, "File with my music");
	MusicFile initializedFile1("fdsfas", "fdsa", wav, "c:\mfasdc", 31, 5, 25, 11, 12, 2015, "File with my music");
	printf("Print initialized file \n");
	table(&initializedFile);
	printf("Print initialized1 file \n");
	table(&initializedFile1);

	
	MusicFile copyFile(initializedFile);
	

	
	printf("Summing \n");
	MusicFile joinedFile = initializedFile + copyFile;

	
	printf("test bool %d\n",joinedFile > copyFile);
	printf("test casting %lf\n", (double)joinedFile);
	printf("Print initialized file \n");
	table(&initializedFile);

	printf("Print copied file \n");
	table(&copyFile);

	printf("Print joined file \n");
	table(&joinedFile);
	

	

	MusicFile* files;
	int boole = 0;
	int count = 0;
	
	
	int choise = 0;

	do {
		fflush(stdin);
		printf("Please, write number of objects (1..5)");
		scanf("%d", &count);
		system("cls");
		files = new MusicFile[count];
	} while (count > 5 || count < 1);

	while (true) {
		fflush(stdin);
		printf("Please, choose a function \n");
		printf("1 - Input data \n");
		printf("2 - Print table \n");
		printf("3 - Exit program \n");
		scanf("%d",&choise);
		
		if (choise != 1 && choise != 2 && choise != 3) {
			continue;
			
		}
		
		switch (choise)
		{
		case 1: 
			for (int i = 0; i < count; i++) {
				input(&files[i]);
			}
				boole = 1;
			 continue; break;
		case 2:system("cls"); if (boole == 1) { 
				table(&files,count);
		}
			   else { printf("Class not initialized \n"); } continue; break;
		case 3:
			return; break;
		default:
			continue;  break;
		}
		
	}
}

