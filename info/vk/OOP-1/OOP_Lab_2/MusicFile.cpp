#include "MusicFile.h"
#include "stdio.h"
#include "iostream"


//Constructors
MusicFile::MusicFile()
{
	char buf[] = "unknown";
	this->description = new char[sizeof(buf) + 1];
	strcpy(this->description,buf);

	printf("Default constructor \n");
}

MusicFile::MusicFile(char* albumName, char* fileName, CodecEnum kodec, char* path, int second, int minute, double sized, int day, int month, int year,char* description) {
	printf("Initialization constructor \n");
	if (strlen(albumName) < 20) {
		this->albumName = new char[strlen(albumName) + 1];
		strcpy(this->albumName,albumName);
	}
	else {
		this->albumName = "NULL";
	}
	if (strlen(fileName) < 20) {
		this->fileName = new char[strlen(fileName) + 1];
		strcpy(this->fileName,fileName);
	}
	else {
		this->fileName = "NULL";
	}
	this->codek = kodec;
	if (strlen(path) < 20) {
		this->path = new char[strlen(path) + 1];
		strcpy(this->path,path);
	}
	else {
		this->path = "NULL";
	}
	if (minute >= 0 && minute < 60) {
		this->minutes = minute;
	}
	else {
		this->minutes = -1;
	}
	if (second > 60) {
		minutes = minutes + second / 60;
		seconds = second % 60;
	}
	else {
		seconds = second;
	}
	if (second < 0) {
		seconds = -1;
	}
	if (sized >= 0) {
		this->size = sized;
	}
	else {
		this->size = -100;
	}
	if (day >= 1 && day <= 31) {
		this->day = day;
	}
	else {
		this->day = -1;
	}

	if (month >= 1 && month <= 12) {
		this->month = month;
	}
	else {
		this->month = -1;
	}

	if (year >= 1900 && year <= 2015) {
		this->year = year;
	}
	else {
		this->year = -1;
	}
	
	
	this->description = new char[strlen(description)+1];
	strcpy(this->description,description);
}

MusicFile::MusicFile(const MusicFile &a) {
	printf("Copy constructor \n");
	albumName = new char[strlen(a.albumName) + 1];
	strcpy(albumName, a.albumName);
	fileName = new char[strlen(a.fileName) + 1];
	strcpy(fileName, a.fileName);
	description = new char[strlen(a.description) + 1];
	strcpy(description, a.description);
	codek = a.codek;
	path = new char[strlen(a.path) + 1];
	strcpy(path, a.path);
	seconds = a.seconds;
	minutes = a.minutes;
	size = a.size;
	year = a.year;
	month = a.month;
	day = a.day;
}


MusicFile::~MusicFile()
{
	printf("destructor \n");
	if (!albumName) delete[] albumName;
	if (!fileName) delete[] fileName;
	if (!path) delete[] path;
	if (!description) delete[] description;
}


//END


// Scanners
void MusicFile::scanAlbumName() {
	char* name = new char[100];
	cin.get();
	do {
		printf("Please, enter name of album (to 20 chars) \n");
		gets(name);
	} while (strlen(name) > 20);
	albumName = name;
}

void MusicFile::setCodec() {
	int codecNum = 0;
	
	do {
		printf("Please, enter codec 1-4: 1 - wav,2 - mp3, 3 - ogg, 4-flac \n");
		scanf("%d",&codecNum);
		fflush(stdin);
		switch (codecNum) {
		case 1: codek = wav; break;
		case 2: codek = mp3; break;
		case 3: codek = ogg; break;
		case 4: codek = flac; break;
		default: printf("Please, enter values from 1 to 4 \n"); break;
		};
	} while (codecNum != 1 && codecNum != 2 && codecNum != 3 && codecNum != 4);
	
}

void MusicFile::setDate() {
	do {
		printf("Please, enter creation day from 1 to 31 \n");
		fflush(stdin);
		scanf("%d", &day); 
	} while (day < 1 || day > 31);

	do {
		printf("Please, enter creation month from 1 to 12 \n");
		scanf("%d", &month);
		fflush(stdin);
	} while (month < 1 || month > 12);

	do {
		printf("Please, enter creation year from 1900 to 2015 \n");
		scanf("%d", &year);
		fflush(stdin);
	} while (year < 1900 || year > 2015);
	
}

void MusicFile::setFileName() {
	char* fName = new char[100];
	cin.get();
	do {
		printf("Please, enter filename (to 20 chars)\n");
		gets(fName);
	} while (strlen(fName) > 20);
	fileName = fName;
}

void MusicFile::setMinutes() {
	do {
		printf("Please,enter length of record: minutes from 1 to 60 \n");
		scanf("%d", &minutes);
		fflush(stdin);
	} while (minutes < 1 || minutes > 60);
}

void MusicFile::setSeconds() {
	do {
		printf("Please,enter length of record: seconds from 1 to 60 \n");
		scanf("%d", &seconds);
		fflush(stdin);
	} while (seconds < 1 || seconds > 60);
}

void MusicFile::setPath() {
	char* name = new char[100];
	cin.get();
	do {
		
		printf("Please, enter path (to 20 chars)\n");
		gets(name);
	} while (strlen(name) > 20);
	path = name;
}


void MusicFile::setSize() {
	do {
		printf("Please,enter size of file(positive) \n");
		scanf("%lf", &size);
		fflush(stdin);
	} while (size <= 0);
	
}

char* MusicFile::printEnum() {
	switch (codek) {
	case wav: return "wav"; break;
	case mp3: return "mp3"; break;
	case ogg: return "ogg"; break;
	case flac:return "flac"; break;
	};

	
}
// END

// Getters
char* MusicFile::getAlbumName(){
	return albumName;
}
char* MusicFile::getFileName() {
	return fileName;
}
char* MusicFile::getPath() {
	return path;
}
int MusicFile::getSeconds() {
	return seconds;
}
int MusicFile::getMinutes() {
	return minutes;
}
double MusicFile::getSize() {
	return size;
}
int MusicFile::getDay() {
	return day;
}
int MusicFile::getMonth() {
	return month;
}
int MusicFile::getYear() {
	return year;
}
//
char* MusicFile::getDescription() {
	return description;
}
// END

// Setters
void MusicFile::setMinutes(int minute) {
	if ((minute >= 0) && (minute <= 60)) {
		minutes = minute;
	}
	else {
		minutes = -100;
	}
}

void MusicFile::setSeconds(int second) {
	if (second > 60) {
		minutes = minutes + second / 60;
		seconds = second % 60;
	}
	else {
		seconds = second;
	}
	if (second < 0) {
		seconds = -1;
	}
}

void MusicFile::setSize(double sized) {
	size = sized;
}
// END

// Overload operators
MusicFile MusicFile::operator+(MusicFile& a){
	MusicFile temp(*this);
	system("pause");
	temp.setMinutes(minutes + a.getMinutes());
	temp.setSeconds(seconds + a.getSeconds());
	temp.setSize(size + a.getSize());
	
	return temp;
}

double MusicFile::operator+ (double a) {
	return size + a;
}

MusicFile::operator double() {
	return size;
}
// END





