#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <direct.h>
#include <fstream>

using namespace std;


enum flowerType { Rose, Carnation, Gerber, Cyclamen,Unknown_};
enum salaryEnum { Flowerpot, Bouquet, Apiece,Unknown};
class Date {
private:
	int year;
	int week;
public :
	int getYear() { return year;}
	int getWeek(){ return week;}
	void setYear(int year_) { year = year_;}
	void setWeek(int week_) { week = week_; }
	Date() {
		year = 1900;
		week = 0;
	}
	Date(int year_,int week_) {
		year = year_;
		week = week_;
	}
};

class FlowerName {
private:
	flowerType name;
public:
	char* getName() {
		switch (name) {
		case Rose: return "Rose"; break;
		case Carnation: return "Carnation"; break;
		case Gerber: return "Gerber"; break;
		case Cyclamen:return "Cyclamen"; break;
		case Unknown_:return "Unknown"; break;
		default:return "Unknown"; break;
		};
	}

	void setName(flowerType type) {
		name = type;
	}



	FlowerName() {
		name = Unknown_;
	}
	FlowerName(flowerType type_) {
		name = type_;
	}
};


class Flower : public FlowerName {
private:
	char color[50];
public:
	char* getColor() {
		return color;
	}

	void setColor(char* color_) {
		strcpy(color,color_);
	}

	Flower() {
		strcpy(color,"unknown");
	}

	Flower(char* color_,flowerType name_) :FlowerName(name_){
		strcpy(color,color_);
	}
};

class salaryType {
private:
	salaryEnum salaryName;
public:
	char* getSalaryName() {
		switch (salaryName) {
		case Flowerpot: return "Flowerpot"; break;
		case Bouquet: return "Bouquet"; break;
		case Apiece: return "Apiece"; break;
		case Unknown : return "Unknown"; break;
		default:return "Unknown"; break;
		};
	}

	void setSalaryName(salaryEnum type) {
		salaryName = type;
	}

	salaryType() {
		salaryName = Unknown;
	}

	salaryType(salaryEnum salaryName_) {
		salaryName = salaryName_;
	}
};


class GoodsInstance : public Flower,public salaryType {
private:
	double price;
public:
	double getPrice() { return price; }
	void setPrice(double price_) {
		price = price_;
	}

	GoodsInstance() : Flower(),salaryType() {
		price = 0;
	}

	GoodsInstance(double price_,salaryEnum salaryName_,char* color_,flowerType name_) : Flower(color_,name_),salaryType(salaryName_) {
		price = price_;
	}
};

class SalaryOperation : public GoodsInstance,public Date{
private:
	int count;
	double operationCost;
public:

	friend ostream& operator<<(ostream& os, SalaryOperation &operation){
		os << "Year: " << operation.getYear() << endl;
		os << "Week" << operation.getWeek() << endl;
		os << "Flower name: " << operation.getName() << endl;
		os << "Color: " << operation.getColor() << endl;
		os << "Salary Type: " << operation.getSalaryName() << endl;
		os << "Price: " << operation.getPrice() << endl;
		os << "Number of operations: " << operation.getCount() << endl;
		os << "Operation cost: " << operation.getOperationCost() << endl;
		return os;
	}

	friend istream& operator >> (istream& is, SalaryOperation &operation) {
		double temp;
		int temp1;
		char temp2[50];
		cout << "Please, enter year" << endl;
		is >> temp1;
		operation.setYear(temp1);
		cout << "Please, enter week" << endl;
		is >> temp1;
		operation.setWeek(temp1);

		// SWITCH
		do {
			cout << "Please, enter flower name 1- Rose, 2- Carnation, 3- Gerber, 4-Cyclamen" << endl;
			is >> temp2;
			temp1 = atoi(temp2);
		} while (temp1 < 1 || temp1 > 4);
		
		switch (temp1)
		{
			case 1: operation.setName(Rose); break;
			case 2:operation.setName(Carnation); break;
			case 3:operation.setName(Gerber); break;
			case 4:operation.setName(Cyclamen); break;
		}
		// SWITCH END

		cout << "Please, enter color" << endl;
		is >> temp2;
		operation.setColor(temp2);


		// SWITCH
		do {
			cout << "Please, enter salary type 1- Flowerpot, 2- Bouquet, 3- Apiece" << endl;
			is >> temp2;
			temp1 = atoi(temp2);
		} while (temp1 < 1 || temp1 > 3);

		switch (temp1)
		{	
			case 1: operation.setSalaryName(Flowerpot); break;
			case 2:operation.setSalaryName(Bouquet); break;
			case 3:operation.setSalaryName(Apiece); break;
		}
		// SWITCH END


		cout << "Please, enter price" << endl;
		is >> temp;
		operation.setPrice(temp);
		cout << "Please, enter count" << endl;
		is >> temp1;
		operation.setCount(temp1);
		operation.setOperationCost(temp1 * operation.getPrice());
		

		return is;
	}

	int getCount() {
		return count;
	}
	void setCount(int count_) {
		count = count_;
	}

	double getOperationCost() {
		return operationCost;
	}

	void setOperationCost(double operationCost_) {
		operationCost = operationCost_;
	}

	SalaryOperation() :GoodsInstance(),Date() {
		int count = 0;
		operationCost = 0;
	}
	SalaryOperation(int count_,int price_,int year_,int week_,salaryEnum salaryName_,flowerType name_,char* color_) :GoodsInstance(price_,salaryName_,color_,name_),Date(year_,week_) {
		count = count_;
		operationCost = count * price_;
	}

	
};



class WeeklyBalance {
private:
	SalaryOperation* list;
	
	int operationCount;
	double summ;
public:
	WeeklyBalance()
	{
		operationCount = 1;
		list = new SalaryOperation[operationCount];
	}

	WeeklyBalance(const int C)
	{
		operationCount = C;
		list = new SalaryOperation[operationCount];
	}

	void inputList(int count_, int price_, int year_, int week_, salaryEnum salaryName_, flowerType name_, char* color_)
	{
		for (int i = 0; i < operationCount; i++)
		{
			SalaryOperation temp(count_, price_, year_, week_, salaryName_, name_, color_);
			list[i] = temp;
		}
	}

	void inputList()
	{
		for (int i = 0; i < operationCount; i++)
		{
			cin >> list[i];
		}
	}

	void inputObj(int count_, int price_, int year_, int week_, salaryEnum salaryName_, flowerType name_, char* color_,int num)
	{
		SalaryOperation temp(count_, price_, year_, week_, salaryName_, name_, color_);
			list[num] = temp;
	}

	void addToList()
	{
		SalaryOperation* temp;
		temp = new SalaryOperation[operationCount+1];
		SalaryOperation temp1;
		cin >> temp1;
		for (int i = 0; i < operationCount; i++)
		{
			temp[i] = list[i];
		}
		operationCount++;
		temp[operationCount-1] = temp1;
		delete [] list;
		list = temp;
	}

	void removeFromList()
	{
		SalaryOperation* temp;
		temp = new SalaryOperation[operationCount - 1];
		SalaryOperation temp1;
		for (int i = 0; i < operationCount-1; i++)
		{
			temp[i] = list[i]; 
		}
		operationCount--;
		delete[] list;
		list = temp;
	}

	int* getFlowersArray() {
		int* arr = new int[operationCount];
		for (int i = 0; i < operationCount; i++) {
			arr[i] = list[i].getCount();
		}
		return arr;
	}

	double* getMoneyArray() {
		double* arr = new double[operationCount];
		for (int i = 0; i < operationCount; i++) {
			arr[i] = list[i].getCount() * list[i].getPrice();
		}
		return arr;
	}

	int getOperationCount() {
		return operationCount;
	}

	SalaryOperation* getMyList()
	{
		return list;
	}
};


// ������ �������
template <typename T>
T �alcCosts(T* mass, int n) {
	T sum = 0;
	
	for (int i = 0; i<n; i++)
		sum += mass[i];
	return sum;
};


class Window {
private:
	int x,y,width,height;
public:
	Window() {
		setX(0);
		setY(0);
		setWidth(1);
		setWidth(2);
	}

	int getX() {
		return x;
	}

	int getY() {
		return y;
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	void setX(int x_) {
		if (x_ >= 0 && x_ <=699)
		x = x_;
	}

	void setY(int y_) {
		if (y_ >= 0 && y_ <= 499) 
		y = y_;
	}

	void setWidth(int width_) {
		if (width_ >= 1 && width <=100)
		width = width_;
	}

	void setHeight(int height_) {
		if (height_ >= 1 && height_ <= 100)
		height = height_;
	}

	Window operator / (Window& b) {
		Window c;
		c.setX(x / b.getX());
		c.setY(y / b.getY());
		c.setWidth(width / b.getWidth());
		c.setHeight(height / b.getHeight());
		return c;
	}

};

int main() {
	Window a();
}




int main() {
	SalaryOperation operation(15, 200, 2014, 2, Flowerpot, Rose, "Red");
	cout << operation;
	cin >> operation;
	cout << operation;

	WeeklyBalance balance(10);
	balance.inputObj(15, 200, 2014, 2, Flowerpot, Rose, "Red",0);
	balance.inputObj(30, 167, 2013, 6, Bouquet, Gerber, "Blue", 1);
	balance.inputObj(24, 143, 2012, 8, Apiece, Carnation, "Green", 2);
	balance.inputObj(18, 123, 2015, 9, Flowerpot, Cyclamen, "Yellow", 3);
	balance.inputObj(17, 125, 2016, 10, Bouquet, Gerber, "White", 4);
	balance.inputObj(10, 143, 2015, 11, Apiece, Carnation, "Purple", 5);
	balance.inputObj(16, 149, 2012, 12, Flowerpot, Cyclamen, "Brown", 6);
	balance.inputObj(65, 123, 2012, 64, Bouquet, Carnation, "Green", 7);
	balance.inputObj(20, 12, 2015, 34, Apiece, Gerber, "Yellow", 8);
	balance.inputObj(24, 265, 2014, 76, Flowerpot, Rose, "Red", 9);

	
	for (int i = 0; i < balance.getOperationCount(); i++) {
		cout << balance.getMyList()[i];
	}
	double* arr = balance.getMoneyArray();
	//int* arr1 = balance.getOperationCount();

	// ��������� ������ �������
	double a = �alcCosts(arr, balance.getOperationCount());

	//int a1 = �alcCosts<int>(arr1, balance.getOperationCount());
	cout << "Template test" << a << endl;
	
	WeeklyBalance balanceTest;

	cout << balance.getMyList()[9];
	balanceTest.addToList();
	cout << balance.getMyList()[10];


	
	
	balance.removeFromList();
	cout << "Deleted object" << endl;
	cout << balance.getMyList()[1];


	SalaryOperation* mass = balance.getMyList();

	cout << "\n\nEnter file name" << endl;
	char fName[30];
	cin >> fName;

	ofstream out(fName, ios::out | ios::binary);
	out.write((char*)&mass, 10 * sizeof(SalaryOperation));
	out.close();
	system("pause");
}	