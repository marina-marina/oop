﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab6
{
    class Round : Point,IGeometrical
    {
    

        public Round() {
            Console.WriteLine("Default round constructor");
        }

        public Round(int x, int y, double radius) : base(x,y){
            Console.WriteLine("Main round constructor");
            this.radius = radius;
        }

        public Round(double radius) : this(0, 0,radius)
        {
            Console.WriteLine("For radius round constructor");
            this.radius = radius;
        }


        public double radius { get; set; }

        public double square
        {
            get { return Math.PI * radius * radius; }
        }
    }
}
