﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab6
{
    class RectangleInRound : Round
    {
        public Rectangle rectangle {get;set;}
        public static String color {get;set;}
        public int writeOnlyTest { private get; set; }
        public int readOnlyTest {get;private set;}
        

        public RectangleInRound() {
            rectangle = new Rectangle();
            rectangle.X = 0;
            rectangle.Y = 0;
            readOnlyTest = 0;
        }

        static RectangleInRound()
        {
            Console.WriteLine("Static constructor");
            color = "Red";
        }

        public RectangleInRound(int x, int y,int side1,int side2) : base(x,y,Math.Sqrt(side1 * side1 + side2 * side2) / 2) {
            rectangle = new Rectangle(x, y, side1, side2);
            readOnlyTest = 123;
        }

    }
}
