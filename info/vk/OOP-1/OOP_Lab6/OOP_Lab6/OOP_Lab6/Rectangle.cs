﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab6
{
    class Rectangle : Point, IGeometrical
    {
        public int side1 { get; set; }
        public int side2 { get; set; }
        private double _square;

        public Rectangle() {
            Console.WriteLine("Default rectangle constructor");
            this.side1 = 0;
            this.side2 = 0;
            _square = 0;
        }

        public Rectangle(int x,int y,int side1, int side2) :base(x,y) {
            Console.WriteLine("Main rectangle constructor");
            this.side1 = side1;
            this.side2 = side2;
            _square = side1 * side2;
        }

        public Rectangle(int side1) : this(0,0,side1,0)
        {
            Console.WriteLine("For side1 rectangle constructor");
        }


        public double square {
            get {
                return _square;
            }

        }

        

        
    }
}
