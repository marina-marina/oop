﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting main");
            RectangleInRound rec = new RectangleInRound(5,6,10,15);
            Console.WriteLine(rec.Y);
            Console.WriteLine(rec.readOnlyTest);
            Console.WriteLine(RectangleInRound.color);
            Console.WriteLine("Square of round =" + rec.square);
            Console.WriteLine("Square of rectangle in round =" + rec.rectangle.square);

            Console.WriteLine("--------------------------------------------");
            Point point = new Point(25);
            Console.WriteLine("---------------------------------------------");
            Rectangle rectangle = new Rectangle(15);
        }
    }
}
