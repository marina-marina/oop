﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab6
{
    public class Point
    {
        private int x;
        private int y;

        public int X {
            get {
                return x;
            }
            set {
                if (value > 0)
                {
                    x = value;
                }
                else {
                    x = 1;
                }
            }
        }

        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                if (value > 0)
                {
                    y = value;
                }
                else
                {
                    y = 1;
                }
            }
        }

        

        public Point() {
            Console.WriteLine("Default Point constructor");
            x = 0;
            y = 0;
        }

        public Point(int x, int y) {
            Console.WriteLine("Main Point constructor");
            this.x = x;
            this.y = y;
        }

        public Point(int x) : this(x, 0)
        {
            Console.WriteLine("For x Point constructor");
        }

    }
}
