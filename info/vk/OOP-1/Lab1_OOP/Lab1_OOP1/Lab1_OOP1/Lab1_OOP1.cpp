// Laba1(OOP).cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "conio.h"
#include "ctime"
#include "cstdlib"
#include "Math.h"

float func(float a, float b, float x);
void scanning(float &a, float &b, float &x1, float &x2, float &dx);
float minSearch(float* array, int steps);
float midValue(float* array, int numsOfSteps);
float calculatingS2(float* array, int numsOfSteps, float mid);
void randomizeArray(float* array, int maxS, int minS, int numsOfSteps);
float maxSearch(float* array, int steps);
void bubbleSort(float* array, int steps);
int* normilizeArray(float* array, int steps);
void buildGistograme(int* array, int steps);
void fileOutput(int* array, int steps);

int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	struct Structure {
		float a, b, x1, x2, dx;
		float *result;
	} var1;
	scanning(var1.a, var1.b, var1.x1, var1.x2, var1.dx);
	

	printf("%f,%f,%f,%f,%f \n", var1.a, var1.b, var1.x1, var1.x2, var1.dx);

	int numsOfSteps = round((var1.x2 - var1.x1) / var1.dx)  + 1;

	var1.result = new float[numsOfSteps];

	

	//Calculate funcion and print table of finding values
	printf("%c", 0xDA);
	
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0xC4, 0xC4, 0xC4, 0xD2, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xD2, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4);
	
	printf("%c \n",0xBF);
	printf("%c  N%c  x   %c    f(x) %c\n",0xB3,0xBA,0xBA,0xB3);
	int steps = 0;



	for (float i = var1.x1; i <= var1.x2; i = i + var1.dx) {
		printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n",0xC6, 0xCD, 0xCD, 0xCD, 0xCE, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCE, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD,0xB5);
		var1.result[steps] = func(var1.a, var1.b, i);
		printf("%c%3d%c%6.1f%c%9.4f%c \n",0xB3, steps,0xBA, i,0xBA, var1.result[steps],0xB3);
		steps++;
	}
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 0xC0, 0xC4, 0xC4, 0xC4, 0xD0, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xD0, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4,0xC4, 0xD9);
	//-------------------------------------------------------------

	//  Searching s1(minimum element)
	
	float s1 = minSearch(var1.result, numsOfSteps);
	printf("Minimum value: %2.4f \n", s1);
	// -----------------------------------------------------



	// Finding middle value of function
	float mid = midValue(var1.result,numsOfSteps);
	
	

	// -----------------------------------

	// Calculating of s2 value
	float s2 = calculatingS2(var1.result,numsOfSteps,mid);

	printf("S1: %3.4f \n", s1);
	printf("S2: %3.4f \n", s2);


	// ---------------------------------------

	// Searching of max and min S


	int maxS = var1.result[0];
	int minS = var1.result[0];

	if (s1 < s2) {
		minS = s1;
		maxS = s2;
	}
	else {
		minS = s2;
		maxS = s1;
	}


	//----------------------------------------------

	// Random array

	float* randArray = new float[numsOfSteps];
	randomizeArray(randArray, maxS, minS, numsOfSteps);
	
	for (int i = 0; i < numsOfSteps; i++) {
		printf("rand arr: %3.3f \n", randArray[i]);
	}
	//------------------------------------------------------

	// Normalize array
	int* normalArray = normilizeArray(randArray,numsOfSteps);

	

	//---------------------------------------------


	//Building graphic
	buildGistograme(normalArray, numsOfSteps);

	//--------------------------------------------------------

	// Sorting random array

	bubbleSort(randArray, numsOfSteps);
	for (int i = 0; i < numsOfSteps; i++) {
		printf("Sorted random array: %2.5f \n", randArray[i]);
	}
	//----------------------------------------------------------

	
	// Output to the file
	fileOutput(normalArray,numsOfSteps);

	//----------------------------------------------------



	getch();

	return 0;
}

//end of main method



// Functions

float func(float a, float b, float x){
	if (x > 1)
	{
		return ((-a) * x * x + b);
	}
	else
	{
		if (x == 0) {
			return -1;
		}
		else
		{
			return (b / x);
		}
	}
}

void scanning(float &a, float &b,float &x1, float &x2, float &dx) {
	printf("Please insert values for variables:  \n");
	scanf_s("%f%f%f%f%f", &a, &b, &x1, &x2, &dx);
	printf("Print values: ");
	
}

float minSearch(float* array, int steps) {
	float s1 = array[0];
	for (int i = 0; i < steps; i++) {
		if (array[i] < s1) {
			s1 = array[i];
		}
	}
	return s1;
}

float maxSearch(float* array, int steps) {
	float s1 = array[0];
	for (int i = 0; i < steps; i++) {
		if (array[i] > s1) {
			s1 = array[i];
		}
	}
	return s1;
}

float midValue(float* array, int numsOfSteps) {
	float mid = 0;
	for (int i = 0; i < numsOfSteps; i++) {
		mid = mid + array[i];
	}
	mid = mid / numsOfSteps;

	return mid;
	
}

float calculatingS2(float* array, int numsOfSteps,float mid) {
	float s2 = 0;
	for (int i = 0; i < numsOfSteps; i++) {
		if (array[i] > mid) {
			s2 = s2 + array[i];
		}
	}

	return s2;
	 
}

void randomizeArray(float* array, int maxS, int minS,int numsOfSteps) {

	const float delta = 0.01;
	float step = maxS * delta;
	printf("step = %3.3f\n", step);
	for (int i = 0; i < numsOfSteps; i++) {
		array[i] = minS + (rand() % int((maxS - minS) / step))*step;
	}
}

void bubbleSort(float* array, int steps) {
	for (int j = 0; j < steps - 1; j++) {
		for (int i = 0; i < steps - j - 1; i++) {
			if (array[i] > array[i + 1]) {
				int b = array[i]; //change for elements
				array[i] = array[i + 1];
				array[i + 1] = b;
			}
		}
	}
	
}

int* normilizeArray(float* array, int steps) {
	int* arr = new int[steps];
	float scale = 80 / (maxSearch(array, steps) - minSearch(array, steps));

	for (int i = 0; i < steps; i++) {
		arr[i] = (array[i] - minSearch(array, steps)) * scale;
	}

	for (int i = 0; i < steps; i++) {
		printf("Normal array %d \n", arr[i]);
	}
	return arr;
}

void buildGistograme(int* array, int steps) {
	int count = array[steps];
	for (int j = 0; j < steps; j++) {
		for (int i = 0; i < array[j]; i++) {
			printf("%c", 0xDC);
		}
		printf("\n");
	}
}

void fileOutput(int* array, int steps) {
	FILE *file;
	if ((file = fopen("D:\\Lab1_Var1\\Var1.txt", "w")) == NULL) {
		printf("Can't open file\n");
		exit(1);
	};
	for (int i = 0; i < steps; i++) {
		fprintf(file, "%d ", array[i]);
	}
	fclose(file);
}