// Солощенко Марина, ЗПІ-зп63

#include <stdio.h>
#include <math.h>

struct values {
  double a, b, x1, x2, dx;
	double *f;
};

int main(){
	float x, y;

  struct values el;
  el.a = 1;
  el.b = 6;
  el.x1 = 0.5;
  el.x2 = 5.5;
  el.dx = 0.25;

  for (x = el.x1; x <= el.x2; x = x + el.dx) {
    if (x >=2) {
      y = - el.a * x * x - el.b;
    } else {
      y = (el.a - el.b) / x;
    }
    // put into array
    printf("%f, %f\n", x, y);
  }
}
